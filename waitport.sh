#!/bin/sh
echo "Waiting for $1 to launch on $2..."

while ! nc -z $1 $2; do
  sleep 1
done
