#!/usr/bin/env sh
cd /app/code

export LDAP_SERVER=$CLOUDRON_LDAP_SERVER
export LDAP_PORT=$CLOUDRON_LDAP_PORT
export LDAP_BASE_DN=$CLOUDRON_LDAP_USERS_BASE_DN
export LDAP_BIND_DN=$CLOUDRON_LDAP_BIND_DN
export LDAP_BIND_PASSWORD=$CLOUDRON_LDAP_BIND_PASSWORD

export DATABASE_URL="${CLOUDRON_POSTGRESQL_URL}?sslmode=disable"

[ ! -f /app/data/jwt_secret ] && base64 -w0 < /dev/urandom | head -c 100 > /app/data/jwt_secret
export JWT_SECRET=$(cat /app/data/jwt_secret)

./dbmate -d db/migrations up
./misremember-exe