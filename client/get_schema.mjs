import { buildSchema, graphqlSync, getIntrospectionQuery } from "graphql";
import fs from "fs";

const schemaSource = fs.readFileSync(
  import.meta.dirname + "/../graphql/schema.gql",
);
const schema = buildSchema(
  "scalar Date\nscalar Cursor\n" + schemaSource.toString("utf8"),
);
const introspect = graphqlSync({
  schema,
  source: getIntrospectionQuery(),
});
fs.writeFileSync(
  import.meta.dirname + "/graphql_schema.json",
  JSON.stringify(introspect.data),
);
