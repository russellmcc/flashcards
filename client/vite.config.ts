import { defineConfig, loadEnv } from "vite";
import react from "@vitejs/plugin-react-swc";
import { join } from "path";

const env = loadEnv("development", join(process.cwd(), ".."), "");
// https://vite.dev/config/
export default defineConfig({
  plugins: [react()],
  build: {
    outDir: "../static",
  },
  server: {
    proxy: {
      "/api": {
        target: env.GRAPHQL_URL,
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },
});
