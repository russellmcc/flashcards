import { useMutation } from "@apollo/client";
import { useCallback, useState } from "react";
import { login } from "./queries.ts";
import LoginForm from "./LoginForm.tsx";

export type AuthorizeProps = {
  logIn: (token: string) => void;
};

const ErrorMap: Record<string, string> = {
  'Failure on Resolving Field "login": Wrong username or password':
    "Wrong username or password",
};

const Authorize = ({ logIn }: AuthorizeProps) => {
  const [mutate] = useMutation(login);
  const [errorMessage, setErrorMessage] = useState<string | undefined>(
    undefined,
  );

  const doAuth = useCallback(
    async ({ username, password }: { username: string; password: string }) => {
      const res = await mutate({
        variables: { input: { username, password } },
      });
      if (res.data) {
        logIn(res.data.login.session.token);
      } else {
        const error = res.errors?.[0]?.message;
        const errorMessage =
          (error ? ErrorMap[error] : undefined) ?? "Unknown Error";
        setErrorMessage(errorMessage);
      }
    },
    [logIn, mutate],
  );

  return <LoginForm error={errorMessage} submit={doAuth} />;
};

export default Authorize;
