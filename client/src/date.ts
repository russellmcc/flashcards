const units = [
  ["year", 365 * 24 * 60 * 60 * 1000],
  ["month", 30 * 24 * 60 * 60 * 1000],
  ["day", 24 * 60 * 60 * 1000],
  ["hour", 60 * 60 * 1000],
  ["minute", 60 * 1000],
  ["second", 1000],
] as const;

export const humanReadableRelativeDate = (from: Date, to: Date) => {
  const rtf = new Intl.RelativeTimeFormat();
  const diff = to.getTime() - from.getTime();
  for (const [unit, ms] of units) {
    const amount = Math.floor(diff / ms);
    if (amount > 0) {
      return rtf.format(amount, unit);
    }
  }
  return rtf.format(0, "second");
};
