export type Result = "bad" | "okay" | "good" | "great";

export type Deck = {
  id: string;
  name: string;
};

export type StudyConfig = {
  newAspectsPerSession: number;
  maxAspectsPerSession: number;
};

export const defaultStudyConfig: StudyConfig = {
  newAspectsPerSession: 30,
  maxAspectsPerSession: 100,
};
