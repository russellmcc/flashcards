import { ReactNode } from "react";
import styles from "./GradeButtons.module.css";
import TestButton from "./TestButton";

export type Grade = "bad" | "okay" | "good" | "great";

const GRADE_CONFIGS = [
  ["bad", "sentiment_very_dissatisfied"],
  ["okay", "sentiment_dissatisfied"],
  ["good", "sentiment_satisfied"],
  ["great", "sentiment_very_satisfied"],
] as const;

type Props = {
  onGrade: (grade: Grade) => void;
};

const GradeButtons = ({ onGrade }: Props): ReactNode => (
  <div className={styles.buttonZone}>
    {GRADE_CONFIGS.map(([grade, icon]) => (
      <TestButton
        key={grade}
        onClick={() => {
          onGrade(grade);
        }}
        icon={icon}
      />
    ))}
  </div>
);

export default GradeButtons;
