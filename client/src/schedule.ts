export type ScheduleConfig = {
  // Steps the card goes through during learning, in minutes.  Must be at least length 1.
  learningStepsMinutes: number[];
  // Steps the card goes through during relearning, in minutes.  Can be length 0.
  relearningStepsMinutes: number[];
  // Initial interval an aspect has when it graduates learning, in minutes.
  newCardIntervalMinutes: number;
  // Initial factor a aspect starts out with
  newCardFactor: number;
  // The minimum factor a aspect can ever have.
  minFactor: number;
  // The interval to use when a aspect graduates from relearning, in minutes
  relearnedIntervalMinutes: number;
  // Factor to use when the aspect is marked as "hard"
  okayFactor: number;

  // Bonus to apply to "great" results - this acts as a scalar on the factor
  greatFactorScale: number;

  badFactorDiff: number;
  okayFactorDiff: number;
  goodFactorDiff: number;
  greatFactorDiff: number;

  // Scale applied any time we use a factor
  factorScale: number;

  // Min difference between response intervals, in minutes
  minResultDiffMinutes: number;
};

export const defaultScheduleConfig: ScheduleConfig = {
  learningStepsMinutes: [0, 10],
  relearningStepsMinutes: [1, 11],
  newCardIntervalMinutes: 24 * 60,
  newCardFactor: 2.5,
  minFactor: 1.3,
  relearnedIntervalMinutes: 24 * 60,
  okayFactor: 1.2,
  greatFactorScale: 1.3,
  badFactorDiff: -0.2,
  okayFactorDiff: -0.15,
  goodFactorDiff: 0,
  greatFactorDiff: 0.15,
  factorScale: 1,
  minResultDiffMinutes: 6 * 60,
};

export type LearningSchedule = {
  __typename: "LearningSchedule";
  step: number;
  dueDate: Date;
};

export type ReviewSchedule = {
  __typename: "ReviewSchedule";
  factor: number;
  studiedDate: Date;
  dueDate: Date;
};

export type RelearningSchedule = {
  __typename: "RelearningSchedule";
  step: number;
  factor: number;
  dueDate: Date;
};

export type Schedule = LearningSchedule | ReviewSchedule | RelearningSchedule;

export type ScheduledAspect = {
  aspectID: string;
  schedule: Schedule;
};
