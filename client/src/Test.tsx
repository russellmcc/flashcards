import { ScheduledAspectFragment } from "./__generated__/graphql.ts";
import EntryTest from "./EntryTest.tsx";
import SelfGradedPictionaryTest from "./SelfGradedPictionaryTest.tsx";
import { TestAction, TestSession } from "./session.ts";

export type Props = {
  test: TestSession;
  dispatch: (action: TestAction) => void;
  aspect: ScheduledAspectFragment;
};

const Test = ({ test, dispatch, aspect }: Props) => {
  if (test.type === "self-graded-pictionary") {
    return <SelfGradedPictionaryTest test={test} dispatch={dispatch} />;
  }
  return <EntryTest test={test} dispatch={dispatch} aspect={aspect} />;
};

export default Test;
