import { ReactNode, useCallback } from "react";
import Header from "./Header.tsx";
import DeckList from "./DeckList.tsx";
import Study from "./Study.tsx";
import { BrowserRouter, Routes, Route, useParams } from "react-router-dom";

export type RouterProps = {
  logOut: () => void;
};

export const FourOhFour = () => <div>{"Not Found!"}</div>;

const StudyWithParams = ({
  header,
}: {
  header: (props: { children?: ReactNode }) => ReactNode;
}) => {
  const { deckID } = useParams();
  if (!deckID) {
    return <FourOhFour />;
  }
  return <Study deckID={deckID} header={header} />;
};

const Router = ({ logOut }: RouterProps) => {
  const header = useCallback(
    ({
      children,
      rightChildren,
    }: {
      children?: ReactNode;
      rightChildren?: ReactNode;
    }) => (
      <Header logOut={logOut} rightChildren={rightChildren}>
        {children}
      </Header>
    ),
    [logOut],
  );
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<DeckList header={header} />} />
        <Route
          path="/study/:deckID"
          element={<StudyWithParams header={header} />}
        />
        <Route path="*" element={<FourOhFour />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
