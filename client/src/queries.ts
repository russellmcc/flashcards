import { gql } from "./__generated__";

export const login = gql(`mutation Login($input: LoginInput!) {
    login(input: $input) {
      session { token }
    }
  }
`);

export const getAllDecks = gql(`
  query GetAllDecks {
    currentUser {
      decks  {
        ...UserDeckData
      }
    }
  }

  fragment UserDeckData on UserDeckData {
    deck  {
      ...Deck
    }
    scheduledAspects(first: 50)  {
      ...ScheduledAspectConnection
    }
    readyAspects  {
      ...ReadyAspect
    }
  }

  fragment ReadyAspect on ReadyAspect {
    aspect  {
      ...Aspect
    }
  }

  fragment ScheduledAspect on ScheduledAspect {
    aspect  {
      ...Aspect
    }
    dueDate
    schedule {
      ... on LearningSchedule  {
        step
      }
      ... on RelearningSchedule  {
        step
        factor
      }
      ... on ReviewSchedule  {
        factor
        studiedDate
      }
    }
  }

  fragment ScheduledAspectConnection on ScheduledAspectConnection {
    edges {
      node {
        ...ScheduledAspect
      }
    }
  }

  fragment Aspect on Aspect {
    id
    name
    tests  {
      ...Test
    }
  }

  fragment SelfGradedPictionaryTestData on SelfGradedPictionaryTestData {
    front
    back
  }

  fragment EntryTestData on EntryTestData {
    front
    back
    wrongs
    autoGrade
  }

  fragment Test on Test {
    name
    testData  {
      ... on SelfGradedPictionaryTestData  {
        ...SelfGradedPictionaryTestData
      }
      ... on EntryTestData  {
        ...EntryTestData
      }
    }
  }

  fragment Deck on Deck {
    id
    name
  }
`);

export const scheduleAspectLearning = gql(`
  mutation ScheduleAspectLearning($input: ScheduleAspectLearningInput!) {
    scheduleAspectLearning(input: $input) {
      scheduledAspect { aspect { id } }
    }
  }
`);

export const scheduleAspectRelearning = gql(`
  mutation ScheduleAspectRelearning($input: ScheduleAspectRelearningInput!) {
    scheduleAspectRelearning(input: $input) {
      scheduledAspect { aspect { id } }
    }
  }
`);

export const scheduleAspectReview = gql(`
  mutation ScheduleAspectReview($input: ScheduleAspectReviewInput!) {
    scheduleAspectReview(input: $input) {
      scheduledAspect { aspect { id } }
    }
  }
`);
