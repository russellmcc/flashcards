import { ReactNode } from "react";
import styles from "./ErrorDisplay.module.css";

export type ErrorDisplayProps = {
  header: (props: object) => ReactNode;
  error: string;
};

export const ErrorDisplay = ({ header, error }: ErrorDisplayProps) => (
  <>
    {header({})}
    <div className={styles.errorText}>{error}</div>
  </>
);
