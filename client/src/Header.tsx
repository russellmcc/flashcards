import { PropsWithChildren, ReactNode } from "react";
import styles from "./Header.module.css";

export type HeaderProps = {
  logOut: () => void;
  rightChildren?: ReactNode;
};

const Header = ({
  logOut,
  rightChildren,
  children,
}: PropsWithChildren<HeaderProps>) => (
  <div className={styles.header}>
    {children}
    <div>
      {rightChildren}
      <button className={styles.logOut! + " material-icons"} onClick={logOut}>
        {"exit_to_app"}
      </button>
    </div>
  </div>
);

export default Header;
