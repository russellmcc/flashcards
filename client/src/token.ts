export const getTokenFromStorage = (): string | null =>
  localStorage.getItem("misremember-auth-token");

export const removeTokenFromStorage = () => {
  localStorage.removeItem("misremember-auth-token");
};

export const saveTokenToStorage = (token: string) => {
  localStorage.setItem("misremember-auth-token", token);
};
