import { useCallback, useState } from "react";
import styles from "./LoginForm.module.css";
import { Content } from "./Content.tsx";

export type LoginFormProps = {
  error?: string;
  submit: (data: { username: string; password: string }) => Promise<void>;
};

export const LoginForm = ({ error, submit }: LoginFormProps) => {
  // use controlled components
  const [state, setState] = useState({ username: "", password: "" });

  const updateUsername = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setState((state) => ({ ...state, username: e.target.value }));
    },
    [],
  );

  const updatePassword = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setState((state) => ({ ...state, password: e.target.value }));
    },
    [],
  );

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    void submit(state);
  };
  return (
    <Content>
      <h1 className={styles.prompt}>{error ?? "Please log in"}</h1>
      <form className={styles.form} onSubmit={onSubmit}>
        <label className={styles.label} htmlFor="username">
          {"Username"}
        </label>
        <input
          className={styles.input}
          type="text"
          name="username"
          onChange={updateUsername}
        />
        <label className={styles.label} htmlFor="password">
          {"Password"}
        </label>
        <input
          className={styles.input}
          type="password"
          name="password"
          onChange={updatePassword}
        />
        <button className={styles.button} type="submit">
          {"login"}
        </button>
      </form>
    </Content>
  );
};

export default LoginForm;
