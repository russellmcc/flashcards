import {
  AspectFragment,
  LearningSchedule,
  RelearningSchedule,
  ReviewSchedule,
  ScheduledAspectFragment,
} from "./__generated__/graphql";
import { ScheduleConfig, ScheduledAspect } from "./schedule.ts";
import { Result } from "./study.ts";

const minutesToMilliseconds = (minutes: number) => minutes * 60 * 1000;

const rescheduleLearn = (
  aspect: AspectFragment,
  schedule: LearningSchedule,
  result: Result,
  studiedDate: Date,
  config: ScheduleConfig,
): ScheduledAspect => {
  if (result === "bad") {
    // If we did "bad",  we reset to the beginning of learning.
    return {
      aspectID: aspect.id,
      schedule: {
        __typename: "LearningSchedule",
        step: 0,
        dueDate: new Date(
          studiedDate.getTime() +
            minutesToMilliseconds(config.learningStepsMinutes[0]!),
        ),
      },
    };
  }

  // If we didn't do bad, we ignore everything else during learning and jump to the next step.
  const newStep = schedule.step + 1;
  if (newStep >= config.learningStepsMinutes.length) {
    // If we've reached the end of learning, we promote to review.
    return {
      aspectID: aspect.id,
      schedule: {
        __typename: "ReviewSchedule",
        factor: config.newCardFactor,
        studiedDate: studiedDate,
        dueDate: new Date(
          studiedDate.getTime() +
            minutesToMilliseconds(config.newCardIntervalMinutes),
        ),
      },
    };
  } else {
    // Otherwise, we progress learning.
    return {
      aspectID: aspect.id,
      schedule: {
        __typename: "LearningSchedule",
        step: newStep,
        dueDate: new Date(
          studiedDate.getTime() +
            minutesToMilliseconds(config.learningStepsMinutes[newStep]!),
        ),
      },
    };
  }
};

const rescheduleRelearn = (
  aspect: AspectFragment,
  schedule: RelearningSchedule,
  result: Result,
  studiedDate: Date,
  config: ScheduleConfig,
): ScheduledAspect => {
  // If we did bad, reset to the start of relearning.
  if (result === "bad") {
    return {
      aspectID: aspect.id,
      schedule: {
        __typename: "RelearningSchedule",
        step: 0,
        factor: schedule.factor,
        dueDate: new Date(
          studiedDate.getTime() +
            minutesToMilliseconds(config.relearningStepsMinutes[0]!),
        ),
      },
    };
  }

  // Otherwise, we progress relearning.
  const newStep = schedule.step + 1;
  if (newStep >= config.relearningStepsMinutes.length) {
    // If we've reached the end of relearning, we promote to review.
    return {
      aspectID: aspect.id,
      schedule: {
        __typename: "ReviewSchedule",
        factor: schedule.factor,
        studiedDate: studiedDate,
        dueDate: new Date(
          studiedDate.getTime() +
            minutesToMilliseconds(config.relearnedIntervalMinutes),
        ),
      },
    };
  }

  // Otherwise, we progress relearning.
  return {
    aspectID: aspect.id,
    schedule: {
      __typename: "RelearningSchedule",
      step: newStep,
      factor: schedule.factor,
      dueDate: new Date(
        studiedDate.getTime() +
          minutesToMilliseconds(config.relearningStepsMinutes[newStep]!),
      ),
    },
  };
};

const factorDiffForResult = (
  config: ScheduleConfig,
  result: Result,
): number => {
  switch (result) {
    case "bad":
      return config.badFactorDiff;
    case "okay":
      return config.okayFactorDiff;
    case "good":
      return config.goodFactorDiff;
    case "great":
      return config.greatFactorDiff;
  }
};

const rescheduleReview = (
  aspect: AspectFragment,
  origDueDate: Date,
  schedule: ReviewSchedule,
  result: Result,
  studiedDate: Date,
  config: ScheduleConfig,
): ScheduledAspect => {
  const newFactor = Math.max(
    config.minFactor,
    schedule.factor + factorDiffForResult(config, result),
  );

  const relearningEnabled = config.relearningStepsMinutes.length > 0;
  const badDue = relearningEnabled
    ? new Date(
        studiedDate.getTime() +
          minutesToMilliseconds(config.relearningStepsMinutes[0]!),
      )
    : new Date(
        studiedDate.getTime() +
          minutesToMilliseconds(config.relearnedIntervalMinutes),
      );
  if (result === "bad" && relearningEnabled) {
    return {
      aspectID: aspect.id,
      schedule: {
        __typename: "RelearningSchedule",
        step: 0,
        factor: newFactor,
        dueDate: badDue,
      },
    };
  }
  const studiedDateMS = studiedDate.getTime();
  const prevIntervalMS =
    origDueDate.getTime() - new Date(schedule.studiedDate as string).getTime();
  const delayMS = studiedDate.getTime() - origDueDate.getTime();
  const okayDue = new Date(
    Math.max(
      badDue.getTime() + minutesToMilliseconds(config.minResultDiffMinutes),
      studiedDateMS + prevIntervalMS * config.okayFactor * config.factorScale,
    ),
  );
  const goodDue = new Date(
    Math.max(
      okayDue.getTime() + minutesToMilliseconds(config.minResultDiffMinutes),
      studiedDateMS +
        (prevIntervalMS + delayMS / 2) * schedule.factor * config.factorScale,
    ),
  );
  const greatDue = new Date(
    Math.max(
      goodDue.getTime() + minutesToMilliseconds(config.minResultDiffMinutes),
      studiedDateMS +
        (prevIntervalMS + delayMS) *
          schedule.factor *
          config.factorScale *
          config.greatFactorScale,
    ),
  );
  const dueDate = (() => {
    switch (result) {
      case "bad":
        return badDue;
      case "okay":
        return okayDue;
      case "good":
        return goodDue;
      case "great":
        return greatDue;
    }
  })();
  return {
    aspectID: aspect.id,
    schedule: {
      __typename: "ReviewSchedule",
      factor: newFactor,
      studiedDate,
      dueDate,
    },
  };
};

export const reschedule = (
  aspect: ScheduledAspectFragment,
  result: Result,
  studiedDate: Date,
  config: ScheduleConfig,
): ScheduledAspect => {
  // How we schedule it depends on the current schedule.
  switch (aspect.schedule.__typename) {
    case "LearningSchedule":
      return rescheduleLearn(
        aspect.aspect,
        aspect.schedule,
        result,
        studiedDate,
        config,
      );
    case "RelearningSchedule":
      return rescheduleRelearn(
        aspect.aspect,
        aspect.schedule,
        result,
        studiedDate,
        config,
      );
    case "ReviewSchedule":
      return rescheduleReview(
        aspect.aspect,
        new Date(aspect.dueDate as string),
        aspect.schedule,
        result,
        studiedDate,
        config,
      );
  }
  throw new Error("Unknown schedule type");
};
