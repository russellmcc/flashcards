import { PropsWithChildren } from "react";
import { Link } from "react-router-dom";
import styles from "./IconLink.module.css";
export type IconLinkProps = {
  to: string;
};

const IconLink = ({ to, children }: PropsWithChildren<IconLinkProps>) => (
  <Link className={styles.iconLink! + " material-icons"} to={to}>
    {children}
  </Link>
);

export default IconLink;
