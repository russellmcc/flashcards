import styles from "./TestButton.module.css";

export type TestButtonProps = {
  onClick: () => void;
  icon: string;
};

const TestButton = ({ onClick, icon }: TestButtonProps) => (
  <button className={styles.button! + " material-icons"} onClick={onClick}>
    {icon}
  </button>
);

export default TestButton;
