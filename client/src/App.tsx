import { useState, useMemo, useCallback } from "react";
import {
  getTokenFromStorage,
  removeTokenFromStorage,
  saveTokenToStorage,
} from "./token.ts";
import { ApolloProvider } from "@apollo/client";
import { instanceForToken } from "./graphqlClient.ts";
import Router from "./Router.tsx";
import Authorize from "./Authorize.tsx";
import { useVisualViewport } from "./viewport.ts";

const App = () => {
  useVisualViewport();
  const [token, setToken] = useState(() => getTokenFromStorage());
  const client = useMemo(() => instanceForToken(token), [token]);
  const logIn = useCallback((token: string) => {
    setToken(token);
    saveTokenToStorage(token);
  }, []);
  const logOut = useCallback(() => {
    setToken(null);
    removeTokenFromStorage();
  }, []);
  return (
    <ApolloProvider client={client}>
      {token ? <Router logOut={logOut} /> : <Authorize logIn={logIn} />}
    </ApolloProvider>
  );
};

export default App;
