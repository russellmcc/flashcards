import { ReactNode } from "react";
import styles from "./LoadingDisplay.module.css";

export type LoadingDisplayProps = {
  header: (props: object) => ReactNode;
};

export const LoadingDisplay = ({ header }: LoadingDisplayProps) => (
  <>
    {header({})}
    <div className={styles.loadingText}>{"Loading..."}</div>
  </>
);
