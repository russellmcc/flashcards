import { ReactNode } from "react";
import styles from "./DeckList.module.css";
import { getAllDecks } from "./queries";
import { useQuery } from "@apollo/client";
import { LoadingDisplay } from "./LoadingDisplay.tsx";
import { ErrorDisplay } from "./ErrorDisplay.tsx";
import { defaultStudyConfig } from "./study.ts";
import { getAllAspects, nextDueAspectDate } from "./session.ts";
import { UserDeckDataFragment } from "./__generated__/graphql";
import { Link } from "react-router-dom";
import { Content } from "./Content.tsx";
import { humanReadableRelativeDate } from "./date.ts";

export type DeckListProps = {
  header: (props: object) => ReactNode;
};

const getParenthetical = (deckData: UserDeckDataFragment) => {
  const now = new Date();
  const allAspects = getAllAspects({
    deckData,
    studyConfig: defaultStudyConfig,
    now,
  });
  if (allAspects.length > 0) {
    return ` (${allAspects.length.toString()} due)`;
  }

  const nextDueDate = nextDueAspectDate(deckData);
  if (!nextDueDate) {
    return ` (nothing)`;
  }

  return ` (next due ${humanReadableRelativeDate(now, nextDueDate)})`;
};

const DeckList = ({ header }: DeckListProps) => {
  const { loading, error, data } = useQuery(getAllDecks);
  if (loading) {
    return <LoadingDisplay header={header} />;
  }
  if (error) {
    return <ErrorDisplay header={header} error={error.message} />;
  }
  if (!data) {
    return <ErrorDisplay header={header} error={"Unknown error"} />;
  }
  return (
    <>
      {header({})}
      <Content>
        {data.currentUser.decks.map((deck) => (
          <div className={styles.entry} key={deck.deck.id}>
            <Link to={`/study/${deck.deck.id}`}>
              {deck.deck.name}
              <span className={styles.due}>{getParenthetical(deck)}</span>
            </Link>
          </div>
        ))}
      </Content>
    </>
  );
};

export default DeckList;
