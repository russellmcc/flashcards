import {
  AspectFragment,
  TestFragment,
  ScheduledAspectFragment,
  UserDeckDataFragment,
  SelfGradedPictionaryTestDataFragment,
  EntryTestDataFragment,
} from "./__generated__/graphql";
import { reschedule as getRescheduled } from "./reschedule.ts";
import { ScheduleConfig, ScheduledAspect } from "./schedule.ts";
type TestDataFragment = TestFragment["testData"];

import { defaultStudyConfig, Result, StudyConfig } from "./study";

export type SelfGradedPictionaryTestState = "front" | "back";
export type SelfGradedPictionaryTestSession = {
  type: "self-graded-pictionary";
  test: SelfGradedPictionaryTestDataFragment;
  state: SelfGradedPictionaryTestState;
};

export type EntryTestState = "front" | { entry: string };
export type EntryTestSession = {
  type: "entry";
  test: EntryTestDataFragment;
  state: EntryTestState;
};

export type TestSession = SelfGradedPictionaryTestSession | EntryTestSession;

export type ActiveSessionForTest<T extends TestSession> = {
  state: "active";
  aspects: ScheduledAspectFragment[];
  aspect: ScheduledAspectFragment;
  test: T;
};

export type ActiveSession = ActiveSessionForTest<TestSession>;

export type CompletedSession = {
  state: "completed";
};

export type NoCardsSession = {
  state: "no-cards";
};

export type ErrorSession = {
  state: "error";
};

export type Session =
  | ActiveSession
  | CompletedSession
  | NoCardsSession
  | ErrorSession;

export type SelfGradedPictionaryFlipAction = {
  type: "self-graded-pictionary";
  action: "flip";
};

export type SelfGradedPictionaryResultAction = {
  type: "self-graded-pictionary";
  action: "result";
  result: Result;
};

export type SelfGradedPictionaryAction =
  | SelfGradedPictionaryFlipAction
  | SelfGradedPictionaryResultAction;

export type EntryTestEntryAction = {
  type: "entry";
  action: "entry";
  value: string;
  autoGrade: boolean;
};

export type EntryTestResultAction = {
  type: "entry";
  action: "result";
  result: Result;
};

export type EntryTestAction = EntryTestEntryAction | EntryTestResultAction;

export type TestAction = SelfGradedPictionaryAction | EntryTestAction;

const shuffleInPlace = <T>(arr: T[]): T[] => {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [arr[i], arr[j]] = [arr[j]!, arr[i]!];
  }
  return arr;
};

const handleResult = (
  state: { session: ActiveSession; deckData: UserDeckDataFragment },
  result: Result,
  scheduleConfig: ScheduleConfig,
  reschedule: (aspect: ScheduledAspect) => void,
  now: Date,
): ActiveSession | CompletedSession => {
  const rescheduled = getRescheduled(
    state.session.aspect,
    result,
    now,
    scheduleConfig,
  );
  // As a side effect, reschedule the aspect.
  reschedule(rescheduled);

  // If the new due date is within 15 minutes, add it to the current session
  const newAspects = state.session.aspects.slice();
  if (
    rescheduled.schedule.dueDate <= new Date(now.getTime() + 15 * 60 * 1000)
  ) {
    newAspects.push({
      aspect: state.session.aspect.aspect,
      dueDate: rescheduled.schedule.dueDate,
      schedule: rescheduled.schedule,
    });
    // Shuffle the aspects
    shuffleInPlace(newAspects);
  }
  return startNextTest(newAspects, now);
};

const dispatchSelfGradedPictionaryAction = (
  state: {
    session: ActiveSessionForTest<SelfGradedPictionaryTestSession>;
    deckData: UserDeckDataFragment;
  },
  action: SelfGradedPictionaryAction,
  scheduleConfig: ScheduleConfig,
  reschedule: (aspect: ScheduledAspect) => void,
  now: Date,
): ActiveSession | CompletedSession => {
  switch (action.action) {
    case "flip":
      return {
        ...state.session,
        test: {
          ...state.session.test,
          state: state.session.test.state === "front" ? "back" : "front",
        },
      };
    case "result": {
      return handleResult(
        state,
        action.result,
        scheduleConfig,
        reschedule,
        now,
      );
    }
  }
};

const dispatchEntryTestAction = (
  state: {
    session: ActiveSessionForTest<EntryTestSession>;
    deckData: UserDeckDataFragment;
  },
  action: EntryTestAction,
  scheduleConfig: ScheduleConfig,
  reschedule: (aspect: ScheduledAspect) => void,
  now: Date,
): ActiveSession | CompletedSession => {
  switch (action.action) {
    case "entry":
      if (action.autoGrade && state.session.test.test.back === action.value) {
        return handleResult(state, "good", scheduleConfig, reschedule, now);
      }
      return {
        ...state.session,
        test: {
          ...state.session.test,
          state: { entry: action.value },
        },
      };
    case "result":
      return handleResult(
        state,
        action.result,
        scheduleConfig,
        reschedule,
        now,
      );
  }
};

export const dispatchTestAction = (
  state: { session: Session; deckData: UserDeckDataFragment },
  action: TestAction,
  scheduleConfig: ScheduleConfig,
  reschedule: (aspect: ScheduledAspect) => void,
  now: Date,
): Session => {
  switch (action.type) {
    case "self-graded-pictionary":
      if (
        state.session.state === "active" &&
        state.session.test.type === "self-graded-pictionary"
      ) {
        return dispatchSelfGradedPictionaryAction(
          {
            // Not sure why typescript can't infer this :(
            session:
              state.session as ActiveSessionForTest<SelfGradedPictionaryTestSession>,
            deckData: state.deckData,
          },
          action,
          scheduleConfig,
          reschedule,
          now,
        );
      } else {
        return { state: "error" };
      }
    case "entry":
      if (state.session.state === "active") {
        return dispatchEntryTestAction(
          {
            // Not sure why typescript can't infer this :(
            session: state.session as ActiveSessionForTest<EntryTestSession>,
            deckData: state.deckData,
          },
          action,
          scheduleConfig,
          reschedule,
          now,
        );
      } else {
        return { state: "error" };
      }
  }
};

export type Action =
  | {
      type: "init";
      deckData: UserDeckDataFragment;
    }
  | TestAction;

export const dispatch = (
  state: { session: Session; deckData: UserDeckDataFragment },
  action: Action,
  scheduleConfig: ScheduleConfig,
  reschedule: (aspect: ScheduledAspect) => void,
  now: Date,
): { session: Session; deckData: UserDeckDataFragment } => {
  switch (action.type) {
    case "init":
      return {
        session: newSession({
          deckData: action.deckData,
          studyConfig: defaultStudyConfig,
          now,
        }),
        deckData: action.deckData,
      };
  }
  return {
    ...state,
    session: dispatchTestAction(state, action, scheduleConfig, reschedule, now),
  };
};

const startTest = (test: TestDataFragment): TestSession | undefined => {
  if (test.__typename === "SelfGradedPictionaryTestData") {
    return {
      type: "self-graded-pictionary",
      test,
      state: "front",
    };
  }
  if (test.__typename === "EntryTestData") {
    return {
      type: "entry",
      test,
      state: "front",
    };
  }
  return undefined;
};

const understoodTestData = (testData: TestFragment["testData"]): boolean =>
  startTest(testData) !== undefined;

type AnyAspect = { aspect: AspectFragment };

const hasTests = (aspect: AnyAspect) =>
  aspect.aspect.tests.some((test) => understoodTestData(test.testData));

export const due = (aspect: ScheduledAspectFragment, now: Date) =>
  new Date(aspect.dueDate as string) <= now;

const pickRandom = <T>(arr: T[]): T => {
  console.assert(arr.length > 0, "array is empty");
  return arr[Math.floor(Math.random() * arr.length)]!;
};

const newTestStateForAspect = (anyAspect: AnyAspect): TestSession => {
  const aspect = anyAspect.aspect;
  const test = pickRandom(
    aspect.tests.filter((test) => understoodTestData(test.testData)),
  );
  // Note - we know that the testData is understood, so we can safely assume
  // that startTest will return a valid test session.
  return startTest(test.testData)!;
};

const startNextTest = (
  aspects: ScheduledAspectFragment[],
  now: Date,
): ActiveSession | CompletedSession => {
  let firstDueIndex = aspects.findIndex((aspect) => due(aspect, now));
  let firstDue = firstDueIndex >= 0 ? aspects[firstDueIndex] : undefined;
  if (!firstDue) {
    if (aspects.length === 0) {
      return { state: "completed" };
    }
    firstDueIndex = Math.floor(Math.random() * aspects.length);
    firstDue = aspects[firstDueIndex]!;
  }
  const remainingAspects = aspects
    .slice(0, firstDueIndex)
    .concat(aspects.slice(firstDueIndex + 1));
  const test = newTestStateForAspect(firstDue);
  return {
    state: "active",
    aspects: remainingAspects,
    aspect: firstDue,
    test,
  };
};

export const getAllScheduled = (
  deckData: UserDeckDataFragment,
): ScheduledAspectFragment[] =>
  deckData.scheduledAspects.edges
    .map((edge) => edge.node)
    .filter((aspect) => hasTests(aspect));

export const getAllAspects = ({
  deckData,
  studyConfig,
  now,
}: {
  deckData: UserDeckDataFragment;
  studyConfig: StudyConfig;
  now: Date;
}): ScheduledAspectFragment[] => {
  const aspectsWithTests = getAllScheduled(deckData);
  const readyAspects = aspectsWithTests
    .filter((aspect) => due(aspect, now))
    .slice(0, studyConfig.maxAspectsPerSession);
  const newAspectLength = Math.min(
    studyConfig.maxAspectsPerSession - readyAspects.length,
    studyConfig.newAspectsPerSession,
  );
  const newAspects = deckData.readyAspects
    .filter((aspect) => hasTests(aspect))
    .slice(0, newAspectLength);
  const all = readyAspects.concat(
    newAspects.map((a) => ({
      aspect: a.aspect,
      dueDate: now,
      schedule: { __typename: "LearningSchedule", step: 0 },
    })),
  );
  // Make sure we don't have duplicate ids, if so, choose one of each.
  const usedIds = new Set<string>();
  return all.filter((a) => {
    if (usedIds.has(a.aspect.id)) {
      return false;
    }
    usedIds.add(a.aspect.id);
    return true;
  });
};

export const nextDueAspectDate = (
  deckData: UserDeckDataFragment,
): Date | undefined => {
  const allScheduled = getAllScheduled(deckData);
  if (allScheduled.length === 0) {
    return undefined;
  }

  allScheduled.sort(
    (a, b) =>
      new Date(a.dueDate as string).getTime() -
      new Date(b.dueDate as string).getTime(),
  );
  return new Date(allScheduled[0]!.dueDate as string);
};

export const newSession = ({
  deckData,
  studyConfig,
  now,
}: {
  deckData: UserDeckDataFragment;
  studyConfig: StudyConfig;
  now: Date;
}): Session => {
  const allAspects = getAllAspects({ deckData, studyConfig, now });
  shuffleInPlace(allAspects);
  const session = startNextTest(allAspects, now);
  if (session.state === "completed") {
    return { state: "no-cards" };
  }
  return session;
};
