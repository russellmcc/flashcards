import { ReactNode, useCallback } from "react";
import IconLink from "./IconLink.tsx";
import { useMutation, useQuery } from "@apollo/client";
import {
  getAllDecks,
  scheduleAspectLearning,
  scheduleAspectRelearning,
  scheduleAspectReview,
} from "./queries.ts";
import { LoadingDisplay } from "./LoadingDisplay.tsx";
import { ErrorDisplay } from "./ErrorDisplay.tsx";
import { ScheduledAspect } from "./schedule.ts";
import { FourOhFour } from "./Router.tsx";
import Session from "./Session.tsx";
export type StudyProps = {
  deckID: string;
  header: (props: { children?: ReactNode }) => ReactNode;
};

const Study = ({ deckID, header: rawHeader }: StudyProps) => {
  const header = useCallback(
    ({ children }: { children?: ReactNode }) =>
      rawHeader({
        children: (
          <>
            <IconLink to={`/`}>{"arrow_back"}</IconLink>
            {children}
          </>
        ),
      }),
    [rawHeader],
  );

  const [saLearning] = useMutation(scheduleAspectLearning);
  const [saRelearning] = useMutation(scheduleAspectRelearning);
  const [saReview] = useMutation(scheduleAspectReview);

  const reschedule = useCallback(
    (schedule: ScheduledAspect) => {
      if (schedule.schedule.__typename === "LearningSchedule") {
        void saLearning({
          variables: {
            input: {
              aspect: schedule.aspectID,
              step: schedule.schedule.step,
              dueDate: schedule.schedule.dueDate.toISOString(),
              scheduledDate: new Date().toISOString(),
            },
          },
        });
      }
      if (schedule.schedule.__typename === "RelearningSchedule") {
        void saRelearning({
          variables: {
            input: {
              aspect: schedule.aspectID,
              step: schedule.schedule.step,
              factor: schedule.schedule.factor,
              dueDate: schedule.schedule.dueDate.toISOString(),
              scheduledDate: new Date().toISOString(),
            },
          },
        });
      }
      if (schedule.schedule.__typename === "ReviewSchedule") {
        void saReview({
          variables: {
            input: {
              aspect: schedule.aspectID,
              factor: schedule.schedule.factor,
              studiedDate: schedule.schedule.studiedDate.toISOString(),
              dueDate: schedule.schedule.dueDate.toISOString(),
              scheduledDate: new Date().toISOString(),
            },
          },
        });
      }
    },
    [saLearning, saRelearning, saReview],
  );

  const { loading, error, data } = useQuery(getAllDecks);
  if (loading) {
    return <LoadingDisplay header={header} />;
  }
  if (error) {
    return <ErrorDisplay header={header} error={error.message} />;
  }
  if (!data) {
    return <ErrorDisplay header={header} error={"Unknown error"} />;
  }

  const deck = data.currentUser.decks.find((deck) => deck.deck.id === deckID);
  if (!deck) {
    return <FourOhFour />;
  }

  return <Session deck={deck} header={header} reschedule={reschedule} />;
};

export default Study;
