import { useRef } from "react";
import {
  SelfGradedPictionaryAction,
  SelfGradedPictionaryTestSession,
} from "./session";
import styles from "./SelfGradedPictionaryTest.module.css";
import SignatureCanvas from "react-signature-canvas";
import TestButton from "./TestButton";
import { SelfGradedPictionaryTestData } from "./__generated__/graphql";
import GradeButtons from "./GradeButtons";

export type Props = {
  test: SelfGradedPictionaryTestSession;
  dispatch: (action: SelfGradedPictionaryAction) => void;
};

type CanvasProxy = {
  clear: () => void;
  on: () => void;
  off: () => void;
};

const SelfGradedPictionaryTest = ({ test, dispatch }: Props) => {
  const sigPad = useRef<CanvasProxy | null>(null);
  const lastTest = useRef<SelfGradedPictionaryTestData | undefined>(undefined);
  const updateSigForPictionary = (testData: SelfGradedPictionaryTestData) => {
    if (testData !== lastTest.current) {
      lastTest.current = testData;
      sigPad.current?.clear();
    }
  };

  if (test.state === "front") {
    updateSigForPictionary(test.test);
    return (
      <>
        <div className={styles.aspectData}>{test.test.front}</div>
        <SignatureCanvas
          canvasProps={{ className: styles.pictionaryCanvas }}
          penColor={"#d3d4d9"}
          minDistance={0}
          throttle={0}
          ref={(ref: CanvasProxy | null) => {
            sigPad.current = ref;
          }}
        />
        <div className={styles.buttonZone}>
          <TestButton
            onClick={() => {
              dispatch({
                type: "self-graded-pictionary",
                action: "flip",
              });
            }}
            icon="subdirectory_arrow_right"
          />
        </div>
      </>
    );
  } else {
    updateSigForPictionary(test.test);
    return (
      <>
        <div className={styles.aspectData}>{test.test.back}</div>
        <SignatureCanvas
          canvasProps={{ className: styles.pictionaryCanvas }}
          penColor={"#ff3e41"}
          minDistance={0}
          throttle={0}
          ref={(ref: CanvasProxy | null) => {
            sigPad.current = ref;
          }}
        />
        <GradeButtons
          onGrade={(result) => {
            dispatch({
              type: "self-graded-pictionary",
              action: "result",
              result,
            });
          }}
        />
      </>
    );
  }
};

export default SelfGradedPictionaryTest;
