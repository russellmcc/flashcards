import { ReactNode, useCallback, useEffect, useState } from "react";
import { defaultScheduleConfig, ScheduledAspect } from "./schedule.ts";
import { UserDeckDataFragment } from "./__generated__/graphql.ts";
import {
  Action,
  newSession,
  Session as SessionState,
  dispatch,
  nextDueAspectDate,
} from "./session.ts";
import { defaultStudyConfig } from "./study";
import { Content } from "./Content.tsx";
import styles from "./Session.module.css";
import { humanReadableRelativeDate } from "./date.ts";
import Test from "./Test.tsx";
export type SessionProps = {
  deck: UserDeckDataFragment;
  header: (props: { children?: ReactNode }) => ReactNode;
  reschedule: (scheduledAspect: ScheduledAspect) => void;
};

type State = {
  deckData: UserDeckDataFragment;
  session: SessionState;
};

const initState = (deckData: UserDeckDataFragment): State => ({
  deckData,
  session: newSession({
    deckData,
    studyConfig: defaultStudyConfig,
    now: new Date(),
  }),
});

const InfoScreen = ({ message }: { message: string }) => (
  <div className={styles.aspectData}>{message}</div>
);

const Session = ({ deck, header, reschedule }: SessionProps) => {
  const sessionReducer = useCallback(
    (state: State, action: Action): State =>
      dispatch(state, action, defaultScheduleConfig, reschedule, new Date()),
    [reschedule],
  );
  const [state, setState] = useState(() => initState(deck));
  const dispatchAction = useCallback(
    (action: Action) => {
      setState(sessionReducer(state, action));
    },
    [sessionReducer, state],
  );

  // Auto re-init when deck changes.
  useEffect(() => {
    if (state.deckData.deck.id !== deck.deck.id) {
      dispatchAction({ type: "init", deckData: deck });
    }
  }, [deck, dispatchAction, state.deckData.deck.id]);
  const aspectsRemaining =
    state.session.state === "active"
      ? (() => {
          const aspectsRemaining = state.session.aspects.length + 1;
          return (
            <div className={styles.aspectsRemainingContainer}>
              <div className={styles.aspectsRemainingLabel}>
                {"aspects remaining"}
              </div>
              <div className={styles.aspectsRemaining}>{aspectsRemaining}</div>
            </div>
          );
        })()
      : undefined;
  return (
    <>
      {header({ children: aspectsRemaining })}
      <Content>
        {(() => {
          switch (state.session.state) {
            case "active": {
              return (
                <Test
                  test={state.session.test}
                  dispatch={dispatchAction}
                  aspect={state.session.aspect}
                />
              );
            }
            case "completed":
              return <InfoScreen message="Session complete! Good work!" />;
            case "no-cards": {
              const nextDue = nextDueAspectDate(state.deckData);
              if (nextDue) {
                return (
                  <InfoScreen
                    message={`No aspects are due! Next aspect is due in ${humanReadableRelativeDate(
                      new Date(),
                      nextDue,
                    )}`}
                  />
                );
              }
              return <InfoScreen message="No aspects are due! Good work!" />;
            }
            case "error":
              return <InfoScreen message="Something bad happened!" />;
          }
        })()}
      </Content>
    </>
  );
};

export default Session;
