import { EntryTestAction, EntryTestSession } from "./session";
import styles from "./EntryTest.module.css";
import { ScheduledAspectFragment } from "./__generated__/graphql";
import GradeButtons from "./GradeButtons";

export type Props = {
  test: EntryTestSession;
  aspect: ScheduledAspectFragment;
  dispatch: (action: EntryTestAction) => void;
};

// Add a helper function to parse and render text with bracketed content
const renderTextWithBrackets = (text: string) => {
  const parts = text.split(/(\[[^\]]*\])/);
  return (
    <span>
      {parts.map((part, index) => {
        if (part.startsWith("[") && part.endsWith("]")) {
          return (
            <span key={index} className={styles.keyword}>
              {part.slice(1, -1)}
            </span>
          );
        }
        return part;
      })}
    </span>
  );
};

const EntryTest = ({ test, aspect, dispatch }: Props) => {
  const schedule = aspect.schedule;
  const multipleChoiceThresholdMS = 4 * 24 * 60 * 60 * 1000;

  // We skip wrongs if the aspect is in review and interval is more than a week.
  const choices =
    (schedule.__typename === "ReviewSchedule" &&
      new Date(aspect.dueDate as string).getTime() -
        new Date(schedule.studiedDate as string).getTime() >
        multipleChoiceThresholdMS) ||
    !test.test.wrongs ||
    test.test.wrongs.length === 0
      ? undefined
      : (() => {
          const answers = [];
          const wrongs = test.test.wrongs.slice();
          while (answers.length < 3 && wrongs.length > 0) {
            const index = Math.floor(Math.random() * wrongs.length);
            answers.push(wrongs[index]!);
            wrongs.splice(index, 1);
          }
          const index = Math.floor(Math.random() * (answers.length + 1));
          answers.splice(index, 0, test.test.back);
          return answers;
        })();

  const autoGrade = choices !== undefined || test.test.autoGrade;

  if (test.state === "front") {
    return (
      <>
        {!choices ? (
          <div className={styles.aspectData}>{aspect.aspect.name}</div>
        ) : undefined}
        <div className={styles.aspectData}>
          {renderTextWithBrackets(test.test.front)}
        </div>
        {choices ? (
          choices.map((choice) => (
            <button
              key={choice}
              className={styles.button}
              onClick={(e) => {
                e.preventDefault();
                dispatch({
                  type: "entry",
                  action: "entry",
                  value: choice,
                  autoGrade,
                });
              }}
            >
              {choice}
            </button>
          ))
        ) : (
          <form
            onSubmit={(e: React.FormEvent<HTMLFormElement>) => {
              e.preventDefault();
              dispatch({
                type: "entry",
                action: "entry",
                value: new FormData(e.currentTarget).get("answer") as string,
                autoGrade,
              });
            }}
          >
            <input
              name="answer"
              autoFocus
              className={styles.input}
              type="text"
              placeholder=""
            />
          </form>
        )}
      </>
    );
  }

  const entry = test.state.entry;
  return (
    <>
      <div className={styles.aspectData}>
        {renderTextWithBrackets(test.test.front)}
      </div>
      {test.test.back === entry ? (
        <div className={styles.aspectData}>{test.test.back}</div>
      ) : (
        <>
          <div className={styles.aspectData}>{test.test.back}</div>
          <div className={styles.incorrect}>{entry}</div>
        </>
      )}
      {autoGrade ? (
        <button
          className={styles.button}
          onClick={(e) => {
            e.preventDefault();
            dispatch({
              type: "entry",
              action: "result",
              result: test.test.back === entry ? "good" : "bad",
            });
          }}
        >
          next
        </button>
      ) : (
        <GradeButtons
          onGrade={(result) => {
            dispatch({
              type: "entry",
              action: "result",
              result,
            });
          }}
        />
      )}
    </>
  );
};

export default EntryTest;
