import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  NormalizedCacheObject,
} from "@apollo/client";

// in memory cache for caching GraphQL data
const cache = new InMemoryCache();

export const instanceForToken = (
  token: string | null,
): ApolloClient<NormalizedCacheObject> => {
  const headers: Record<string, string> =
    token !== null ? { Authorization: `Bearer ${token}` } : {};
  const link = new HttpLink({ uri: "/api", headers });
  return new ApolloClient({
    link,
    cache,
    defaultOptions: {
      mutate: { awaitRefetchQueries: true, errorPolicy: "all" },
      query: { fetchPolicy: "network-only", errorPolicy: "all" },
      watchQuery: { fetchPolicy: "network-only", errorPolicy: "all" },
    },
  });
};
