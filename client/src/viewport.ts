import { useCallback, useEffect } from "react";

export const useVisualViewport = () => {
  const updateViewport = useCallback((): void => {
    if (window.visualViewport) {
      document.documentElement.style.setProperty(
        "--viewport-height",
        `${window.visualViewport.height.toString()}px`,
      );
    } else {
      document.documentElement.style.setProperty("--viewport-height", "100dvh");
    }
  }, []);

  useEffect(() => {
    updateViewport();
    const viewport = window.visualViewport;
    if (viewport) {
      viewport.addEventListener("resize", updateViewport);
      viewport.addEventListener("scroll", updateViewport);

      return () => {
        viewport.removeEventListener("resize", updateViewport);
        viewport.removeEventListener("scroll", updateViewport);
      };
    }
  }, [updateViewport]);
};
