-- migrate:up
create table decks
(
    id            serial,
    name          varchar not null,
    owner         integer,
    metadata      jsonb not null,
    public        boolean not null
);

create unique index decks_id on decks (id);
create index decks_owner on decks (owner);

-- migrate:down
drop table decks;