-- migrate:up
create table tests
(
    id            serial,
    name          varchar not null,
    aspect_id     int not null references aspects(id),
    type          int not null,
    test_data     jsonb not null
);

create unique index tests_id on tests(id);

-- migrate:down
drop table tests;


