-- migrate:up
create table scheduled_aspects
(
    user_id       int not null references users(id),
    aspect_id     int not null references aspects(id),
    type          int not null,
    due_date      timestamptz not null,
    studied_date  timestamptz,
    factor        double precision,
    step          int,
    constraint pk_user_aspect primary key (aspect_id, user_id)
);

-- migrate:down
drop table scheduled_aspects

