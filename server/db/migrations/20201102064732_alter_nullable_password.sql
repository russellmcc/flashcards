-- migrate:up
alter table users alter column password_hash drop not null;

-- migrate:down
alter table users alter column password_hash set not null;
