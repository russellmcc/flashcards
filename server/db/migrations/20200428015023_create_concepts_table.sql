-- migrate:up
create table concepts
(
    id            serial,
    name          varchar not null,
    deck_id       int not null references decks(id),
    info          jsonb not null
);

create unique index concepts_id on concepts (id);

-- migrate:down
drop table concepts;


