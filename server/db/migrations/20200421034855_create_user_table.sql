-- migrate:up
create table users
(
    id            serial,
    username      varchar not null unique,
    password_hash varchar not null,
    profile       jsonb not null
);

create unique index users_id on users (id);
create unique index users_username on users (username);

-- migrate:down
drop table users;
