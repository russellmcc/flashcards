-- migrate:up
create table aspects
(
    id            serial,
    name          varchar not null,
    concept_id    int not null references concepts(id),
    info          jsonb not null
);

create unique index aspects_id on aspects (id);

-- migrate:down
drop table aspects;

