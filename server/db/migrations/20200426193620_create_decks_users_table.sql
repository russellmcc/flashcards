-- migrate:up
create table decks_users
(
    user_id       int not null references users(id),
    deck_id       int not null references decks(id),
    constraint pk_user_deck primary key (user_id, deck_id)
);

create index decks_users_user_id on decks_users (user_id);
create index decks_users_deck_id on decks_users (deck_id);

-- migrate:down
drop table decks_users;

