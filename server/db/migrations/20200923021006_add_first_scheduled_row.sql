-- migrate:up
alter table scheduled_aspects add first_scheduled timestamptz not null default (now())

-- migrate:down
alter table scheduled_aspects drop column first_scheduled