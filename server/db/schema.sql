SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: aspects; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.aspects (
    id integer NOT NULL,
    name character varying NOT NULL,
    concept_id integer NOT NULL,
    info jsonb NOT NULL
);


--
-- Name: aspects_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.aspects_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: aspects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.aspects_id_seq OWNED BY public.aspects.id;


--
-- Name: concepts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.concepts (
    id integer NOT NULL,
    name character varying NOT NULL,
    deck_id integer NOT NULL,
    info jsonb NOT NULL
);


--
-- Name: concepts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.concepts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: concepts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.concepts_id_seq OWNED BY public.concepts.id;


--
-- Name: decks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.decks (
    id integer NOT NULL,
    name character varying NOT NULL,
    owner integer,
    metadata jsonb NOT NULL,
    public boolean NOT NULL
);


--
-- Name: decks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.decks_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: decks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.decks_id_seq OWNED BY public.decks.id;


--
-- Name: decks_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.decks_users (
    user_id integer NOT NULL,
    deck_id integer NOT NULL
);


--
-- Name: scheduled_aspects; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.scheduled_aspects (
    user_id integer NOT NULL,
    aspect_id integer NOT NULL,
    type integer NOT NULL,
    due_date timestamp with time zone NOT NULL,
    studied_date timestamp with time zone,
    factor double precision,
    step integer,
    first_scheduled timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: tests; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tests (
    id integer NOT NULL,
    name character varying NOT NULL,
    aspect_id integer NOT NULL,
    type integer NOT NULL,
    test_data jsonb NOT NULL
);


--
-- Name: tests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tests_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tests_id_seq OWNED BY public.tests.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    username character varying NOT NULL,
    password_hash character varying,
    profile jsonb NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: aspects id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.aspects ALTER COLUMN id SET DEFAULT nextval('public.aspects_id_seq'::regclass);


--
-- Name: concepts id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.concepts ALTER COLUMN id SET DEFAULT nextval('public.concepts_id_seq'::regclass);


--
-- Name: decks id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.decks ALTER COLUMN id SET DEFAULT nextval('public.decks_id_seq'::regclass);


--
-- Name: tests id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tests ALTER COLUMN id SET DEFAULT nextval('public.tests_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: scheduled_aspects pk_user_aspect; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_aspects
    ADD CONSTRAINT pk_user_aspect PRIMARY KEY (aspect_id, user_id);


--
-- Name: decks_users pk_user_deck; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.decks_users
    ADD CONSTRAINT pk_user_deck PRIMARY KEY (user_id, deck_id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: users users_username_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- Name: aspects_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX aspects_id ON public.aspects USING btree (id);


--
-- Name: concepts_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX concepts_id ON public.concepts USING btree (id);


--
-- Name: decks_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX decks_id ON public.decks USING btree (id);


--
-- Name: decks_owner; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX decks_owner ON public.decks USING btree (owner);


--
-- Name: decks_users_deck_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX decks_users_deck_id ON public.decks_users USING btree (deck_id);


--
-- Name: decks_users_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX decks_users_user_id ON public.decks_users USING btree (user_id);


--
-- Name: tests_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX tests_id ON public.tests USING btree (id);


--
-- Name: users_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX users_id ON public.users USING btree (id);


--
-- Name: users_username; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX users_username ON public.users USING btree (username);


--
-- Name: aspects aspects_concept_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.aspects
    ADD CONSTRAINT aspects_concept_id_fkey FOREIGN KEY (concept_id) REFERENCES public.concepts(id);


--
-- Name: concepts concepts_deck_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.concepts
    ADD CONSTRAINT concepts_deck_id_fkey FOREIGN KEY (deck_id) REFERENCES public.decks(id);


--
-- Name: decks_users decks_users_deck_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.decks_users
    ADD CONSTRAINT decks_users_deck_id_fkey FOREIGN KEY (deck_id) REFERENCES public.decks(id);


--
-- Name: decks_users decks_users_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.decks_users
    ADD CONSTRAINT decks_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: scheduled_aspects scheduled_aspects_aspect_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_aspects
    ADD CONSTRAINT scheduled_aspects_aspect_id_fkey FOREIGN KEY (aspect_id) REFERENCES public.aspects(id);


--
-- Name: scheduled_aspects scheduled_aspects_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_aspects
    ADD CONSTRAINT scheduled_aspects_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: tests tests_aspect_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tests
    ADD CONSTRAINT tests_aspect_id_fkey FOREIGN KEY (aspect_id) REFERENCES public.aspects(id);


--
-- PostgreSQL database dump complete
--


--
-- Dbmate schema migrations
--

INSERT INTO public.schema_migrations (version) VALUES
    ('20200421034855'),
    ('20200423050115'),
    ('20200426193620'),
    ('20200428015023'),
    ('20200429021020'),
    ('20200501022204'),
    ('20200503201127'),
    ('20200923021006'),
    ('20201102064732');
