{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE UndecidableInstances #-}

module Graphql where

import Control.Monad.Trans (MonadTrans)
import Data.Morpheus.Types
import Web

-------------------------------------------------------------------------------

-- | Resolve single value
type Value o m a = Resolver o () m a

-- | Resolve object (which includes other fields that need their own resolvers)
type Object o m a = Resolver o () m (a (Resolver o () m))

-- | Resolve (Maybe object)
type MaybeObject o m a =
  Resolver o () m (Maybe (a (Resolver o () m)))

-- | Resolve [object]
type ArrayObject o m a =
  Resolver o () m [a (Resolver o () m)]

type GraphQL o m =
  ( MonadAppContextOp m,
    MonadFail (Resolver o () m),
    Monad (Resolver o () m),
    WithOperation o,
    MonadTrans (Resolver o ())
  )
