{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

module Source
  ( Source (..),
    fetchSource,
    throwWeb,
  )
where

import Authentication.Password
import Control.Monad.Catch (catch)
import Control.Monad.Fraxl (ASeq (..), Fetch, traverseASeq)
import Control.Monad.Reader (asks)
import Control.Monad.Trans (liftIO)
import Control.Monad.Trans.Except (Except, ExceptT, runExcept, throwE)
import Control.Monad.Trans.State (State, evalState, execState, get, modify)
import Data.GADT.Compare.TH
import Data.GADT.Show.TH
import Data.Maybe (listToMaybe)
import Data.Pool (withResource)
import Data.Profunctor.Product.Default (Default)
import Data.Text (Text)
import Data.Time.Clock (UTCTime, addUTCTime, getCurrentTime, nominalDay)
import qualified Database.Aspect as DBA
import qualified Database.Concept as DBC
import qualified Database.Deck as DBD
import Database.Model (AspectID, ConceptID, DeckID, TestID, UserID)
import qualified Database.Model as DB
import Database.Pagination
import Database.PostgreSQL.Simple (SqlError)
import qualified Database.ScheduledAspect as DBSA
import qualified Database.Test as DBT
import qualified Database.User as DBU
import GHC.Int (Int64)
import Opaleye (Column, Delete, FromFields, Insert, Select, SqlInt4, Update, asc, toFields, (.&&), (.==), (.>), (.||))
import qualified Opaleye (countRows, limit, runDelete, runInsert, runSelect, runUpdate)
import Web (Env (..), Web, WebError (..))

type WebExceptT m a = ExceptT WebError m a

type WebExcept a = Except WebError a

data Source a where
  FindUserByUserName :: Text -> Source (WebExcept (Maybe DB.User))
  FindUserByID :: UserID -> Source (WebExcept DB.User)
  CreateUser :: Text -> Maybe Text -> Source (WebExcept DB.User)
  UpdatePassword :: UserID -> Text -> Source (WebExcept ())
  CreateDeck :: Text -> UserID -> Bool -> Source (WebExcept DB.Deck)
  DecksForUser :: UserID -> Source (WebExcept [DB.Deck])
  ConceptsForDeck :: DeckID -> Source (WebExcept [DB.Concept])
  FindDeckByID :: DeckID -> Source (WebExcept DB.Deck)
  FindConceptByID :: ConceptID -> Source (WebExcept DB.Concept)
  CreateConcept :: DeckID -> Text -> Source (WebExcept DB.Concept)
  DeleteConcepts :: [ConceptID] -> Source (WebExcept ())
  FindAspectsByConceptID :: ConceptID -> Source (WebExcept [DB.Aspect])
  DeleteAspects :: [AspectID] -> Source (WebExcept ())
  CreateAspect :: ConceptID -> Text -> Source (WebExcept DB.Aspect)
  FindAspectByID :: AspectID -> Source (WebExcept DB.Aspect)
  FindTestsByAspectID :: AspectID -> Source (WebExcept [DB.Test])
  CreateTest :: AspectID -> Text -> DB.TestData -> Source (WebExcept DB.Test)
  FindTestByID :: TestID -> Source (WebExcept DB.Test)
  DeleteTests :: [TestID] -> Source (WebExcept ())
  ScheduledAspectsForDeck ::
    UserID ->
    DeckID ->
    PaginationArgs (UTCTime, DB.AspectID) ->
    Source (WebExcept (Paginated (UTCTime, DB.AspectID) DB.ScheduledAspect))
  CurrentTime :: Source UTCTime
  ReadyAspectsForTime :: UTCTime -> UserID -> DeckID -> Source (WebExcept [DB.Aspect])
  DeckAccessible :: UserID -> DeckID -> Source (WebExcept Bool)
  ScheduleAspect :: DB.ScheduledAspect -> UTCTime -> Source (WebExcept DB.ScheduledAspect)

deriveGShow ''Source

deriveGEq ''Source

deriveGCompare ''Source

throwWeb :: (Monad m) => Text -> WebExceptT m a
throwWeb = throwE . WebError

expandWeak :: [(a, Int)] -> Int -> [Maybe a]
expandWeak ans_ k = go ans_ 0
  where
    go [] i | k == i = []
    go [] i | i < k = Nothing : go [] (i + 1)
    go ((a, n) : ans) i | n == i = Just a : go ans (i + 1)
    go ans i = Nothing : go ans (i + 1)

runWeakMultiSelect ::
  Default FromFields fields haskells =>
  ([a] -> Select (fields, Column SqlInt4)) ->
  [a] ->
  Web [Maybe haskells]
runWeakMultiSelect select as =
  if Prelude.null as
    then pure []
    else do
      db <- asks dbPool
      bs <- liftIO $
        withResource db $
          \connection -> Opaleye.runSelect connection (select as)
      pure $ expandWeak bs (length as)

runStrongMultiSelect ::
  Default FromFields fields haskells =>
  ([a] -> Select (fields, Column SqlInt4)) ->
  [a] ->
  Web (WebExcept [haskells])
runStrongMultiSelect select as =
  if Prelude.null as
    then pure $ pure []
    else do
      db <- asks dbPool
      (bs :: [(haskells, Int)]) <- liftIO $
        withResource db $
          \connection -> Opaleye.runSelect connection (select as)
      if length bs == length as
        then pure $ pure $ fst <$> bs
        else pure $ throwWeb "Invalid key"

expandList :: [(a, Int)] -> Int -> [[a]]
expandList ans_ k = go ans_ (-1)
  where
    go [] i | k == i = []
    go [] i | i < k = [] : go [] (i + 1)
    go ((a, n) : ans) i | n == i = app' a $ go ans i
    go ans i = [] : go ans (i + 1)
    app' x (y : ys) = (x : y) : ys
    app' _ [] = []

runListMultiSelect ::
  Default FromFields fields haskells =>
  ([a] -> Select (fields, Column SqlInt4)) ->
  [a] ->
  Web [[haskells]]
runListMultiSelect select as =
  if Prelude.null as
    then pure [[]]
    else do
      db <- asks dbPool
      (bs :: [(haskells, Int)]) <- liftIO $
        withResource db $
          \connection -> Opaleye.runSelect connection (select as)
      pure $ expandList bs (length as)

runSelect' ::
  Default FromFields fields haskells =>
  Select fields ->
  Web [haskells]
runSelect' select = do
  db <- asks dbPool
  liftIO $
    withResource db $
      \connection -> Opaleye.runSelect connection select

runInsert :: Insert haskells -> Web haskells
runInsert insert = do
  db <- asks dbPool
  liftIO $
    withResource db $
      \connection -> Opaleye.runInsert connection insert

runUpdate' :: Update haskells -> Web haskells
runUpdate' update = do
  db <- asks dbPool
  liftIO $
    withResource db $
      \connection -> Opaleye.runUpdate connection update

runDelete' :: Delete haskells -> Web haskells
runDelete' delete = do
  db <- asks dbPool
  liftIO $
    withResource db $
      \connection -> Opaleye.runDelete connection delete

upsert :: (a -> Insert b) -> (a -> Update b) -> a -> Web b
upsert ins upd a = catch (runInsert (ins a)) (\(_ :: SqlError) -> runUpdate' . upd $ a)

doUnbatchedFindUserByUserName :: Text -> Web (WebExcept (Maybe DB.User))
doUnbatchedFindUserByUserName n = pure . head <$> runWeakMultiSelect DBU.findUsersByUsernames [n]

doStrongUnbatched :: Default FromFields fields b => ([a] -> Select (fields, Column SqlInt4)) -> a -> Web (WebExcept b)
doStrongUnbatched s k = (head <$>) <$> runStrongMultiSelect s [k]

doUnbatchedFindUserByID :: UserID -> Web (WebExcept DB.User)
doUnbatchedFindUserByID = doStrongUnbatched DBU.findUsersByIDs

createUser :: Text -> Maybe Text -> Web (WebExcept DB.User)
createUser username password = do
  ph <- mapM (liftIO . hashPassword) password
  runInsert (DBU.insertUser (username, ph)) >>= (pure . maybe (throwWeb "couldn't make user") pure) . listToMaybe

updatePassword :: UserID -> Text -> Web (WebExcept ())
updatePassword u p = do
  ph <- liftIO $ hashPassword p
  i <- runUpdate' $ DBU.updateUserPassword u ph
  case i of
    1 -> pure $ pure ()
    _ -> pure $ throwWeb "couldn't update password"

createDeck :: Text -> UserID -> Bool -> Web (WebExcept DB.Deck)
createDeck n u p =
  runInsert (DBD.insertDeck (n, u, p))
    >>= maybe
      (pure $ throwWeb "couldn't make deck")
      ( \deck ->
          runInsert (DBD.insertDeckUser (u, DB.deckID deck)) >>= \case
            1 -> pure $ pure deck
            _ -> pure $ throwWeb "couldn't make deck"
      )
      . listToMaybe

decksForUser :: UserID -> Web (WebExcept [DB.Deck])
decksForUser u = pure <$> runSelect' (DBD.findDecksByUserID u)

conceptsForDeck :: DeckID -> Web (WebExcept [DB.Concept])
conceptsForDeck d = pure <$> runSelect' (DBC.findConceptsByDeckID d)

doUnbatchedFindDeckByID :: DeckID -> Web (WebExcept DB.Deck)
doUnbatchedFindDeckByID = doStrongUnbatched DBD.findDecksByIDs

doUnbatchedFindConceptByID :: ConceptID -> Web (WebExcept DB.Concept)
doUnbatchedFindConceptByID = doStrongUnbatched DBC.findConceptsByIDs

createConcept :: DeckID -> Text -> Web (WebExcept DB.Concept)
createConcept dID n =
  runInsert (DBC.insertConcept (dID, n)) >>= (pure . maybe (throwWeb "Database error: could not create concept") pure) . listToMaybe

deleteConcepts :: [ConceptID] -> Web (WebExcept ())
deleteConcepts cIDs = runDelete' (DBC.deleteConceptByIDs cIDs) >> pure (pure ())

deleteAspects :: [AspectID] -> Web (WebExcept ())
deleteAspects aIDs =
  runDelete' (DBT.deleteTestsWithAspectIDs aIDs)
    >> runDelete' (DBSA.deleteScheduledAspectByIDs aIDs)
    >> runDelete' (DBA.deleteAspectByIDs aIDs)
    >> pure (pure ())

deleteTests :: [TestID] -> Web (WebExcept ())
deleteTests tIDs = runDelete' (DBT.deleteTestsByIDs tIDs) >> pure (pure ())

createAspect :: ConceptID -> Text -> Web (WebExcept DB.Aspect)
createAspect cID n =
  runInsert (DBA.insertAspect (cID, n)) >>= (pure . maybe (throwWeb "Database error: could not create aspect") pure) . listToMaybe

createTest :: AspectID -> Text -> DB.TestData -> Web (WebExcept DB.Test)
createTest aID n d =
  runInsert (DBT.insertTest (aID, n, d)) >>= (pure . maybe (throwWeb "Database error: could not create test") pure) . (DB.testFromRaw <$>) . listToMaybe

scheduledAspectsForDeck :: UserID -> DeckID -> PaginationArgs (UTCTime, DB.AspectID) -> Web (WebExcept (Paginated (UTCTime, DB.AspectID) DB.ScheduledAspect))
scheduledAspectsForDeck uID dID pa =
  either throwWeb (pure . (DB.scheduledAspectFromRaw <$>))
    <$> runPaginatedSelect
      (\r -> (DB.scheduledAspectRawFDueDate r, DB.scheduledAspectRawFAspectID r))
      (asc DB.scheduledAspectRawFDueDate <> asc (DB.getID . DB.scheduledAspectRawFAspectID))
      ( \r k ->
          (DB.scheduledAspectRawFDueDate r .> toFields (fst k))
            .|| ( (DB.scheduledAspectRawFDueDate r .== toFields (fst k))
                    .&& DB.getID (DB.scheduledAspectRawFAspectID r) .> (toFields . DB.getID $ snd k)
                )
      )
      (DBSA.rawFindScheduledAspectsByDeckID uID dID)
      runSelect'
      pa

maxReadyAspectsPerDay :: Int
maxReadyAspectsPerDay = 30

readyAspectsForTime :: UTCTime -> UserID -> DeckID -> Web (WebExcept [DB.Aspect])
readyAspectsForTime now uID dID =
  runSelect' (Opaleye.countRows $ DBSA.findScheduledAspectsByDeckIDFirstScheduledAfter uID dID (addUTCTime (- nominalDay) now)) >>= \ns ->
    maybe
      (pure $ throwWeb "Database error")
      ( \(n :: Int64) ->
          pure <$> runSelect' (Opaleye.limit (Prelude.max (0 :: Int) (maxReadyAspectsPerDay - fromIntegral n)) $ DBSA.findReadyAspectsByDeckID uID dID)
      )
      (listToMaybe ns)

deckAccessible :: UserID -> DeckID -> Web (WebExcept Bool)
deckAccessible u d = pure . Prelude.null <$> ((runSelect' $ DBD.findDeckForUser u d) :: Web [DB.Deck])

scheduleAspect :: DB.ScheduledAspect -> UTCTime -> Web (WebExcept DB.ScheduledAspect)
scheduleAspect sa schedDate =
  -- Note that here updateScheduledAspect will ignore the first scheduled field.
  upsert DBSA.insertScheduledAspect DBSA.updateScheduledAspect (DB.scheduledAspectToRaw schedDate sa)
    >>= (pure . maybe (throwWeb "Couldn't update schedule") (pure . DB.scheduledAspectFromRaw)) . listToMaybe

data BatchedReq = BatchedReq
  { aspectIDs :: [AspectID],
    testIDs :: [TestID],
    aspectsForConceptIDs :: [ConceptID],
    testsForAspectIDs :: [AspectID]
  }

data BatchedRes = BatchedRes
  { aspects :: WebExcept [DB.Aspect],
    tests :: WebExcept [DB.Test],
    aspectsForConcepts :: WebExcept [[DB.Aspect]],
    testsForAspects :: WebExcept [[DB.Test]]
  }

batchSource :: ASeq Source a -> BatchedReq
batchSource = flip execState (BatchedReq [] [] [] []) . traverseASeq batch
  where
    batch :: Source a -> State BatchedReq (Source a)
    batch x@(FindAspectByID a) = modify (\b -> b {aspectIDs = a : aspectIDs b}) >> pure x
    batch x@(FindTestByID t) = modify (\b -> b {testIDs = t : testIDs b}) >> pure x
    batch x@(FindAspectsByConceptID a) = modify (\b -> b {aspectsForConceptIDs = a : aspectsForConceptIDs b}) >> pure x
    batch x@(FindTestsByAspectID a) = modify (\b -> b {testsForAspectIDs = a : testsForAspectIDs b}) >> pure x
    batch x = pure x

runGetTestsForAspectIDs :: [AspectID] -> Web [[DB.Test]]
runGetTestsForAspectIDs as = ((DB.testFromRaw <$>) <$>) <$> raws
  where
    raws :: Web [[DB.TestRaw]]
    raws = runListMultiSelect DBT.findTestsByAspectIDs as

runBatch :: BatchedReq -> Web BatchedRes
runBatch BatchedReq {aspectIDs, testIDs, aspectsForConceptIDs, testsForAspectIDs} =
  BatchedRes
    <$> ((reverse <$>) <$> runStrongMultiSelect DBA.findAspectsByIDs aspectIDs)
    <*> (((DB.testFromRaw <$>) <$>) <$> ((reverse <$>) <$> runStrongMultiSelect DBT.findTestsByIDs testIDs))
    <*> (pure <$> (reverse <$> runListMultiSelect DBA.findAspectsByConceptIDs aspectsForConceptIDs))
    <*> (pure <$> (reverse <$> runGetTestsForAspectIDs testsForAspectIDs))

popField :: (BatchedRes -> WebExcept [a]) -> (WebExcept [a] -> BatchedRes -> BatchedRes) -> State BatchedRes (Web (WebExcept a))
popField g s = get >>= either (pure . pure . throwE) pop' . runExcept . g
  where
    pop' as = modify (s (pure $ tail as)) >> pure (pure $ pure $ head as)

popAspect :: State BatchedRes (Web (WebExcept DB.Aspect))
popAspect = popField aspects (\as b -> b {aspects = as})

popTest :: State BatchedRes (Web (WebExcept DB.Test))
popTest = popField tests (\as b -> b {tests = as})

popAspectForConceptID :: State BatchedRes (Web (WebExcept [DB.Aspect]))
popAspectForConceptID = popField aspectsForConcepts (\as b -> b {aspectsForConcepts = as})

popTestsForAspectID :: State BatchedRes (Web (WebExcept [DB.Test]))
popTestsForAspectID = popField testsForAspects (\as b -> b {testsForAspects = as})

doFetch :: Source a -> State BatchedRes (Web a)
doFetch (FindUserByUserName n) = pure $ doUnbatchedFindUserByUserName n
doFetch (FindUserByID n) = pure $ doUnbatchedFindUserByID n
doFetch (CreateUser un pw) = pure $ createUser un pw
doFetch (UpdatePassword uID pw) = pure $ updatePassword uID pw
doFetch (CreateDeck n u p) = pure $ createDeck n u p
doFetch (DecksForUser u) = pure $ decksForUser u
doFetch (ConceptsForDeck d) = pure $ conceptsForDeck d
doFetch (FindDeckByID d) = pure $ doUnbatchedFindDeckByID d
doFetch (FindConceptByID c) = pure $ doUnbatchedFindConceptByID c
doFetch (CreateConcept d n) = pure $ createConcept d n
doFetch (DeleteConcepts cs) = pure $ deleteConcepts cs
doFetch (FindAspectsByConceptID _) = popAspectForConceptID
doFetch (DeleteAspects as) = pure $ deleteAspects as
doFetch (CreateAspect c n) = pure $ createAspect c n
doFetch (FindAspectByID _) = popAspect
doFetch (FindTestByID _) = popTest
doFetch (FindTestsByAspectID _) = popTestsForAspectID
doFetch (CreateTest a n t) = pure $ createTest a n t
doFetch (DeleteTests ts) = pure $ deleteTests ts
doFetch (ScheduledAspectsForDeck u d p) = pure $ scheduledAspectsForDeck u d p
doFetch CurrentTime = pure $ liftIO getCurrentTime
doFetch (ReadyAspectsForTime n u d) = pure $ readyAspectsForTime n u d
doFetch (DeckAccessible u d) = pure $ deckAccessible u d
doFetch (ScheduleAspect sa d) = pure $ scheduleAspect sa d

fetchSource :: Fetch Source Web a
fetchSource as =
  runBatch (batchSource as) >>= \batch ->
    pure $ evalState (traverseASeq doFetch as) batch
