module Authentication.Logic (login, register, changePassword) where

import Authentication.JWT
import Authentication.Password
import Control.Monad.Trans (MonadTrans, lift)
import Data.Bool (bool)
import Data.Text
import qualified Database.Model as DB
import Logic
import Web (MonadAppContextOp (..), WebError (..), WebErrorHandler (..))

type Carrier t m =
  ( Monad m,
    MonadAppContextOp m,
    Monad (t (Context m)),
    WebErrorHandler (t (Context m)),
    MonadTrans t
  )

liftCT :: (Monad m, MonadTrans t) => m a -> t (Context m) a
liftCT = lift . lift

passwordError :: WebErrorHandler m => m a
passwordError = handle $ WebError "Wrong username or password"

loginPassword :: (Carrier t m) => Text -> Text -> t (Context m) Token
loginPassword username password = findUserByUserName username >>= maybe passwordError validateAndLogin
  where
    validateAndLogin user =
      if maybe False (`validateHashedPassword` password) (DB.userPasswordHash user)
        then liftCT (makeJWTForUserID (DB.userID user))
        else passwordError

loginLDAP :: (Carrier t m) => Text -> Text -> t (Context m) Token
loginLDAP username password =
  liftCT (verifyLDAPPassword username password)
    >>= bool passwordError (findUserByUserName username >>= maybe (createUser username Nothing >>= token) token)
  where
    token = liftCT . makeJWTForUserID . DB.userID

login :: (Carrier t m) => Text -> Text -> t (Context m) Token
login u p = liftCT hasLDAP >>= bool (loginPassword u p) (loginLDAP u p)

registerPassword :: (Carrier t m) => Text -> Text -> t (Context m) Token
registerPassword u p = findUserByUserName u >>= maybe register' (const . handle . WebError $ "This username is already taken")
  where
    register' = createUser u (Just p) >>= liftCT . makeJWTForUserID . DB.userID

noRegistrationError :: WebErrorHandler m => m a
noRegistrationError = handle $ WebError "Registration disabled in LDAP mode"

register :: (Carrier t m) => Text -> Text -> t (Context m) Token
register u p = liftCT hasLDAP >>= bool (registerPassword u p) noRegistrationError

changePassword :: (Carrier t m) => DB.User -> Text -> Text -> t (Context m) ()
changePassword u old new =
  maybe
    (handle $ WebError "Changing password disabled in LDAP mode")
    (\expectedHash -> if validateHashedPassword expectedHash old then pure () else handle $ WebError "Wrong password")
    (DB.userPasswordHash u)
    >> updatePassword (DB.userID u) new
