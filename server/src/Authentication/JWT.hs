{-# LANGUAGE OverloadedStrings #-}

module Authentication.JWT
  ( Token,
    toText,
    fromText,
    makeJWT,
    verifyJWT,
  )
where

import Control.Monad (guard)
import Data.Aeson.Types (Value (Number))
import qualified Data.Map as Map
import Data.Scientific (coefficient)
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8)
import Data.Time.Clock (UTCTime)
import Data.Time.Clock.POSIX
import Web.JWT (ClaimsMap (ClaimsMap, unClaimsMap), JWTClaimsSet (exp, iss, unregisteredClaims), VerifySigner (VerifyHMACSecret), claims, decode, encodeSigned, hmacSecret, numericDate, stringOrURI, verify)
import Prelude hiding (exp)

-------------------------------------------------------------------------------
type SecretKey = Text

newtype Token = Token {toText :: Text}

fromText :: Text -> Token
fromText = Token

userIDKey :: Text
userIDKey = "USERID"

-- |

-------------------------------------------------------------------------------
makeJWT :: UTCTime -> SecretKey -> Int -> Token
makeJWT currentTime secret userID =
  let cs =
        mempty -- mempty returns a default JWTClaimsSet
          { iss = stringOrURI "webhaskell",
            unregisteredClaims =
              ClaimsMap $
                Map.fromList [(userIDKey, Number $ fromIntegral userID)],
            exp =
              numericDate $
                utcTimeToPOSIXSeconds currentTime + 30 * posixDayLength
          }
      signer = hmacSecret secret
   in Token $ encodeSigned signer mempty cs

-- |

-------------------------------------------------------------------------------
verifyJWT :: UTCTime -> SecretKey -> Token -> Maybe Int
verifyJWT currentTime secret token = do
  let signer = VerifyHMACSecret $ encodeUtf8 secret
  unverifiedJWT <- decode $ toText token
  verifiedJWT <- verify signer unverifiedJWT
  expTime <- exp . claims $ verifiedJWT
  now <- numericDate $ utcTimeToPOSIXSeconds currentTime
  guard (now < expTime)
  let kv = unClaimsMap . unregisteredClaims . claims $ verifiedJWT
  userIDVal <- Map.lookup userIDKey kv
  case userIDVal of
    Number userID -> return . fromIntegral $ coefficient userID
    _ -> Nothing
