module Config where

import Control.Monad.Except (ExceptT (..), throwError)
import Control.Monad.IO.Class (liftIO)
import Data.ByteString (ByteString)
import Data.Pool
import Data.Text (Text)
import qualified Data.Text as T
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.URL
import GHC.Generics
import LoadEnv
import System.Envy

-- |

-------------------------------------------------------------------------------
data LDAPConfig = LDAPConfig
  { server :: String,
    port :: Int,
    baseDn :: Text,
    bindDn :: Text,
    bindPassword :: ByteString
  }
  deriving (Generic, Show)

fmap' :: (Functor f1, Functor f2) => (a -> b) -> f1 (f2 a) -> f1 (f2 b)
fmap' f x = (f <$>) <$> x

ap' :: (Applicative f1, Applicative f2) => f2 (f1 (a -> b)) -> f2 (f1 a) -> f2 (f1 b)
ap' f x = (<*>) <$> f <*> x

instance FromEnv (Maybe LDAPConfig) where
  fromEnv _ =
    LDAPConfig
      `fmap'` envMaybe "LDAP_SERVER"
      `ap'` envMaybe "LDAP_PORT"
      `ap'` envMaybe "LDAP_BASE_DN"
      `ap'` envMaybe "LDAP_BIND_DN"
      `ap'` envMaybe "LDAP_BIND_PASSWORD"

data Config = Config
  { databaseUrl :: Text,
    jwtSecret :: Text,
    ldap :: Maybe LDAPConfig
  }
  deriving (Generic, Show)

instance DefConfig Config where
  defConfig =
    Config
      { databaseUrl =
          "postgres://Dandoh:dandoh@127.0.0.1:5432/webhaskell?sslmode=disable",
        jwtSecret = "MY_SECRET_KEY",
        ldap = Nothing
      }

instance FromEnv Config where
  fromEnv _ = Config <$> env "DATABASE_URL" <*> env "JWT_SECRET" <*> fromEnv Nothing

-------------------------------------------------------------------------------
type Init a = ExceptT String IO a

loadConfig :: Init Config
loadConfig = ExceptT $ liftIO $ loadEnv >> decodeEnv

createConnectionsPool :: Config -> Init (Pool Connection)
createConnectionsPool config =
  case parseDatabaseUrl . T.unpack . databaseUrl $ config of
    Just connectionInfo ->
      liftIO $ newPool $ setNumStripes (Just 2) $ defaultPoolConfig (connect connectionInfo) close 5.0 10
    _ -> throwError "Invalid database url"

initialize :: Init (Config, Pool Connection)
initialize = do
  config <- loadConfig
  connectionPool <- createConnectionsPool config
  return (config, connectionPool)
