module Application where

import Authentication.JWT
import Config
import Control.Monad.Fraxl (FreerT, evalCachedFraxl)
import Control.Monad.Trans.Except
import Control.Monad.Trans.Reader (runReaderT)
import qualified Data.ByteString.Lazy.Char8 as B
import qualified Data.CaseInsensitive as CI
import Data.Char (isSpace)
import Data.List (find)
import Data.Morpheus (interpreter)
import Data.Morpheus.Types
import Data.Pool
import qualified Data.Text as T
import qualified Data.Text.Lazy as LT
import Data.Time.Clock (getCurrentTime)
import qualified Database.Model as DB
import Database.PostgreSQL.Simple
import Graphql.Resolver.Root
import Graphql.Schema
import Network.HTTP.Types.Status (status200, status405)
import Network.Wai.Middleware.Cors
import Network.Wai.Middleware.Static
import Source (Source, fetchSource)
import Web
import Web.Scotty

-------------------------------------------------------------------------------
api :: B.ByteString -> Web B.ByteString
api a = evalCachedFraxl fetchSource (interpreter (rootResolver :: RootResolver (FreerT Source Web) () Graphql.Schema.Query Mutation Undefined) a)

-------------------------------------------------------------------------------
app :: IO ()
app = do
  initialized <- runExceptT initialize
  case initialized of
    Left err -> putStrLn err
    Right res -> webServer res

extractBearerAuth :: LT.Text -> Maybe LT.Text
extractBearerAuth bs =
  let (x, y) = LT.break isSpace bs
   in if LT.toLower x == "bearer"
        then Just $ LT.dropWhile isSpace y
        else Nothing

resource ::
  (Foldable t, Applicative t) =>
  RoutePattern ->
  t
    ( RoutePattern ->
      ScottyM a
    ) ->
  ScottyM ()
resource r vbs =
  sequence_ (vbs <*> pure r)
    >> matchAny r (status status405 >> Web.Scotty.text "Method Not Allowed")

-------------------------------------------------------------------------------
webServer :: (Config, Pool Connection) -> IO ()
webServer (config, connectionPool) =
  scotty 8080 $ do
    let myCors = cors (const $ Just (simpleCorsResourcePolicy {corsRequestHeaders = ["Content-Type", "Authorization"]}))
    middleware myCors
    resource
      "/api"
      [ \r ->
          post r $ do
            reqBody <- body
            reqHeaders <- headers
            currentTime <- liftIO getCurrentTime
            let currentUserID = case find ((== CI.mk "Authorization") . CI.mk . fst) reqHeaders of
                  Just (_, token) -> DB.ID <$> (verifyJWT currentTime (jwtSecret config) . fromText . T.pack . LT.unpack =<< extractBearerAuth token)
                  _ -> Nothing
            let env = Env connectionPool config currentUserID
            response <- liftIO . flip runReaderT env . runWeb $ api reqBody
            setHeader "Content-Type" "application/json; charset=utf-8"
            status status200
            raw response
      ]
    middleware $ staticPolicy (noDots >-> addBase "static")
    notFound $ setHeader "Content-Type" "text/html; charset=utf-8" >> file "static/index.html"
