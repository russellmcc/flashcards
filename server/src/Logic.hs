module Logic
  ( Context,
    decksForUser,
    findUserByUserName,
    findUserByID,
    createUser,
    updatePassword,
    conceptsForDeck,
    createDeck,
    findDeckByID,
    findConceptByID,
    createConcept,
    aspectsForConcept,
    deleteConcept,
    createAspect,
    findAspectByID,
    deleteAspect,
    testsForAspect,
    deleteTest,
    createTest,
    readyAspectsForDeck,
    scheduleAspect,
    scheduledAspectsForDeck,
    getCurrentTime,
  )
where

import Control.Monad (when)
import Control.Monad.Fraxl (FreerT, dataFetch)
import Control.Monad.Trans (MonadTrans, lift)
import Control.Monad.Trans.Except
import Data.Text (Text)
import Data.Time.Clock (UTCTime)
import Database.Model (AspectID, ConceptID, DeckID, TestID, UserID)
import qualified Database.Model as DB
import Database.Pagination
import Source
import Web (WebError (..), WebErrorHandler (..))

type Context m = FreerT Source m

type Carrier t m =
  ( Monad m,
    Monad (t (Context m)),
    WebErrorHandler (t (Context m)),
    MonadTrans t
  )

liftC :: (Carrier t m) => Context m (Except WebError a) -> t (Context m) a
liftC a = lift a >>= (either handle pure . runExcept)

decksForUser :: (Carrier t m) => UserID -> t (Context m) [DB.Deck]
decksForUser uID = liftC $ dataFetch $ DecksForUser uID

findUserByUserName :: (Carrier t m) => Text -> t (Context m) (Maybe DB.User)
findUserByUserName username = liftC $ dataFetch $ FindUserByUserName username

findUserByID :: (Carrier t m) => UserID -> t (Context m) DB.User
findUserByID uID = liftC $ dataFetch $ FindUserByID uID

createUser :: (Carrier t m) => Text -> Maybe Text -> t (Context m) DB.User
createUser un pw = liftC $ dataFetch $ CreateUser un pw

updatePassword :: (Carrier t m) => UserID -> Text -> t (Context m) ()
updatePassword uID pw = liftC $ dataFetch $ UpdatePassword uID pw

conceptsForDeck :: (Carrier t m) => DeckID -> t (Context m) [DB.Concept]
conceptsForDeck d = liftC $ dataFetch $ ConceptsForDeck d

createDeck :: (Carrier t m) => Text -> UserID -> Bool -> t (Context m) DB.Deck
createDeck name uID public = liftC $ dataFetch $ CreateDeck name uID public

findDeckByID :: (Carrier t m) => DeckID -> t (Context m) DB.Deck
findDeckByID d = liftC $ dataFetch $ FindDeckByID d

findConceptByID :: (Carrier t m) => ConceptID -> t (Context m) DB.Concept
findConceptByID d = liftC $ dataFetch $ FindConceptByID d

createConcept :: (Carrier t m) => UserID -> DeckID -> Text -> t (Context m) DB.Concept
createConcept uID dID n = do
  deck <- findDeckByID dID
  when (DB.deckOwner deck /= DB.toNullableID uID) $ handle $ WebError "Only the owner can modify deck"
  liftC $ dataFetch $ CreateConcept dID n

checkUserCanModifyConcept ::
  (Carrier t m) =>
  UserID ->
  ConceptID ->
  t (Context m) ()
checkUserCanModifyConcept uID cID = do
  concept <- findConceptByID cID
  deck <- findDeckByID (DB.conceptDeckID concept)
  when (DB.deckOwner deck /= DB.toNullableID uID) $ handle $ WebError "Only the owner can modify deck"

deleteAspects :: (Carrier t m) => [AspectID] -> t (Context m) ()
deleteAspects aIDs = liftC $ dataFetch $ DeleteAspects aIDs

deleteTests :: (Carrier t m) => [TestID] -> t (Context m) ()
deleteTests tIDs = liftC $ dataFetch $ DeleteTests tIDs

aspectsForConcept :: (Carrier t m) => ConceptID -> t (Context m) [DB.Aspect]
aspectsForConcept cID = liftC $ dataFetch $ FindAspectsByConceptID cID

deleteConcepts :: (Carrier t m) => [ConceptID] -> t (Context m) ()
deleteConcepts cIDs =
  mapM_ deleteAspectsFor cIDs >> liftC (dataFetch $ DeleteConcepts cIDs)
  where
    deleteAspectsFor :: (Carrier t m) => ConceptID -> t (Context m) ()
    deleteAspectsFor cID = do
      aspects <- aspectsForConcept cID
      deleteAspects (DB.aspectID <$> aspects)

deleteConcept :: (Carrier t m) => UserID -> ConceptID -> t (Context m) ()
deleteConcept uID cID = do
  checkUserCanModifyConcept uID cID
  deleteConcepts [cID]

createAspect :: (Carrier t m) => UserID -> ConceptID -> Text -> t (Context m) DB.Aspect
createAspect uID cID n = do
  checkUserCanModifyConcept uID cID
  liftC $ dataFetch $ CreateAspect cID n

findAspectByID :: (Carrier t m) => AspectID -> t (Context m) DB.Aspect
findAspectByID aID = liftC $ dataFetch $ FindAspectByID aID

findTestByID :: (Carrier t m) => TestID -> t (Context m) DB.Test
findTestByID tID = liftC $ dataFetch $ FindTestByID tID

checkUserCanModifyAspect ::
  (Carrier t m) =>
  UserID ->
  AspectID ->
  t (Context m) ()
checkUserCanModifyAspect uID aID = do
  aspect <- findAspectByID aID
  checkUserCanModifyConcept uID (DB.aspectConceptID aspect)

checkUserCanModifyTest ::
  (Carrier t m) =>
  UserID ->
  TestID ->
  t (Context m) ()
checkUserCanModifyTest uID tID = do
  test <- findTestByID tID
  checkUserCanModifyAspect uID (DB.testAspectID test)

deleteAspect :: (Carrier t m) => UserID -> AspectID -> t (Context m) ()
deleteAspect uID aID = do
  checkUserCanModifyAspect uID aID
  deleteAspects [aID]

testsForAspect :: (Carrier t m) => AspectID -> t (Context m) [DB.Test]
testsForAspect aID = liftC $ dataFetch $ FindTestsByAspectID aID

createTest :: (Carrier t m) => UserID -> AspectID -> Text -> DB.TestData -> t (Context m) DB.Test
createTest uID aID n d = do
  checkUserCanModifyAspect uID aID
  liftC $ dataFetch $ CreateTest aID n d

deleteTest :: (Carrier t m) => UserID -> TestID -> t (Context m) ()
deleteTest uID tID = do
  checkUserCanModifyTest uID tID
  deleteTests [tID]

scheduledAspectsForDeck ::
  (Carrier t m) =>
  UserID ->
  DeckID ->
  PaginationArgs (UTCTime, AspectID) ->
  t (Context m) (Paginated (UTCTime, AspectID) DB.ScheduledAspect)
scheduledAspectsForDeck u d pa = liftC $ dataFetch $ ScheduledAspectsForDeck u d pa

getCurrentTime :: (Carrier t m) => t (Context m) UTCTime
getCurrentTime = lift $ dataFetch CurrentTime

readyAspectsForDeck :: (Carrier t m) => UserID -> DeckID -> t (Context m) [DB.Aspect]
readyAspectsForDeck uID dID =
  getCurrentTime >>= \now -> liftC $ dataFetch $ ReadyAspectsForTime now uID dID

scheduleAspect :: (Carrier t m) => DB.ScheduledAspect -> UTCTime -> t (Context m) DB.ScheduledAspect
scheduleAspect sa@DB.ScheduledAspect {scheduledAspectUserID, scheduledAspectAspectID} schedDate = do
  aspect <- findAspectByID scheduledAspectAspectID
  concept <- findConceptByID . DB.aspectConceptID $ aspect
  accessible <- liftC $ dataFetch $ DeckAccessible scheduledAspectUserID (DB.conceptDeckID concept)
  when accessible $ handle $ WebError "Can't schedule aspect not in your decks"
  liftC $ dataFetch $ ScheduleAspect sa schedDate
