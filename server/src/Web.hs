{-# LANGUAGE UndecidableInstances #-}

module Web
  ( Env (..),
    Web (..),
    MonadAppContextOp (..),
    handleWebExcept,
    WebExcept,
    WebErrorHandler (..),
    WebError (..),
  )
where

import Authentication.JWT
import Config
import Control.Exception ()
import Control.Monad.Base (MonadBase)
import Control.Monad.Catch
import Control.Monad.Reader (MonadReader, ReaderT, asks)
import Control.Monad.Trans (MonadIO, MonadTrans, liftIO)
import Control.Monad.Trans.Control
import Control.Monad.Trans.Except
import Data.Either (fromRight)
import qualified Data.Maybe
import Data.Morpheus.Types
import Data.Pool (Pool)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import qualified Data.Time.Clock as DTC
import qualified Database.Model as DB
import Database.PostgreSQL.Simple (Connection)
import qualified Ldap.Client as Ldap

data Env = Env
  { dbPool :: Pool Connection,
    config :: Config,
    currentUserID :: Maybe DB.UserID
  }

newtype WebError = WebError {runWebError :: T.Text}

class WebErrorHandler m where
  handle :: forall a. WebError -> m a

newtype Web a = Web
  { runWeb :: ReaderT Env IO a
  }
  deriving newtype
    ( Functor,
      Applicative,
      Monad,
      MonadReader Env,
      MonadThrow,
      MonadCatch,
      MonadIO,
      MonadBase IO,
      MonadBaseControl IO
    )

type WebExcept m a = ExceptT WebError m a

handleWebExcept :: (Monad m, MonadTrans t, Monad (t m), WebErrorHandler (t m)) => WebExcept m a -> t m a
handleWebExcept x = lift (runExceptT x) >>= either Web.handle pure

class Monad m => MonadAppContextOp m where
  makeJWTForUserID :: DB.UserID -> m Token
  authorizedUserID :: m (Maybe DB.UserID)
  verifyLDAPPassword :: T.Text -> T.Text -> m Bool
  hasLDAP :: m Bool

instance (MonadFail m) => WebErrorHandler m where
  handle = fail . T.unpack . runWebError

makeJWTForUserID' :: DB.UserID -> Web Token
makeJWTForUserID' userID = do
  time <- liftIO DTC.getCurrentTime
  secret <- asks (jwtSecret . Web.config)
  pure $ makeJWT time secret (DB.getID userID)

withLDAP :: (LDAPConfig -> Ldap.Ldap -> IO a) -> Web (Either () a)
withLDAP x =
  runExceptT $
    (maybe (throwE ()) pure =<< asks (Config.ldap . Web.config)) >>= \c@LDAPConfig {server, port} ->
      either (const $ throwE ()) pure =<< liftIO (Ldap.with (Ldap.Plain server) (fromInteger $ toInteger port) (x c))

verifyLDAPPassword' :: T.Text -> T.Text -> Web Bool
verifyLDAPPassword' uname pass =
  fromRight False
    <$> withLDAP
      ( \LDAPConfig {baseDn, bindDn, bindPassword} ld ->
          Ldap.bind ld (Ldap.Dn bindDn) (Ldap.Password bindPassword)
            >> Ldap.search
              ld
              (Ldap.Dn baseDn)
              mempty
              searchFilter
              []
              >>= handleSearchResults ld
      )
  where
    searchFilter =
      Ldap.And
        ( pure (Ldap.Attr "objectclass" Ldap.:= "user")
            <> pure
              ( Ldap.Or
                  ( pure (Ldap.Attr "username" Ldap.:= encodeUtf8 uname)
                      <> pure (Ldap.Attr "mail" Ldap.:= encodeUtf8 uname)
                  )
              )
        )
    handleSearchResults ld ((Ldap.SearchEntry dn _) : _) = Ldap.bind ld dn (Ldap.Password $ encodeUtf8 pass) >> pure True
    handleSearchResults _ _ = pure False

instance MonadAppContextOp Web where
  makeJWTForUserID = makeJWTForUserID'
  authorizedUserID = asks currentUserID
  verifyLDAPPassword = verifyLDAPPassword'
  hasLDAP = asks (Data.Maybe.isJust . Config.ldap . Web.config)
