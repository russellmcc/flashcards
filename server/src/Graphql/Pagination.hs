module Graphql.Pagination where

import Data.Text (Text, pack, splitOn, unpack)
import Data.Text.Encoding.Base64
import Data.Text.Read (decimal)
import Data.Time.Clock (UTCTime)
import Data.Time.Format.ISO8601 (iso8601ParseM, iso8601Show)
import qualified Database.Model as DB
import Database.Pagination
import qualified Graphql.Schema as GQL

newtype FailEither a = FailEither {runFailEither :: Either String a}
  deriving newtype
    ( Functor,
      Applicative,
      Monad
    )

instance MonadFail FailEither where
  fail x = FailEither $ Left x

class IsCursor a where
  fromText :: Text -> Maybe a
  toText :: a -> Text

instance IsCursor (UTCTime, DB.AspectID) where
  fromText = go . splitOn " "
    where
      go [t, a] =
        (,)
          <$> either (pure Nothing) Just ((runFailEither . iso8601ParseM) $ unpack t)
          <*> either (pure Nothing) (Just . DB.ID . fst) (decimal a)
      go _ = Nothing
  toText (t, a) = pack (iso8601Show t ++ " " ++ show (DB.getID a))

fromText' :: (IsCursor k) => Text -> Maybe k
fromText' = either (pure Nothing) fromText . decodeBase64

toText' :: (IsCursor k) => k -> Text
toText' = encodeBase64 . toText

toCursor :: (IsCursor k) => Maybe (Cursor k) -> GQL.Cursor
toCursor = GQL.Cursor . maybe "" (toText' . runCursor)

toPageInfo :: (IsCursor k, Applicative f) => PageInfo k -> GQL.PageInfo f
toPageInfo PageInfo {hasPreviousPage, hasNextPage, startCursor, endCursor} =
  GQL.PageInfo
    { hasPreviousPage = pure hasPreviousPage,
      hasNextPage = pure hasNextPage,
      startCursor = pure $ toCursor startCursor,
      endCursor = pure $ toCursor endCursor
    }

toPaginationArgs :: (IsCursor k) => Maybe GQL.Cursor -> Maybe Int -> PaginationArgs k
toPaginationArgs after = PaginationArgs (after >>= ((Cursor <$>) . fromText' . GQL.runCursor))
