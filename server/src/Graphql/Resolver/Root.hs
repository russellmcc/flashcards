{-# LANGUAGE BlockArguments #-}

module Graphql.Resolver.Root
  ( rootResolver,
  )
where

import qualified Authentication.JWT as JWT
import Authentication.Logic
import Control.Monad.Trans (MonadTrans)
import Control.Monad.Trans.Except
import Data.Functor ((<&>))
import Data.Morpheus.Types
import Data.Text.Read (decimal)
import Data.Time.Clock (UTCTime)
import Database.Model (AspectID, ConceptID, DeckID, TestID, UserID)
import qualified Database.Model as DB
import Database.Pagination as DBP
import Graphql
import Graphql.Pagination
import Graphql.Schema
import Logic
import TextShow
import Util (bind2, bind3)
import Web

tokenToSession :: (Applicative m) => JWT.Token -> Session m
tokenToSession = Session . pure . ID . JWT.toText

class IsID i where
  toID :: (Applicative m) => i -> m ID
  fromID :: (MonadFail m) => ID -> m i

instance IsID Int where
  toID = pure . ID . showt
  fromID (ID i) = either (pure throwInvalid) extract (decimal i)
    where
      throwInvalid = fail "Invalid ID"
      extract (x, "") = pure x
      extract _ = throwInvalid

instance IsID AspectID where
  toID = toID . DB.getID
  fromID = (DB.ID <$>) . fromID

instance IsID TestID where
  toID = toID . DB.getID
  fromID = (DB.ID <$>) . fromID

instance IsID ConceptID where
  toID = toID . DB.getID
  fromID = (DB.ID <$>) . fromID

instance IsID DeckID where
  toID = toID . DB.getID
  fromID = (DB.ID <$>) . fromID

instance IsID UserID where
  toID = toID . DB.getID
  fromID = (DB.ID <$>) . fromID

type Context' m = ExceptT WebError (Context m)

liftC' :: (Monad m) => WebExcept m a -> Context' m a
liftC' a = lift (lift $ runExceptT a) >>= either throwE pure

liftT ::
  ( Monad m,
    Monad (t (Context m)),
    WebErrorHandler (t (Context m)),
    MonadTrans t
  ) =>
  WebExcept m a ->
  t (Context m) a
liftT = handleWebExcept . liftC'

requireAuthorizedUserID' :: (MonadAppContextOp m) => WebExcept m UserID
requireAuthorizedUserID' = lift authorizedUserID >>= maybe (throwE $ WebError "Unauthorized") pure

requireAuthorizedUserID :: (GraphQL o m) => Resolver o () (Context m) UserID
requireAuthorizedUserID = liftT requireAuthorizedUserID'

rootResolver :: (MonadAppContextOp m) => RootResolver (Context m) () Query Mutation Undefined
rootResolver =
  defaultRootResolver {queryResolver, mutationResolver}
  where
    queryResolver = Query {currentUser = currentUserResolver}
    mutationResolver =
      Mutation
        { login = loginResolver,
          register = registerResolver,
          changePassword = changePasswordResolver,
          createDeck = createDeckResolver,
          createConcept = createConceptResolver,
          deleteConcept = deleteConceptResolver,
          createAspect = createAspectResolver,
          deleteAspect = deleteAspectResolver,
          createSelfGradedPictionaryTest = createSelfGradedPictionaryTestResolver,
          createEntryTest = createEntryTestResolver,
          deleteTest = deleteTestResolver,
          scheduleAspectLearning = scheduleAspectLearningResolver,
          scheduleAspectRelearning = scheduleAspectRelearningResolver,
          scheduleAspectReview = scheduleAspectReviewResolver
        }

userResolver :: GraphQL o m => DB.User -> Object o (Context m) User
userResolver user =
  let DB.User {userID, userName, userProfile = _} = user
   in pure
        User
          { id = toID userID,
            username = pure userName,
            decks = decksForUser userID >>= traverse userDeckDataResolver
          }

loginResolverInternal ::
  GraphQL MUTATION m =>
  Arg "input" LoginInput ->
  Object MUTATION (Context m) Session
loginResolverInternal (Arg LoginInput {username, password}) = tokenToSession <$> Authentication.Logic.login username password

loginResolver ::
  GraphQL MUTATION m =>
  Arg "input" LoginInput ->
  Object MUTATION (Context m) LoginPayload
loginResolver a = loginResolverInternal a <&> (LoginPayload . pure)

---------------------------------------------------------------------------------

registerResolver :: (GraphQL MUTATION m) => Arg "input" RegisterInput -> Object MUTATION (Context m) RegisterPayload
registerResolver (Arg RegisterInput {username, password}) =
  RegisterPayload . pure . tokenToSession <$> Authentication.Logic.register username password

-- -------------------------------------------------------------------------------
currentUserResolver :: (GraphQL QUERY m) => Object QUERY (Context m) User
currentUserResolver = do
  u <- requireAuthorizedUserID
  findUserByID u >>= userResolver

---------------------------------------------------------------------------------

changePasswordResolver :: (GraphQL MUTATION m) => Arg "input" ChangePasswordInput -> Object MUTATION (Context m) ChangePasswordPayload
changePasswordResolver (Arg ChangePasswordInput {oldPassword, newPassword}) =
  ChangePasswordPayload <$> do
    uID <- requireAuthorizedUserID
    user <- findUserByID uID
    Authentication.Logic.changePassword user oldPassword newPassword
    pure . pure $ True

userDeckDataResolver :: (GraphQL o m) => DB.Deck -> Object o (Context m) UserDeckData
userDeckDataResolver deck = do
  uID <- requireAuthorizedUserID
  pure $
    UserDeckData
      { deck = deckResolver deck,
        scheduledAspects = \ScheduledAspectsArgs {first, after} ->
          scheduledAspectsForDeck uID (DB.deckID deck) (toPaginationArgs after first) >>= scheduledAspectConnectionResolver,
        readyAspects = readyAspectsForDeck uID (DB.deckID deck) >>= traverse readyAspectResolver
      }

-- ---------------------------------------------------------------------------------------

deckResolver :: (GraphQL o m) => DB.Deck -> Object o (Context m) Deck
deckResolver deck =
  let DB.Deck {deckID, deckName, deckOwner, deckMetadata = _, deckPublic} = deck
      resolveUser uID = findUserByID uID >>= (fmap Just . userResolver)
   in pure
        Deck
          { id = toID deckID,
            name = pure deckName,
            public = pure deckPublic,
            owner = maybe (pure Nothing) resolveUser (DB.liftID deckOwner),
            concepts = conceptsForDeck deckID >>= traverse conceptResolver
          }

createDeckResolver :: (GraphQL MUTATION m) => Arg "input" CreateDeckInput -> Object MUTATION (Context m) CreateDeckPayload
createDeckResolver (Arg CreateDeckInput {name}) = pure $
  CreateDeckPayload $ do
    uID <- requireAuthorizedUserID
    deck <- Logic.createDeck name uID False
    deckResolver deck

-- ---------------------------------------------------------------------------------------

conceptIDResolver :: (GraphQL o m) => ConceptID -> Object o (Context m) Concept
conceptIDResolver cID =
  pure
    Concept
      { id = toID cID,
        name = DB.conceptName <$> findConceptByID cID,
        deck = findConceptByID cID >>= findDeckByID . DB.conceptDeckID >>= deckResolver,
        aspects = aspectsForConcept cID >>= traverse aspectResolver
      }

conceptResolver :: (GraphQL o m) => DB.Concept -> Object o (Context m) Concept
conceptResolver concept =
  let DB.Concept {conceptID, conceptName, conceptDeckID, conceptInfo = _} = concept
   in pure
        Concept
          { id = toID conceptID,
            name = pure conceptName,
            deck = findDeckByID conceptDeckID >>= deckResolver,
            aspects = aspectsForConcept conceptID >>= traverse aspectResolver
          }

createConceptResolver :: (GraphQL o m) => Arg "input" CreateConceptInput -> Object o (Context m) CreateConceptPayload
createConceptResolver (Arg CreateConceptInput {deck = deckID, name}) =
  bind3 Logic.createConcept requireAuthorizedUserID (fromID deckID) (pure name)
    >>= ( \concept ->
            pure $
              CreateConceptPayload
                { concept = conceptResolver concept
                }
        )

deleteConceptResolver :: (GraphQL o m) => Arg "input" DeleteConceptInput -> Object o (Context m) DeleteConceptPayload
deleteConceptResolver (Arg DeleteConceptInput {concept = conceptID}) =
  bind2 Logic.deleteConcept requireAuthorizedUserID (fromID conceptID)
    >>= pure
      ( pure $
          DeleteConceptPayload
            { deletedConcept = pure conceptID
            }
      )

-- ---------------------------------------------------------------------------------------

aspectResolver :: (GraphQL o m) => DB.Aspect -> Object o (Context m) Aspect
aspectResolver aspect =
  let DB.Aspect {aspectID, aspectName, aspectConceptID, aspectInfo = _} = aspect
   in pure
        Aspect
          { id = toID aspectID,
            name = pure aspectName,
            concept = conceptIDResolver aspectConceptID,
            tests = testsForAspect aspectID >>= traverse testResolver
          }

aspectIDResolver :: (GraphQL o m) => AspectID -> Object o (Context m) Aspect
aspectIDResolver aID =
  pure
    Aspect
      { id = toID aID,
        name = DB.aspectName <$> findAspectByID aID,
        concept = findAspectByID aID >>= conceptIDResolver . DB.aspectConceptID,
        tests = testsForAspect aID >>= traverse testResolver
      }

createAspectResolver :: (GraphQL o m) => Arg "input" CreateAspectInput -> Object o (Context m) CreateAspectPayload
createAspectResolver (Arg CreateAspectInput {concept = conceptID, name}) =
  bind3 Logic.createAspect requireAuthorizedUserID (fromID conceptID) (pure name)
    >>= ( \aspect ->
            pure $
              CreateAspectPayload
                { aspect = aspectResolver aspect
                }
        )

deleteAspectResolver :: (GraphQL o m) => Arg "input" DeleteAspectInput -> Object o (Context m) DeleteAspectPayload
deleteAspectResolver (Arg DeleteAspectInput {aspect = aspectID}) =
  bind2 Logic.deleteAspect requireAuthorizedUserID (fromID aspectID)
    >>= pure
      ( pure $
          DeleteAspectPayload
            { deletedAspect = pure aspectID
            }
      )

-- ---------------------------------------------------------------------------------------

toGQLTestData :: (Applicative m) => DB.TestData -> TestData m
toGQLTestData (DB.SelfGradedPictionaryTestData DB.SelfGradedPictionaryData {sgFront, sgBack}) =
  TestDataSelfGradedPictionaryTestData $
    pure $
      SelfGradedPictionaryTestData
        { front = pure sgFront,
          back = pure sgBack
        }
toGQLTestData (DB.EntryTestData DB.EntryData {agFront, agBack, agWrongs, agAutoGrade}) =
  TestDataEntryTestData $
    pure $
      EntryTestData
        { front = pure agFront,
          back = pure agBack,
          wrongs = pure agWrongs,
          autoGrade = pure agAutoGrade
        }

class ToDBTestData a where
  toDBTestData :: a -> DB.TestData

instance ToDBTestData SelfGradedPictionaryTestDataInput where
  toDBTestData SelfGradedPictionaryTestDataInput {front, back} =
    DB.SelfGradedPictionaryTestData $
      DB.SelfGradedPictionaryData
        { sgFront = front,
          sgBack = back
        }

instance ToDBTestData EntryTestDataInput where
  toDBTestData EntryTestDataInput {front, back, wrongs, autoGrade} =
    DB.EntryTestData $ DB.EntryData {agFront = front, agBack = back, agWrongs = wrongs, agAutoGrade = autoGrade}

testResolver :: (GraphQL o m) => DB.Test -> Object o (Context m) Test
testResolver test =
  let DB.Test {testID, testName, testAspectID, testData} = test
   in pure
        Test
          { id = toID testID,
            name = pure testName,
            aspect = aspectIDResolver testAspectID,
            testData = pure $ toGQLTestData testData
          }

createSelfGradedPictionaryTestResolver :: (GraphQL o m) => Arg "input" CreateSelfGradedPictionaryTestInput -> Object o (Context m) CreateSelfGradedPictionaryTestPayload
createSelfGradedPictionaryTestResolver (Arg CreateSelfGradedPictionaryTestInput {aspect = aspectID, name, testData}) = do
  uID <- requireAuthorizedUserID
  aID <- fromID aspectID
  Logic.createTest uID aID name (toDBTestData testData) <&> (CreateSelfGradedPictionaryTestPayload . testResolver)

createEntryTestResolver :: (GraphQL o m) => Arg "input" CreateEntryTestInput -> Object o (Context m) CreateEntryTestPayload
createEntryTestResolver (Arg CreateEntryTestInput {aspect = aspectID, name, testData}) = do
  uID <- requireAuthorizedUserID
  aID <- fromID aspectID
  Logic.createTest uID aID name (toDBTestData testData) <&> (CreateEntryTestPayload . testResolver)

deleteTestResolver :: (GraphQL o m) => Arg "input" DeleteTestInput -> Object o (Context m) DeleteTestPayload
deleteTestResolver (Arg DeleteTestInput {test = testID}) =
  bind2 Logic.deleteTest requireAuthorizedUserID (fromID testID)
    >>= pure
      ( pure $
          DeleteTestPayload
            { deletedTest = pure testID
            }
      )

-- ---------------------------------------------------------------------------------------

toGQLSchedData :: (Applicative m) => DB.Sched -> Schedule m
toGQLSchedData (DB.SchedLearning DB.LearningSched {learnStep}) =
  ScheduleLearningSchedule $
    pure $
      LearningSchedule
        { step = pure learnStep
        }
toGQLSchedData (DB.SchedRelearning DB.RelearningSched {relearnStep, relearnFactor}) =
  ScheduleRelearningSchedule $
    pure $
      RelearningSchedule
        { step = pure relearnStep,
          factor = pure $ realToFrac relearnFactor
        }
toGQLSchedData (DB.SchedReview DB.ReviewSched {reviewFactor, reviewStudiedDate}) =
  ScheduleReviewSchedule $
    pure $
      ReviewSchedule
        { factor = pure $ realToFrac reviewFactor,
          studiedDate = pure $ Date reviewStudiedDate
        }

scheduledAspectEdgeResolver :: (GraphQL o m) => DBP.Item (UTCTime, AspectID) DB.ScheduledAspect -> Object o (Context m) ScheduledAspectEdge
scheduledAspectEdgeResolver DBP.Item {value, cursor} =
  pure
    ScheduledAspectEdge
      { node = scheduledAspectResolver value,
        cursor = pure $ toCursor (pure cursor)
      }

scheduledAspectConnectionResolver ::
  (GraphQL o m) =>
  DBP.Paginated (UTCTime, AspectID) DB.ScheduledAspect ->
  Object o (Context m) ScheduledAspectConnection
scheduledAspectConnectionResolver DBP.Paginated {items, pageInfo} =
  pure
    ScheduledAspectConnection
      { pageInfo = pure $ toPageInfo pageInfo,
        edges = traverse scheduledAspectEdgeResolver items
      }

scheduledAspectResolver :: (GraphQL o m) => DB.ScheduledAspect -> Object o (Context m) ScheduledAspect
scheduledAspectResolver scheduledAspect =
  let DB.ScheduledAspect {scheduledAspectAspectID, scheduledAspectSched, scheduledAspectDueDate} = scheduledAspect
      gqlSched = toGQLSchedData scheduledAspectSched
   in pure
        ScheduledAspect
          { aspect = aspectIDResolver scheduledAspectAspectID,
            schedule = pure gqlSched,
            dueDate = pure $ Date scheduledAspectDueDate
          }

readyAspectResolver :: (GraphQL o m) => DB.Aspect -> Object o (Context m) ReadyAspect
readyAspectResolver aspect =
  pure
    ReadyAspect
      { aspect = aspectResolver aspect
      }

scheduleAspectResolver :: (GraphQL o m) => ID -> Date -> UTCTime -> DB.Sched -> Object o (Context m) ScheduledAspect
scheduleAspectResolver aspectID date schedDate sched = do
  uID <- requireAuthorizedUserID
  aID <- fromID aspectID
  Logic.scheduleAspect
    ( DB.ScheduledAspect
        { scheduledAspectUserID = uID,
          scheduledAspectAspectID = aID,
          scheduledAspectDueDate = runDate date,
          scheduledAspectSched = sched
        }
    )
    schedDate
    >>= scheduledAspectResolver

resolveDate :: (GraphQL o m) => Maybe Date -> Resolver o () (Context m) UTCTime
resolveDate = maybe Logic.getCurrentTime (pure . runDate)

scheduleAspectLearningResolver :: (GraphQL o m) => Arg "input" ScheduleAspectLearningInput -> Object o (Context m) ScheduleAspectLearningPayload
scheduleAspectLearningResolver (Arg ScheduleAspectLearningInput {aspect = aspectID, step, dueDate, scheduledDate}) =
  resolveDate scheduledDate >>= \schedDate ->
    scheduleAspectResolver aspectID dueDate schedDate (DB.SchedLearning DB.LearningSched {learnStep = step}) <&> (ScheduleAspectLearningPayload . pure)

scheduleAspectRelearningResolver :: (GraphQL o m) => Arg "input" ScheduleAspectRelearningInput -> Object o (Context m) ScheduleAspectRelearningPayload
scheduleAspectRelearningResolver (Arg ScheduleAspectRelearningInput {aspect = aspectID, step, dueDate, scheduledDate, factor}) =
  resolveDate scheduledDate >>= \schedDate ->
    scheduleAspectResolver
      aspectID
      dueDate
      schedDate
      ( DB.SchedRelearning
          DB.RelearningSched
            { relearnStep = step,
              relearnFactor = factor
            }
      )
      <&> (ScheduleAspectRelearningPayload . pure)

scheduleAspectReviewResolver :: (GraphQL o m) => Arg "input" ScheduleAspectReviewInput -> Object o (Context m) ScheduleAspectReviewPayload
scheduleAspectReviewResolver (Arg ScheduleAspectReviewInput {aspect = aspectID, studiedDate, dueDate, scheduledDate, factor}) =
  resolveDate scheduledDate >>= \schedDate ->
    scheduleAspectResolver
      aspectID
      dueDate
      schedDate
      ( DB.SchedReview
          DB.ReviewSched
            { reviewStudiedDate = runDate studiedDate,
              reviewFactor = factor
            }
      )
      <&> (ScheduleAspectReviewPayload . pure)
