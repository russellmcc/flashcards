{-# OPTIONS_GHC -Wno-name-shadowing #-}
{-# OPTIONS_GHC -Wno-redundant-constraints #-}

module Graphql.Schema where

import Data.Either.Combinators (mapLeft)
import Data.Morpheus.Document
import Data.Morpheus.Kind
import Data.Morpheus.Types
import Data.Text (Text, pack, unpack)
import Data.Time.Clock (UTCTime)
import Data.Time.Format.ISO8601 (iso8601ParseM, iso8601Show)
import GHC.Generics

newtype Date = Date {runDate :: UTCTime} deriving (Generic, Show)

newtype FailEither a = FailEither {runFailEither :: Either String a}
  deriving newtype
    ( Functor,
      Applicative,
      Monad
    )

newtype Cursor = Cursor {runCursor :: Text} deriving (Generic, Show)

instance DecodeScalar Cursor where
  decodeScalar (String x) = Right $ Cursor x
  decodeScalar _ = Left "Not a valid cursor."

instance EncodeScalar Cursor where
  encodeScalar (Cursor x) = String x

instance GQLType Cursor where
  type KIND Cursor = SCALAR

instance MonadFail FailEither where
  fail x = FailEither $ Left x

instance DecodeScalar Date where
  decodeScalar (String x) = Date <$> mapLeft pack (runFailEither . iso8601ParseM . unpack $ x)
  decodeScalar _ = Left "Not a valid date."

instance EncodeScalar Date where
  encodeScalar (Date t) = String . pack $ iso8601Show t

instance GQLType Date where
  type KIND Date = SCALAR

importGQLDocument "../graphql/schema.gql"
