module Database.Utils where

import Data.Bifunctor (bimap)
import Data.Profunctor.Product.Default (Default)
import GHC.Int (Int32)
import Opaleye
import Opaleye.Internal.QueryArr (Query)
import Opaleye.Values ()

zippies :: (Default ToFields a b, Default Valuesspec b b) => [a] -> Query (b, Column SqlInt4)
zippies as = values $ (bimap toFields toFields <$>) $ zip as ([0 ..] :: [Int32])
