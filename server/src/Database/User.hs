module Database.User where

import Control.Arrow (returnA)
import Data.Text (Text)
import Database.Model
import GHC.Int (Int64)
import Data.Aeson.Types (emptyObject)
import Opaleye
import Database.Utils
-------------------------------------------------------------------------------
userSelect :: Select UserFR
userSelect = selectTable userTable

-------------------------------------------------------------------------------
insertUser :: (Text, Maybe Text) -> Insert [User]
insertUser (userName, userPasswordHash) =
  Insert
    { iTable = userTable,
      iRows =
          [ UserF
              { userFID = ID Nothing,
                userFName = toFields userName,
                userFPasswordHash = toFields userPasswordHash,
                userFProfile = toFields emptyObject
              }
          ],
      iReturning = rReturning id,
      iOnConflict = Nothing
    }

-------------------------------------------------------------------------------
findUsersByUsernames :: [Text] -> Select (UserFR, Column SqlInt4)
findUsersByUsernames names = orderBy (asc snd) $
  proc () -> do
    user <- userSelect -< ()
    (userNames, idx) <- zippies names -< ()
    restrict -< userFName user .== userNames
    returnA -< (user, idx)

-------------------------------------------------------------------------------
findUsersByIDs :: [UserID] -> Select (UserFR, Column SqlInt4)
findUsersByIDs uIDs_ = orderBy (asc snd) $ proc () -> do
  user <- userSelect -< ()
  (uIDs, idx) <- zippies uIDs_ -< ()
  restrict -< userFID user .=== uIDs
  returnA -< (user, idx)

-------------------------------------------------------------------------------
updateUserPassword :: UserID -> Text -> Update Int64
updateUserPassword uID newPasswordHash =
  Update
    { uTable = userTable,
      uUpdateWith = updateEasy updatePasswordHash,
      uWhere = (toFields uID .===) . userFID,
      uReturning = rCount
    }
  where
    updatePasswordHash userData =
      userData {
        userFPasswordHash = toFields $ Just newPasswordHash
      }
