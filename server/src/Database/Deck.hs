module Database.Deck where

import Control.Arrow (returnA)
import Data.Text (Text)
import Database.Model
import Data.Aeson.Types (emptyObject)
import Opaleye
import GHC.Int (Int64)
import Database.Utils

deckSelect :: Select DeckFR
deckSelect = selectTable deckTable

insertDeck :: (Text, UserID, Bool) -> Insert [Deck]
insertDeck (name, owner, public) =
  Insert
    { iTable = deckTable,
      iRows =
          [ DeckF
              { deckFID = ID Nothing,
                deckFName = toFields name,
                deckFOwner = ownerField,
                deckFPublic = toFields public,
                deckFMetadata = toFields emptyObject
              }
          ],
      iReturning = rReturning id,
      iOnConflict = Nothing
    } where
      ownerJustId :: IDT 'UserIDT (Maybe Int)
      ownerJustId = ID . Just . getID $ owner
      ownerField = toFields ownerJustId

insertDeckUser :: (UserID, DeckID) -> Insert Int64
insertDeckUser (uID, dID) =
  Insert
    { iTable = deckUserTable,
      iRows =
          [ DeckUserF
              { deckUserFDeckID = toFields dID,
                deckUserFUserID = toFields uID
              }
          ],
      iReturning = rCount,
      iOnConflict = Nothing
    }

findDecksByIDs :: [DeckID] -> Select (DeckFR, Column SqlInt4)
findDecksByIDs dIDs_ = orderBy (asc snd) $ proc () -> do
  deck <- deckSelect -< ()
  (dIDs, idx) <- zippies dIDs_ -< ()
  restrict -< deckFID deck .=== dIDs
  returnA -< (deck, idx)

findDecksByUserID :: UserID -> Select DeckFR
findDecksByUserID uID =
  proc () -> do
    deck <- deckSelect -< ()
    deckUser <- selectTable deckUserTable -< ()
    restrict -< (deckFID deck .=== deckUserFDeckID deckUser)
    restrict -< (toFields uID .=== deckUserFUserID deckUser)
    returnA -< deck

findDeckForUser :: UserID -> DeckID -> Select DeckFR
findDeckForUser uID dID =
  proc () -> do
    deck <- deckSelect -< ()
    deckUser <- selectTable deckUserTable -< ()
    restrict -< (toFields dID .=== deckFID deck)
    restrict -< (toFields dID .=== deckUserFDeckID deckUser)
    restrict -< (toFields uID .=== deckUserFUserID deckUser)
    returnA -< deck