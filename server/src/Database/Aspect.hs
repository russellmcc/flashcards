module Database.Aspect where

import Control.Arrow (returnA)
import Data.Aeson.Types (emptyObject)
import Data.Text (Text)
import Database.Model
import Opaleye
import GHC.Int (Int64)
import Database.Utils

aspectSelect :: Select  AspectFR
aspectSelect = selectTable aspectTable

insertAspect :: (ConceptID, Text) -> Insert [Aspect]
insertAspect (conceptID, name) =
  Insert
    { iTable = aspectTable,
      iRows =
        [ AspectF
            { aspectFID = ID Nothing,
              aspectFName = toFields name,
              aspectFConceptID = toFields conceptID,
              aspectFInfo = toFields emptyObject
            }
        ],
      iReturning = rReturning id,
      iOnConflict = Nothing
    }

findAspectsByConceptIDs :: [ConceptID] -> Select  (AspectFR, Column SqlInt4)
findAspectsByConceptIDs cIDs_ = orderBy (asc snd) $ proc () -> do
  aspect <- aspectSelect -< ()
  (cIDs, idx) <- zippies cIDs_ -< ()
  restrict -< (aspectFConceptID aspect .=== cIDs)
  returnA -< (aspect, idx)

findAspectsByIDs :: [AspectID] -> Select (AspectFR, Column SqlInt4)
findAspectsByIDs aIDs_ = orderBy (asc snd) $ proc () -> do
  aspect <- aspectSelect -< ()
  (aIDs, idx) <- zippies aIDs' -< ()
  restrict -< (aspectFID aspect .=== aIDs)
  returnA -< (aspect, idx)
  where
    aIDs' :: [IDT 'AspectIDT (Field SqlInt4)]
    aIDs' = ID . toFields . getID <$> aIDs_

deleteAspectByIDs :: [AspectID] -> Delete Int64
deleteAspectByIDs aIDs =
  Delete { dTable = aspectTable
         , dWhere = \AspectF{ aspectFID } -> (ID . toFields . getID <$> aIDs) `inMany` aspectFID
         , dReturning = rCount
         }