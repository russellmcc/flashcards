{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module Database.Model where

import Control.Arrow ((>>>))
import Data.Aeson
import Data.Functor ((<&>))
import Data.Kind (Constraint)
import qualified Data.Profunctor as P
import qualified Data.Profunctor.Product as PP
import Data.Profunctor.Product.Default (Default)
import qualified Data.Profunctor.Product.Default as D
import Data.Text (Text)
import Data.Time.Clock (UTCTime)
import GHC.Generics
import Opaleye
import Text.Casing (camel)

data IDTag = DeckIDT | UserIDT | AspectIDT | ConceptIDT | TestIDT

newtype IDT (t :: IDTag) a = ID {getID :: a} deriving (Show, Eq, Ord, Functor)

toNullableID :: (Applicative m) => IDT t1 b -> IDT t1 (m b)
toNullableID = getID >>> pure >>> ID

liftID :: (Functor m) => IDT t (m b) -> m (IDT t b)
liftID x = getID x <&> ID

idDef ::
  ( P.Profunctor p,
    Default p a b
  ) =>
  p (IDT t a) (IDT t b)
idDef = P.dimap getID ID D.def

instance
  ( Default p h a,
    P.Profunctor p
  ) =>
  Default p (IDT t h) (IDT t a)
  where
  def = idDef

tableFieldID :: TableFields a b -> TableFields (IDT t a) (IDT t b)
tableFieldID = P.dimap getID ID

fst1 :: (f :*: g) a -> f a
fst1 (l :*: _) = l

snd1 :: (f :*: g) a -> g a
snd1 (_ :*: r) = r

class GenDef p f g where
  genDef :: p (f a) (g a)
  type GenDefaultConstraints' p f g :: Constraint

instance (PP.ProductProfunctor p, GenDef p f g, GenDef p f' g') => GenDef p (f :*: f') (g :*: g') where
  genDef = (:*:) PP.***$ P.lmap fst1 genDef PP.**** P.lmap snd1 genDef
  type GenDefaultConstraints' p (f :*: f') (g :*: g') = (PP.ProductProfunctor p, GenDefaultConstraints' p f g, GenDefaultConstraints' p f' g')

instance (P.Profunctor p, GenDef p f g) => GenDef p (M1 i c f) (M1 i' c' g) where
  genDef = P.dimap unM1 M1 genDef
  type GenDefaultConstraints' p (M1 i c f) (M1 i' c' g) = GenDefaultConstraints' p f g

instance (P.Profunctor p, Default p a b) => GenDef p (K1 i a) (K1 i b) where
  genDef = P.dimap unK1 K1 D.def
  type GenDefaultConstraints' p (K1 i a) (K1 i b) = (P.Profunctor p, Default p a b)

type GenDefaultConstraints p f g = GenDefaultConstraints' p (Rep f) (Rep g)

defaultGenDef :: (P.Profunctor p, Generic f, Generic g, GenDef p (Rep f) (Rep g)) => p f g
defaultGenDef = P.dimap from to genDef

---------------

type UserID = IDT 'UserIDT Int

data User = User
  { userID :: UserID,
    userName :: Text,
    userPasswordHash :: Maybe Text,
    userProfile :: Value
  }
  deriving (Generic)

data UserF a = UserF
  { userFID :: a,
    userFName :: Field SqlText,
    userFPasswordHash :: FieldNullable SqlText,
    userFProfile :: Field SqlJsonb
  }
  deriving (Generic)

type UserFR = UserF (IDT 'UserIDT (Field SqlInt4))

type UserFW = UserF (IDT 'UserIDT (Maybe (Field SqlInt4)))

instance
  (GenDefaultConstraints p (UserF a) (UserF b)) =>
  Default p (UserF a) (UserF b)
  where
  def = defaultGenDef

instance
  (GenDefaultConstraints p UserFR User) =>
  Default p UserFR User
  where
  def = defaultGenDef

userTable :: Table UserFW UserFR
userTable =
  table "users" $
    UserF <$> P.lmap userFID (tableFieldID (optionalTableField "id"))
      <*> P.lmap userFName (requiredTableField "username")
      <*> P.lmap userFPasswordHash (requiredTableField "password_hash")
      <*> P.lmap userFProfile (requiredTableField "profile")

-------------------------------------------------------------------------------

type DeckID = IDT 'DeckIDT Int

data Deck = Deck
  { deckID :: DeckID,
    deckName :: Text,
    deckOwner :: IDT 'UserIDT (Maybe Int),
    deckMetadata :: Value,
    deckPublic :: Bool
  }
  deriving (Generic)

data DeckF a = DeckF
  { deckFID :: a,
    deckFName :: Field SqlText,
    deckFOwner :: IDT 'UserIDT (FieldNullable SqlInt4),
    deckFMetadata :: Field SqlJsonb,
    deckFPublic :: Field SqlBool
  }
  deriving (Generic)

type DeckFR = DeckF (IDT 'DeckIDT (Field SqlInt4))

type DeckFW = DeckF (IDT 'DeckIDT (Maybe (Field SqlInt4)))

instance
  (GenDefaultConstraints p (DeckF a) (DeckF b)) =>
  Default p (DeckF a) (DeckF b)
  where
  def = defaultGenDef

instance
  (GenDefaultConstraints p DeckFR Deck) =>
  Default p DeckFR Deck
  where
  def = defaultGenDef

deckTable :: Table DeckFW DeckFR
deckTable =
  table "decks" $
    DeckF <$> P.lmap deckFID (tableFieldID (optionalTableField "id"))
      <*> P.lmap deckFName (requiredTableField "name")
      <*> P.lmap deckFOwner (tableFieldID (requiredTableField "owner"))
      <*> P.lmap deckFMetadata (requiredTableField "metadata")
      <*> P.lmap deckFPublic (requiredTableField "public")

-------------------------------------------------------------------------------

data DeckUser = DeckUser
  { deckUserDeckID :: DeckID,
    deckUserUserID :: UserID
  }
  deriving (Generic)

data DeckUserF = DeckUserF
  { deckUserFDeckID :: IDT 'DeckIDT (Field SqlInt4),
    deckUserFUserID :: IDT 'UserIDT (Field SqlInt4)
  }
  deriving (Generic)

instance
  (GenDefaultConstraints p DeckUserF DeckUserF) =>
  Default p DeckUserF DeckUserF
  where
  def = defaultGenDef

instance
  (GenDefaultConstraints p DeckUserF DeckUser) =>
  Default p DeckUserF DeckUser
  where
  def = defaultGenDef

deckUserTable :: Table DeckUserF DeckUserF
deckUserTable =
  table "decks_users" $
    DeckUserF <$> P.lmap deckUserFDeckID (tableFieldID (requiredTableField "deck_id"))
      <*> P.lmap deckUserFUserID (tableFieldID (requiredTableField "user_id"))

-------------------------------------------------------------------------------

type ConceptID = IDT 'ConceptIDT Int

data Concept = Concept
  { conceptID :: ConceptID,
    conceptName :: Text,
    conceptDeckID :: DeckID,
    conceptInfo :: Value
  }
  deriving (Generic)

data ConceptF a = ConceptF
  { conceptFID :: a,
    conceptFName :: Field SqlText,
    conceptFDeckID :: IDT 'DeckIDT (Field SqlInt4),
    conceptFInfo :: Field SqlJsonb
  }
  deriving (Generic)

type ConceptFR = ConceptF (IDT 'ConceptIDT (Field SqlInt4))

type ConceptFW = ConceptF (IDT 'ConceptIDT (Maybe (Field SqlInt4)))

instance
  (GenDefaultConstraints p (ConceptF a) (ConceptF b)) =>
  Default p (ConceptF a) (ConceptF b)
  where
  def = defaultGenDef

instance
  (GenDefaultConstraints p ConceptFR Concept) =>
  Default p ConceptFR Concept
  where
  def = defaultGenDef

conceptTable :: Table ConceptFW ConceptFR
conceptTable =
  table "concepts" $
    ConceptF <$> P.lmap conceptFID (tableFieldID (optionalTableField "id"))
      <*> P.lmap conceptFName (requiredTableField "name")
      <*> P.lmap conceptFDeckID (tableFieldID (requiredTableField "deck_id"))
      <*> P.lmap conceptFInfo (requiredTableField "info")

-------------------------------------------------------------------------------
type AspectID = IDT 'AspectIDT Int

data Aspect = Aspect
  { aspectID :: AspectID,
    aspectName :: Text,
    aspectConceptID :: ConceptID,
    aspectInfo :: Value
  }
  deriving (Generic)

data AspectF a = AspectF
  { aspectFID :: a,
    aspectFName :: Field SqlText,
    aspectFConceptID :: IDT 'ConceptIDT (Field SqlInt4),
    aspectFInfo :: Field SqlJsonb
  }
  deriving (Generic)

type AspectFR = AspectF (IDT 'AspectIDT (Field SqlInt4))

type AspectFW = AspectF (IDT 'AspectIDT (Maybe (Field SqlInt4)))

instance
  (GenDefaultConstraints p (AspectF a) (AspectF b)) =>
  Default p (AspectF a) (AspectF b)
  where
  def = defaultGenDef

instance
  (GenDefaultConstraints p AspectFR Aspect) =>
  Default p AspectFR Aspect
  where
  def = defaultGenDef

aspectTable :: Table AspectFW AspectFR
aspectTable =
  table "aspects" $
    AspectF <$> P.lmap aspectFID (tableFieldID (optionalTableField "id"))
      <*> P.lmap aspectFName (requiredTableField "name")
      <*> P.lmap aspectFConceptID (tableFieldID (requiredTableField "concept_id"))
      <*> P.lmap aspectFInfo (requiredTableField "info")

-------------------------------------------------------------------------------
type TestID = IDT 'TestIDT Int

data TestRaw = TestRaw
  { testRawID :: TestID,
    testRawName :: Text,
    testRawAspectID :: AspectID,
    testRawType :: Int,
    testRawTestData :: Value
  }
  deriving (Generic)

data TestRawF a = TestRawF
  { testRawFID :: a,
    testRawFName :: Field SqlText,
    testRawFAspectID :: IDT 'AspectIDT (Field SqlInt4),
    testRawFType :: Field SqlInt4,
    testRawFTestData :: Field SqlJsonb
  }
  deriving (Generic)

type TestRawFR = TestRawF (IDT 'TestIDT (Field SqlInt4))

type TestRawFW = TestRawF (IDT 'TestIDT (Maybe (Field SqlInt4)))

instance
  (GenDefaultConstraints p (TestRawF a) (TestRawF b)) =>
  Default p (TestRawF a) (TestRawF b)
  where
  def = defaultGenDef

instance
  (GenDefaultConstraints p TestRawFR TestRaw) =>
  Default p TestRawFR TestRaw
  where
  def = defaultGenDef

testTable :: Table TestRawFW TestRawFR
testTable =
  table "tests" $
    TestRawF <$> P.lmap testRawFID (tableFieldID (optionalTableField "id"))
      <*> P.lmap testRawFName (requiredTableField "name")
      <*> P.lmap testRawFAspectID (tableFieldID (requiredTableField "aspect_id"))
      <*> P.lmap testRawFType (requiredTableField "type")
      <*> P.lmap testRawFTestData (requiredTableField "test_data")

data SelfGradedPictionaryData = SelfGradedPictionaryData
  { sgFront :: Text,
    sgBack :: Text
  }
  deriving (Generic, Show, Eq, Ord)

instance FromJSON SelfGradedPictionaryData where
  parseJSON = genericParseJSON defaultOptions {fieldLabelModifier = camel . drop 2}

instance ToJSON SelfGradedPictionaryData where
  toJSON = genericToJSON defaultOptions {fieldLabelModifier = camel . drop 2}

data EntryData = EntryData
  { agFront :: Text,
    agBack :: Text,
    agWrongs :: Maybe [Text],
    agAutoGrade :: Bool
  }
  deriving (Generic, Show, Eq, Ord)

instance FromJSON EntryData where
  parseJSON = genericParseJSON defaultOptions {fieldLabelModifier = camel . drop 2}

instance ToJSON EntryData where
  toJSON = genericToJSON defaultOptions {fieldLabelModifier = camel . drop 2}

data TestData = SelfGradedPictionaryTestData SelfGradedPictionaryData | EntryTestData EntryData deriving (Show, Eq, Ord)

getTestType :: TestData -> Int
getTestType (SelfGradedPictionaryTestData _) = 0
getTestType (EntryTestData _) = 1

getTestData :: TestData -> Value
getTestData (SelfGradedPictionaryTestData s) = toJSON s
getTestData (EntryTestData a) = toJSON a

data Test = Test
  { testID :: TestID,
    testName :: Text,
    testAspectID :: AspectID,
    testData :: TestData
  }

assume :: Result a -> a
assume (Success x) = x
assume _ = error "Database schema error"

testFromRaw :: TestRaw -> Test
testFromRaw TestRaw {testRawID, testRawName, testRawAspectID, testRawType = 0, testRawTestData} =
  Test
    { testID = testRawID,
      testName = testRawName,
      testAspectID = ID $ getID testRawAspectID,
      testData = (SelfGradedPictionaryTestData . assume . fromJSON) testRawTestData
    }
testFromRaw TestRaw {testRawID, testRawName, testRawAspectID, testRawType = 1, testRawTestData} =
  Test
    { testID = testRawID,
      testName = testRawName,
      testAspectID = ID $ getID testRawAspectID,
      testData = (EntryTestData . assume . fromJSON) testRawTestData
    }
testFromRaw TestRaw {testRawType = _} = error "Unknown test type"

testToRaw :: Test -> TestRaw
testToRaw Test {testID, testName, testAspectID, testData} =
  TestRaw
    { testRawID = testID,
      testRawName = testName,
      testRawAspectID = ID $ getID testAspectID,
      testRawType = getTestType testData,
      testRawTestData = getTestData testData
    }

-------------------------------------------------------------------------------

data ScheduledAspectRaw = ScheduledAspectRaw
  { scheduledAspectRawUserID :: IDT 'UserIDT Int,
    scheduledAspectRawAspectID :: IDT 'AspectIDT Int,
    scheduledAspectRawType :: Int,
    scheduledAspectRawDueDate :: UTCTime,
    scheduledAspectRawStudiedDate :: Maybe UTCTime,
    scheduledAspectRawFirstScheduledDate :: UTCTime,
    scheduledAspectRawFactor :: Maybe Double,
    scheduledAspectRawStep :: Maybe Int
  }
  deriving (Generic)

data ScheduledAspectRawF = ScheduledAspectRawF
  { scheduledAspectRawFUserID :: IDT 'UserIDT (Field SqlInt4),
    scheduledAspectRawFAspectID :: IDT 'AspectIDT (Field SqlInt4),
    scheduledAspectRawFType :: Field SqlInt4,
    scheduledAspectRawFDueDate :: Field SqlTimestamptz,
    scheduledAspectRawFStudiedDate :: FieldNullable SqlTimestamptz,
    scheduledAspectRawFFirstScheduledDate :: Field SqlTimestamptz,
    scheduledAspectRawFFactor :: FieldNullable SqlFloat8,
    scheduledAspectRawFStep :: FieldNullable SqlInt4
  }
  deriving (Generic)

instance
  (GenDefaultConstraints p ScheduledAspectRawF ScheduledAspectRawF) =>
  Default p ScheduledAspectRawF ScheduledAspectRawF
  where
  def = defaultGenDef

instance
  (GenDefaultConstraints p ScheduledAspectRaw ScheduledAspectRawF) =>
  Default p ScheduledAspectRaw ScheduledAspectRawF
  where
  def = defaultGenDef

instance
  (GenDefaultConstraints p ScheduledAspectRawF ScheduledAspectRaw) =>
  Default p ScheduledAspectRawF ScheduledAspectRaw
  where
  def = defaultGenDef

scheduledAspectTable :: Table ScheduledAspectRawF ScheduledAspectRawF
scheduledAspectTable =
  table "scheduled_aspects" $
    ScheduledAspectRawF
      <$> P.lmap scheduledAspectRawFUserID (tableFieldID (tableField "user_id"))
      <*> P.lmap scheduledAspectRawFAspectID (tableFieldID (tableField "aspect_id"))
      <*> P.lmap scheduledAspectRawFType (tableField "type")
      <*> P.lmap scheduledAspectRawFDueDate (tableField "due_date")
      <*> P.lmap scheduledAspectRawFStudiedDate (tableField "studied_date")
      <*> P.lmap scheduledAspectRawFFirstScheduledDate (tableField "first_scheduled")
      <*> P.lmap scheduledAspectRawFFactor (tableField "factor")
      <*> P.lmap scheduledAspectRawFStep (tableField "step")

newtype LearningSched = LearningSched
  { learnStep :: Int
  }
  deriving (Eq, Ord, Show)

data RelearningSched = RelearningSched
  { relearnStep :: Int,
    relearnFactor :: Double
  }
  deriving (Eq, Ord, Show)

data ReviewSched = ReviewSched
  { reviewFactor :: Double,
    reviewStudiedDate :: UTCTime
  }
  deriving (Eq, Ord, Show)

data Sched = SchedLearning LearningSched | SchedRelearning RelearningSched | SchedReview ReviewSched deriving (Eq, Ord, Show)

data ScheduledAspect = ScheduledAspect
  { scheduledAspectUserID :: UserID,
    scheduledAspectAspectID :: AspectID,
    scheduledAspectDueDate :: UTCTime,
    scheduledAspectSched :: Sched
  }
  deriving (Eq, Ord, Show)

assumeJust :: Maybe a -> a
assumeJust Nothing = error "Unexpected Nothing"
assumeJust (Just a) = a

schedFromRaw :: ScheduledAspectRaw -> Sched
schedFromRaw ScheduledAspectRaw {scheduledAspectRawType = 1, scheduledAspectRawStep} =
  SchedLearning $
    LearningSched
      { learnStep = assumeJust scheduledAspectRawStep
      }
schedFromRaw ScheduledAspectRaw {scheduledAspectRawType = 2, scheduledAspectRawStep, scheduledAspectRawFactor} =
  SchedRelearning $
    RelearningSched
      { relearnStep = assumeJust scheduledAspectRawStep,
        relearnFactor = assumeJust scheduledAspectRawFactor
      }
schedFromRaw ScheduledAspectRaw {scheduledAspectRawType = 3, scheduledAspectRawStudiedDate, scheduledAspectRawFactor} =
  SchedReview $
    ReviewSched
      { reviewFactor = assumeJust scheduledAspectRawFactor,
        reviewStudiedDate = assumeJust scheduledAspectRawStudiedDate
      }
schedFromRaw ScheduledAspectRaw {scheduledAspectRawType = _} = error "Unknown schedule"

schedToRaw :: UserID -> AspectID -> UTCTime -> UTCTime -> Sched -> ScheduledAspectRaw
schedToRaw uID aID dueDate firstScheduledDate (SchedLearning LearningSched {learnStep}) =
  ScheduledAspectRaw
    { scheduledAspectRawUserID = uID,
      scheduledAspectRawAspectID = aID,
      scheduledAspectRawType = 1,
      scheduledAspectRawDueDate = dueDate,
      scheduledAspectRawStudiedDate = Nothing,
      scheduledAspectRawFirstScheduledDate = firstScheduledDate,
      scheduledAspectRawFactor = Nothing,
      scheduledAspectRawStep = Just learnStep
    }
schedToRaw uID aID dueDate firstScheduledDate (SchedRelearning RelearningSched {relearnStep, relearnFactor}) =
  ScheduledAspectRaw
    { scheduledAspectRawUserID = uID,
      scheduledAspectRawAspectID = aID,
      scheduledAspectRawType = 2,
      scheduledAspectRawDueDate = dueDate,
      scheduledAspectRawStudiedDate = Nothing,
      scheduledAspectRawFirstScheduledDate = firstScheduledDate,
      scheduledAspectRawFactor = Just relearnFactor,
      scheduledAspectRawStep = Just relearnStep
    }
schedToRaw uID aID dueDate firstScheduledDate (SchedReview ReviewSched {reviewStudiedDate, reviewFactor}) =
  ScheduledAspectRaw
    { scheduledAspectRawUserID = uID,
      scheduledAspectRawAspectID = aID,
      scheduledAspectRawType = 3,
      scheduledAspectRawDueDate = dueDate,
      scheduledAspectRawStudiedDate = Just reviewStudiedDate,
      scheduledAspectRawFirstScheduledDate = firstScheduledDate,
      scheduledAspectRawFactor = Just reviewFactor,
      scheduledAspectRawStep = Nothing
    }

scheduledAspectFromRaw :: ScheduledAspectRaw -> ScheduledAspect
scheduledAspectFromRaw s =
  ScheduledAspect
    { scheduledAspectUserID = scheduledAspectRawUserID s,
      scheduledAspectAspectID = scheduledAspectRawAspectID s,
      scheduledAspectDueDate = scheduledAspectRawDueDate s,
      scheduledAspectSched = schedFromRaw s
    }

scheduledAspectToRaw :: UTCTime -> ScheduledAspect -> ScheduledAspectRaw
scheduledAspectToRaw firstSched s =
  schedToRaw
    (scheduledAspectUserID s)
    (scheduledAspectAspectID s)
    (scheduledAspectDueDate s)
    firstSched
    (scheduledAspectSched s)

----

inMany ::
  (D.Default EqPP fields fields, Foldable f) =>
  f fields ->
  fields ->
  Field SqlBool
inMany l x = foldr (\a b -> (a .=== x) .|| b) (sqlBool False) l
