module Database.Pagination (Cursor(..), 
  Item(..), PageInfo(..), Paginated(..),
  PaginationArgs(..), runPaginatedSelect, runPaginatedSelectSingle) where

import GHC.Generics
import Opaleye hiding (Cursor)
import Control.Arrow (returnA)
import Data.Profunctor.Product.Default (Default)
import Data.String (IsString)

newtype Cursor a = Cursor {runCursor :: a} deriving (Generic, Show, Eq, Ord, Functor)

data Item b a = Item {
    value :: a,
    cursor :: Cursor b
  } deriving (Generic, Show, Eq, Ord, Functor)

data PageInfo a = PageInfo {
  hasPreviousPage :: Bool,
  hasNextPage :: Bool,
  startCursor :: Maybe (Cursor a),
  endCursor :: Maybe (Cursor a)
  } deriving (Generic, Show, Eq, Ord)

data Paginated b a = Paginated {
  items :: [Item b a],
  pageInfo :: PageInfo b
} deriving (Generic, Show, Eq, Ord)

instance Functor (Paginated b) where
  fmap f Paginated {items, pageInfo} = Paginated ((f <$>) <$> items) pageInfo

data PaginationArgs a = PaginationArgs {
  after :: Maybe (Cursor a),
  first :: Maybe Int
} deriving (Generic, Show, Eq, Ord)

toItems :: [(v, k)] -> [Item k v]
toItems = (toItem <$>) where
  toItem (v, k) = Item v (Cursor k)

toPaginated :: Int -> [(v, k)] -> Paginated k v
toPaginated _ [] = Paginated [] (PageInfo False False Nothing Nothing)
toPaginated n ks | length ks <= n = Paginated (toItems ks) (PageInfo 
  False 
  False 
  (Just . Cursor . snd $ head ks) 
  (Just . Cursor . snd $ last ks))
toPaginated n ks = Paginated (toItems $ take n ks) (PageInfo False True (Just . Cursor . snd $ head ks) (Just . Cursor . snd $ last (take n ks)))

runPaginatedSelect :: (Functor f, IsString s) =>
                            (full -> k) -> Order full -> (full -> k' -> Field SqlBool)
                            -> Select full
                            -> (Select (full, k) -> f [(v, k')])
                            -> PaginationArgs k'
                            -> f (Either s (Paginated k' v))
runPaginatedSelect sortKey order gt rawSelect doRun PaginationArgs {after, first} =
    fixResult first <$> doRun select where
        select = pairKey $ selectFirst first $ selectAfter after $ doSort rawSelect
        doSort = orderBy order
        selectFirst Nothing = id
        selectFirst (Just n) = limit (n + 1)
        selectAfter Nothing s = s
        selectAfter (Just n) s = proc () -> do
            res <- s -< ()
            restrict -< gt res (runCursor n)
            returnA -< res
        pairKey s = proc () -> do
            res <- s -< ()
            returnA -< (res, sortKey res)
        fixResult (Just n) _ | n < 0 = Left "first must be non-negative" 
        fixResult (Just n) as = Right $ toPaginated n as
        fixResult Nothing as = Right $ toPaginated (length as) as

runPaginatedSelectSingle ::
  ( SqlOrd k,
    Default
      ToFields
      k'
      (Field k),
    Functor f,
    IsString s
  ) =>
  (full -> Field k) ->
  Select full ->
  (Select (full, Field k) -> f [(v, k')]) ->
  PaginationArgs k' ->
  f (Either s (Paginated k' v))
runPaginatedSelectSingle sortKey =
  runPaginatedSelect
    sortKey
    (asc sortKey)
    (\f k -> sortKey f .> toFields k)
