{-# LANGUAGE DataKinds #-}

module Database.ScheduledAspect where

import Control.Arrow (returnA)
import Database.Aspect
import Database.Concept
import Database.Model
import Database.Test
import Opaleye
import GHC.Int (Int64)
import Data.Time.Clock (UTCTime)

scheduledAspectSelect :: Select ScheduledAspectRawF
scheduledAspectSelect = selectTable scheduledAspectTable

insertScheduledAspect :: ScheduledAspectRaw -> Insert [ScheduledAspectRaw]
insertScheduledAspect sa = Insert {
  iTable = scheduledAspectTable,
  iRows = [toFields sa],
  iReturning = rReturning id,
  iOnConflict = Nothing
  }

updateScheduledAspect :: ScheduledAspectRaw -> Update [ScheduledAspectRaw]
updateScheduledAspect sa = Update {
  uTable = scheduledAspectTable,
  uUpdateWith = updateEasy (\(ScheduledAspectRawF uID aID _ _ _ schedDate _ _) -> ScheduledAspectRawF uID aID (toFields $ scheduledAspectRawType sa) (toFields $ scheduledAspectRawDueDate sa) (toFields$ scheduledAspectRawStudiedDate sa) schedDate (toFields $ scheduledAspectRawFactor sa) (toFields $ scheduledAspectRawStep sa)),
  uWhere = \(ScheduledAspectRawF uID aID _ _ _ _ _ _) -> (uID .=== (toFields . scheduledAspectRawUserID $ sa)) .&& (aID .=== (toFields . scheduledAspectRawAspectID $ sa)),
  uReturning = rReturning id
  }

rawFindScheduledAspectsByDeckID :: UserID -> DeckID -> Select ScheduledAspectRawF
rawFindScheduledAspectsByDeckID userID deckID = proc () -> do
   concept <- conceptSelect -< ()
   aspect <- aspectSelect -< ()
   scheduledAspect <- scheduledAspectSelect -< ()
   restrict -< (conceptFDeckID concept .=== toFields deckID)
   restrict -< (aspectFConceptID aspect .=== conceptFID concept)
   restrict -< (scheduledAspectRawFAspectID scheduledAspect .=== aspectFID aspect)
   restrict -< (scheduledAspectRawFUserID scheduledAspect .=== toFields userID)
   returnA -< scheduledAspect

findScheduledAspectsByDeckIDFirstScheduledAfter :: UserID -> DeckID -> UTCTime -> Select ScheduledAspectRawF
findScheduledAspectsByDeckIDFirstScheduledAfter uID dID after = proc () -> do
  sA <- rawFindScheduledAspectsByDeckID uID dID -< ()
  restrict -< (scheduledAspectRawFFirstScheduledDate sA .> toFields after)
  returnA -< sA

findReadyAspectsByDeckID :: UserID -> DeckID -> Select AspectFR
findReadyAspectsByDeckID userID deckID =
  let scheduledForUserAndAspect :: SelectArr (IDT 'AspectIDT (Field SqlInt4)) (IDT 'UserIDT (Field SqlInt4))
      scheduledForUserAndAspect = proc aID -> do
        scheduledAspect <- scheduledAspectSelect -< ()
        restrict -< (scheduledAspectRawFAspectID scheduledAspect .=== aID)
        restrict -< (scheduledAspectRawFUserID scheduledAspect .=== toFields userID)
        returnA -< scheduledAspectRawFUserID scheduledAspect
  in distinct $ proc () -> do
    concept <- conceptSelect -< ()
    aspect <- aspectSelect -< ()
    test <- testSelect -< ()
    restrict -< (conceptFDeckID concept .=== toFields deckID)
    restrict -< (aspectFConceptID aspect .=== conceptFID concept)
    restrict -< (testRawFAspectID test .=== aspectFID aspect)
    restrictNotExists scheduledForUserAndAspect -< (aspectFID aspect)
    returnA -< aspect

deleteScheduledAspectByIDs :: [AspectID] -> Delete Int64
deleteScheduledAspectByIDs aspectIDs =
  Delete { dTable = scheduledAspectTable
         , dWhere = \ScheduledAspectRawF{ scheduledAspectRawFAspectID } -> (toFields <$> aspectIDs) `inMany` scheduledAspectRawFAspectID
         , dReturning = rCount
         }