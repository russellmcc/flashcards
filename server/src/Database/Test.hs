module Database.Test where

import Control.Arrow (returnA)
import Data.Text (Text)
import Database.Model
import Opaleye
import GHC.Int (Int64)
import Database.Utils

testSelect :: Select TestRawFR
testSelect = selectTable testTable

insertTest :: (AspectID, Text, TestData) -> Insert [TestRaw]
insertTest (aspectID, name, testData) =
  let TestRaw
        { testRawID = _,
          testRawName,
          testRawAspectID,
          testRawType,
          testRawTestData
        } =
          testToRaw $ Test (ID 0) name aspectID testData
   in Insert
        { iTable = testTable,
          iRows =
            [ TestRawF
                { testRawFID = ID Nothing,
                  testRawFName = toFields testRawName,
                  testRawFAspectID = toFields testRawAspectID,
                  testRawFType = toFields testRawType,
                  testRawFTestData = toFields testRawTestData
                }
            ],
          iReturning = rReturning id,
          iOnConflict = Nothing
        }

findTestsByAspectIDs :: [AspectID] -> Select (TestRawFR, Column SqlInt4)
findTestsByAspectIDs aIDs_ = orderBy (asc snd) $ proc () -> do
   test <- testSelect -< ()
   (aIDs, idx) <- zippies aIDs_ -< ()
   restrict -< (testRawFAspectID test .=== aIDs)
   returnA -< (test, idx)

deleteTestsWithAspectIDs :: [AspectID] -> Delete Int64
deleteTestsWithAspectIDs aspectIDs =
  Delete { dTable = testTable
         , dWhere = \TestRawF{ testRawFAspectID } -> (ID . toFields . getID <$> aspectIDs) `inMany` testRawFAspectID
         , dReturning = rCount
         }

deleteTestsByIDs :: [TestID] -> Delete Int64
deleteTestsByIDs tIDs =
  Delete { dTable = testTable
         , dWhere = \TestRawF{ testRawFID } -> (ID . toFields . getID <$> tIDs) `inMany` testRawFID
         , dReturning = rCount
         }

findTestsByIDs :: [TestID] -> Select (TestRawFR, Column SqlInt4)
findTestsByIDs tIDs_ = orderBy (asc snd) $ proc () -> do
   test <- testSelect -< ()
   (tIDs, idx) <- zippies tIDs_ -< ()
   restrict -< (testRawFID test .=== tIDs)
   returnA -< (test, idx)
