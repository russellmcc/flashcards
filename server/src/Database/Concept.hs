module Database.Concept where

import Control.Arrow (returnA)
import Data.Aeson.Types (emptyObject)
import Data.Text (Text)
import Database.Model
import Opaleye
import GHC.Int (Int64)
import Database.Utils

conceptSelect :: Select ConceptFR
conceptSelect = selectTable conceptTable

insertConcept :: (DeckID, Text) -> Insert [Concept]
insertConcept (deckID, name) =
  Insert
    { iTable = conceptTable,
      iRows =
        [ ConceptF
            { conceptFID = ID Nothing,
              conceptFName = toFields name,
              conceptFDeckID = toFields deckID,
              conceptFInfo = toFields emptyObject
            }
        ],
      iReturning = rReturning id,
      iOnConflict = Nothing
    }

findConceptsByIDs :: [ConceptID] -> Select (ConceptFR, Column SqlInt4)
findConceptsByIDs cIDs_ = orderBy (asc snd) $ proc () -> do
  concept <- conceptSelect -< ()
  (cIDs, idx) <- zippies cIDs_ -< ()
  restrict -< (conceptFID concept .=== cIDs)
  returnA -< (concept, idx)

findConceptsByDeckID :: DeckID -> Select ConceptFR
findConceptsByDeckID deckID = proc () -> do
  concept <- conceptSelect -< ()
  restrict -< (conceptFDeckID concept .===  toFields deckID)
  returnA -< concept

deleteConceptByIDs :: [ConceptID] -> Delete Int64
deleteConceptByIDs cIDs =
  Delete { dTable = conceptTable
         , dWhere = \ConceptF{ conceptFID } -> (ID . toFields . getID <$> cIDs) `inMany` conceptFID
         , dReturning = rCount
         }