const gql = require("graphql-tag").default;
const { clearDatabase, graphQLClient, clientForUsername } = require("./util");
const { createDeck } = require("./queries");

beforeEach(() => {
  return clearDatabase();
});

describe("`createDeck` mutation", () => {
  const currentUserDecks = gql`
    query Query {
      currentUser {
        decks {
          deck {
            name
          }
        }
      }
    }
  `;

  test("should fail when not logged in", async () => {
    expect.assertions(1);
    const client = graphQLClient();
    await expect(
      client.mutate({ mutation: createDeck, variables: { name: "Math" } })
    ).rejects.toThrow();
  });

  test("can make a deck", async () => {
    const client = await clientForUsername("Gomez");
    expect(
      (
        await client.mutate({
          mutation: createDeck,
          variables: { name: "Golf" },
        })
      ).data.createDeck.deck.name
    ).toBe("Golf");
  });

  test("deck is owned by myself", async () => {
    const client = await clientForUsername("Gomez");
    expect(
      (
        await client.mutate({
          mutation: createDeck,
          variables: { name: "Golf" },
        })
      ).data.createDeck.deck.owner.username
    ).toBe("Gomez");
  });

  test("deck appears in currentUser's list", async () => {
    const client = await clientForUsername("Gomez");
    await client.mutate({ mutation: createDeck, variables: { name: "Golf" } });
    expect(
      (await client.query({ query: currentUserDecks })).data.currentUser.decks
        .length
    ).toBe(1);
    expect(
      (await client.query({ query: currentUserDecks })).data.currentUser
        .decks[0].deck.name
    ).toBe("Golf");
  });

  test("decks don't appear in other user's lists", async () => {
    const gomez = await clientForUsername("Gomez");
    const fester = await clientForUsername("Fester");
    await gomez.mutate({ mutation: createDeck, variables: { name: "Golf" } });
    expect(
      (await fester.query({ query: currentUserDecks })).data.currentUser.decks
        .length
    ).toBe(0);
  });
});
