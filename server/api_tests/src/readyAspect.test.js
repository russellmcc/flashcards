const gql = require("graphql-tag").default;
const { clearDatabase, graphQLClient, clientForUsername } = require("./util");
const {
  createDeck,
  createConcept,
  createAspect,
  createSelfGradedPictionaryTest,
} = require("./queries");

beforeEach(() => {
  return clearDatabase();
});

describe("`readyAspects` query", () => {
  const currentUserReadyAspects = gql`
    query Query {
      currentUser {
        decks {
          readyAspects {
            aspect {
              id
            }
          }
        }
      }
    }
  `;

  const getTestEnv = async () => {
    const client = await clientForUsername("Cookie Monster");
    const deckID = (
      await client.mutate({
        mutation: createDeck,
        variables: { name: "Things I Like" },
      })
    ).data.createDeck.deck.id;
    const conceptID = (
      await client.mutate({
        mutation: createConcept,
        variables: { name: "Parts of Cookie", deckID },
      })
    ).data.createConcept.concept.id;
    const aspectID = (
      await client.mutate({
        mutation: createAspect,
        variables: { name: "Chocolate Chips", conceptID },
      })
    ).data.createAspect.aspect.id;
    return { client, conceptID, deckID, aspectID };
  };

  const scheduleLearning = gql`
    mutation Mutation(
      $dueDate: Date!
      $aspectID: ID!
      $step: Int!
      $scheduledDate: Date
    ) {
      scheduleAspectLearning(
        input: {
          aspect: $aspectID
          dueDate: $dueDate
          step: $step
          scheduledDate: $scheduledDate
        }
      ) {
        scheduledAspect {
          aspect {
            id
          }
        }
      }
    }
  `;
  test("not ready without tests", async () => {
    const { aspectID, client } = await getTestEnv();
    const readies = (await client.query({ query: currentUserReadyAspects }))
      .data.currentUser.decks[0].readyAspects;
    expect(readies.length).toBe(0);
  });
  test("ready shows up", async () => {
    const { aspectID, client } = await getTestEnv();
    await client.mutate({
      mutation: createSelfGradedPictionaryTest,
      variables: {
        name: "Color Test",
        aspectID,
        front: "Color?",
        back: "Blackish brown",
      },
    });
    const readies = (await client.query({ query: currentUserReadyAspects }))
      .data.currentUser.decks[0].readyAspects;
    expect(readies.length).toBe(1);
  });
  test("not ready if you schedule it", async () => {
    const { aspectID, client } = await getTestEnv();
    await client.mutate({
      mutation: createSelfGradedPictionaryTest,
      variables: {
        name: "Color Test",
        aspectID,
        front: "Color?",
        back: "Blackish brown",
      },
    });
    expect(
      (await client.query({ query: currentUserReadyAspects })).data.currentUser
        .decks[0].readyAspects[0].aspect.id
    ).toBe(aspectID);
    client.clearStore();
    await client.mutate({
      mutation: scheduleLearning,
      variables: {
        aspectID,
        dueDate: "2020-06-21T00:00:00Z",
        step: 3,
      },
    });
    const readies = (await client.query({ query: currentUserReadyAspects }))
      .data.currentUser.decks[0].readyAspects;
    expect(readies.length).toBe(0);
  });

  test("max 30 readies by default", async () => {
    const { client, conceptID } = await getTestEnv();
    for (let i = 0; i < 35; ++i) {
      const aspectID = (
        await client.mutate({
          mutation: createAspect,
          variables: { name: `Chocolate Chips $i`, conceptID },
        })
      ).data.createAspect.aspect.id;
      await client.mutate({
        mutation: createSelfGradedPictionaryTest,
        variables: {
          name: "Color Test",
          aspectID,
          front: "Color?",
          back: "Blackish brown",
        },
      });
    }
    const readies = (await client.query({ query: currentUserReadyAspects }))
      .data.currentUser.decks[0].readyAspects;
    expect(readies.length).toBe(30);
  });

  test("max no more than 30 readies per day", async () => {
    const { client, conceptID } = await getTestEnv();
    for (let i = 0; i < 35; ++i) {
      const aspectID = (
        await client.mutate({
          mutation: createAspect,
          variables: { name: `Chocolate Chips $i`, conceptID },
        })
      ).data.createAspect.aspect.id;
      await client.mutate({
        mutation: createSelfGradedPictionaryTest,
        variables: {
          name: "Color Test",
          aspectID,
          front: "Color?",
          back: "Blackish brown",
        },
      });
      if (i < 3) {
        await client.mutate({
          mutation: scheduleLearning,
          variables: {
            aspectID,
            dueDate: "2020-06-21T00:00:00Z",
            step: 3,
            scheduledDate: new Date(Date.now()).toISOString(),
          },
        });
      }
    }
    const readies = (await client.query({ query: currentUserReadyAspects }))
      .data.currentUser.decks[0].readyAspects;
    expect(readies.length).toBe(27);
  });

  test("old schedules don't count towards 30 readies per day", async () => {
    const { client, conceptID } = await getTestEnv();
    for (let i = 0; i < 35; ++i) {
      const aspectID = (
        await client.mutate({
          mutation: createAspect,
          variables: { name: `Chocolate Chips $i`, conceptID },
        })
      ).data.createAspect.aspect.id;
      await client.mutate({
        mutation: createSelfGradedPictionaryTest,
        variables: {
          name: "Color Test",
          aspectID,
          front: "Color?",
          back: "Blackish brown",
        },
      });
      if (i < 3) {
        await client.mutate({
          mutation: scheduleLearning,
          variables: {
            aspectID,
            dueDate: "2020-06-21T00:00:00Z",
            step: 3,
            scheduledDate: "2020-06-21T00:00:00Z",
          },
        });
      }
    }
    const readies = (await client.query({ query: currentUserReadyAspects }))
      .data.currentUser.decks[0].readyAspects;
    expect(readies.length).toBe(30);
  });
});
