const { ApolloClient } = require("apollo-client");
require("cross-fetch/polyfill");
const { InMemoryCache } = require("apollo-cache-inmemory");
const { HttpLink } = require("apollo-link-http");
const { ApolloLink } = require("apollo-link");
require("dotenv").config({ path: "../../.env" });
const gql = require("graphql-tag").default;

const { Client } = require("pg");

module.exports.clearDatabase = async () => {
  const client = new Client({
    connectionString: process.env.TEST_DATABASE_URL,
  });
  await client.connect();
  await client.query("DELETE FROM scheduled_aspects");
  await client.query("DELETE FROM tests");
  await client.query("DELETE FROM aspects");
  await client.query("DELETE FROM concepts");
  await client.query("DELETE FROM decks_users");
  await client.query("DELETE FROM users");
  await client.query("DELETE FROM decks");
  await client.end();
};

module.exports.graphQLClient = (token) => {
  let link = new HttpLink({ uri: process.env.GRAPHQL_URL });
  if (token) {
    const authMiddleware = new ApolloLink((operation, forward) => {
      operation.setContext(({ headers = {} }) => {
        return {
          headers: {
            ...headers,
            authorization: `Bearer ${token}`,
          },
        };
      });
      return forward(operation);
    });

    link = ApolloLink.from([authMiddleware, link]);
  }

  return new ApolloClient({
    cache: new InMemoryCache(),
    link,
  });
};

module.exports.clientForUsername = async (username) => {
  const register = gql`
    mutation Mutation($username: String!) {
      register(input: { username: $username, password: "Please" }) {
        session {
          token
        }
      }
    }
  `;
  return module.exports.graphQLClient(
    (
      await module.exports.graphQLClient().mutate({
        mutation: register,
        variables: { username },
      })
    ).data.register.session.token
  );
};
