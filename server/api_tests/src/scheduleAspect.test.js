const gql = require("graphql-tag").default;
const { clearDatabase, graphQLClient, clientForUsername } = require("./util");
const {
  createDeck,
  createConcept,
  createAspect,
  createSelfGradedPictionaryTest,
  scheduleLearning,
  scheduleReview,
  scheduleRelearning,
} = require("./queries");

beforeEach(() => {
  return clearDatabase();
});

describe("scheduled aspects", () => {
  const currentUserScheduledAspects = gql`
    query Query {
      currentUser {
        decks {
          scheduledAspects {
            edges {
              node {
                aspect {
                  id
                }
                dueDate
                schedule {
                  __typename
                  ... on LearningSchedule {
                    step
                  }
                }
              }
            }
          }
          readyAspects {
            aspect {
              id
            }
          }
        }
      }
    }
  `;

  const currentUserScheduledAspectsMax1 = gql`
    query Query {
      currentUser {
        decks {
          scheduledAspects(first: 1) {
            pageInfo {
              startCursor
              endCursor
              hasNextPage
            }
            edges {
              cursor
              node {
                aspect {
                  id
                }
              }
            }
          }
        }
      }
    }
  `;

  const currentUserScheduledAspectsAfter = gql`
    query Query($after: Cursor!) {
      currentUser {
        decks {
          scheduledAspects(after: $after) {
            pageInfo {
              startCursor
              endCursor
              hasNextPage
            }
            edges {
              cursor
              node {
                aspect {
                  id
                }
              }
            }
          }
        }
      }
    }
  `;

  const getTestEnv = async () => {
    const client = await clientForUsername("Cookie Monster");
    const deckID = (
      await client.mutate({
        mutation: createDeck,
        variables: { name: "Things I Like" },
      })
    ).data.createDeck.deck.id;
    const conceptID = (
      await client.mutate({
        mutation: createConcept,
        variables: { name: "Parts of Cookie", deckID },
      })
    ).data.createConcept.concept.id;
    const aspectID = (
      await client.mutate({
        mutation: createAspect,
        variables: { name: "Chocolate Chips", conceptID },
      })
    ).data.createAspect.aspect.id;
    const aspectID2 = (
      await client.mutate({
        mutation: createAspect,
        variables: { name: "Peanut Butter", conceptID },
      })
    ).data.createAspect.aspect.id;
    return { client, conceptID, deckID, aspectID, aspectID2 };
  };

  test("should fail when not logged in", async () => {
    expect.assertions(1);
    const { aspectID } = await getTestEnv();
    const client = graphQLClient();
    await expect(
      client.mutate({
        mutation: scheduleLearning,
        variables: {
          aspectID,
          dueDate: "2020-06-21T00:00:00Z",
          step: 3,
        },
      })
    ).rejects.toThrow();
  });

  test("can schedule", async () => {
    const { aspectID, client } = await getTestEnv();
    const schedule = (
      await client.mutate({
        mutation: scheduleLearning,
        variables: {
          aspectID,
          dueDate: "2020-06-21T00:00:00Z",
          step: 3,
        },
      })
    ).data.scheduleAspectLearning.scheduledAspect;
    expect(schedule.aspect.id).toBe(aspectID);
    expect(schedule.dueDate).toBe("2020-06-21T00:00:00Z");
    expect(schedule.schedule.step).toBe(3);
  });

  test("appears in my account", async () => {
    const { aspectID, client } = await getTestEnv();
    const schedule = (
      await client.mutate({
        mutation: scheduleLearning,
        variables: {
          aspectID,
          dueDate: "2021-06-21T00:00:00Z",
          step: 3,
        },
      })
    ).data.scheduleAspectLearning.scheduledAspect;
    const scheduleds = (
      await client.query({ query: currentUserScheduledAspects })
    ).data.currentUser.decks[0].scheduledAspects.edges;
    expect(scheduleds.length).toBe(1);
    expect(scheduleds[0].node.aspect.id).toBe(aspectID);
    expect(scheduleds[0].node.dueDate).toBe("2021-06-21T00:00:00Z");
    expect(scheduleds[0].node.schedule.step).toBe(3);
  });

  test("pagination basics", async () => {
    const { aspectID, aspectID2, client } = await getTestEnv();
    const schedule = (
      await client.mutate({
        mutation: scheduleLearning,
        variables: {
          aspectID,
          dueDate: "2021-06-21T00:00:00Z",
          step: 3,
        },
      })
    ).data.scheduleAspectLearning.scheduledAspect;
    const schedule2 = (
      await client.mutate({
        mutation: scheduleLearning,
        variables: {
          aspectID: aspectID2,
          dueDate: "2021-06-21T00:00:00Z",
          step: 3,
        },
      })
    ).data.scheduleAspectLearning.scheduledAspect;

    const scheduleds = (
      await client.query({ query: currentUserScheduledAspects })
    ).data.currentUser.decks[0].scheduledAspects.edges;
    expect(scheduleds.length).toBe(2);
    const scheduleds2 = (
      await client.query({ query: currentUserScheduledAspectsMax1 })
    ).data.currentUser.decks[0].scheduledAspects;
    expect(scheduleds2.edges.length).toBe(1);
    expect(scheduleds2.pageInfo.hasNextPage).toBe(true);
    expect(scheduleds2.pageInfo.startCursor).toBe(scheduleds2.edges[0].cursor);
    expect(scheduleds2.pageInfo.endCursor).toBe(scheduleds2.edges[0].cursor);

    const scheduleds3 = (
      await client.query({
        query: currentUserScheduledAspectsAfter,
        variables: { after: scheduleds2.edges[0].cursor },
      })
    ).data.currentUser.decks[0].scheduledAspects;

    expect(scheduleds3.edges.length).toBe(1);
    expect(scheduleds3.pageInfo.hasNextPage).toBe(false);
  });

  test("can re-schedule", async () => {
    const { aspectID, client } = await getTestEnv();
    await client.mutate({
      mutation: scheduleLearning,
      variables: {
        aspectID,
        dueDate: "2020-06-21T00:00:00Z",
        step: 1,
      },
    });
    const schedule = (
      await client.mutate({
        mutation: scheduleLearning,
        variables: {
          aspectID,
          dueDate: "2021-06-21T00:00:00Z",
          step: 3,
        },
      })
    ).data.scheduleAspectLearning.scheduledAspect;
    expect(schedule.aspect.id).toBe(aspectID);
    expect(schedule.dueDate).toBe("2021-06-21T00:00:00Z");
    expect(schedule.schedule.step).toBe(3);
    const scheduleds = (
      await client.query({ query: currentUserScheduledAspects })
    ).data.currentUser.decks[0].scheduledAspects.edges;
    expect(scheduleds.length).toBe(1);
    expect(scheduleds[0].node.dueDate).toBe("2021-06-21T00:00:00Z");
  });

  test("random user can't schedule my deck", async () => {
    expect.assertions(1);
    const { aspectID } = await getTestEnv();
    const client = await clientForUsername("The Count");
    await expect(
      client.mutate({
        mutation: scheduleLearning,
        variables: {
          aspectID,
          dueDate: "2021-06-21T00:00:00Z",
          step: 3,
        },
      })
    ).rejects.toThrow();
  });

  test("fails on non-existant aspect", async () => {
    expect.assertions(1);
    const { aspectID, client } = await getTestEnv();
    await expect(
      client.mutate({
        mutation: scheduleLearning,
        variables: {
          aspectID: aspectID + 1,
          dueDate: "2021-06-21T00:00:00Z",
          step: 3,
        },
      })
    ).rejects.toThrow();
  });

  test("ordered by due date", async () => {
    const { aspectID, conceptID, client } = await getTestEnv();
    const aspectID2 = (
      await client.mutate({
        mutation: createAspect,
        variables: { name: "Peanut Butter Chunks", conceptID },
      })
    ).data.createAspect.aspect.id;
    await client.mutate({
      mutation: scheduleLearning,
      variables: {
        aspectID,
        dueDate: "2021-06-21T00:00:00Z",
        step: 1,
      },
    });
    await client.mutate({
      mutation: scheduleLearning,
      variables: {
        aspectID: aspectID2,
        dueDate: "2020-06-21T00:00:00Z",
        step: 1,
      },
    });
    const scheduleds = (
      await client.query({ query: currentUserScheduledAspects })
    ).data.currentUser.decks[0].scheduledAspects.edges;
    expect(scheduleds.length).toBe(2);
    expect(scheduleds[0].node.aspect.id).toBe(aspectID2);
    expect(scheduleds[1].node.aspect.id).toBe(aspectID);
  });

  test("relearning", async () => {
    const { aspectID, client } = await getTestEnv();
    const schedule = (
      await client.mutate({
        mutation: scheduleRelearning,
        variables: {
          aspectID,
          dueDate: "2020-06-21T00:00:00Z",
          step: 3,
          factor: 2.3,
        },
      })
    ).data.scheduleAspectRelearning.scheduledAspect;
    expect(schedule.aspect.id).toBe(aspectID);
    expect(schedule.dueDate).toBe("2020-06-21T00:00:00Z");
    expect(schedule.schedule.step).toBe(3);
    expect(schedule.schedule.factor).toBe(2.3);
  });

  test("reviewing", async () => {
    const { aspectID, client } = await getTestEnv();
    const schedule = (
      await client.mutate({
        mutation: scheduleReview,
        variables: {
          aspectID,
          dueDate: "2020-06-21T00:00:00Z",
          studiedDate: "2020-05-21T00:00:00Z",
          factor: 2.3,
        },
      })
    ).data.scheduleAspectReview.scheduledAspect;
    expect(schedule.aspect.id).toBe(aspectID);
    expect(schedule.dueDate).toBe("2020-06-21T00:00:00Z");
    expect(schedule.schedule.factor).toBe(2.3);
    expect(schedule.schedule.studiedDate).toBe("2020-05-21T00:00:00Z");
  });
});
