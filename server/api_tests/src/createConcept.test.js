const gql = require("graphql-tag").default;
const { clearDatabase, graphQLClient, clientForUsername } = require("./util");
const { createDeck, createConcept } = require("./queries");

beforeEach(() => {
  return clearDatabase();
});

describe("`createConcept` mutation", () => {
  const currentUserConcepts = gql`
    query Query {
      currentUser {
        decks {
          deck {
            id
            concepts {
              name
              deck {
                id
              }
            }
          }
        }
      }
    }
  `;

  const getTestEnv = async () => {
    const client = await clientForUsername("Cookie Monster");
    const deckID = (
      await client.mutate({
        mutation: createDeck,
        variables: { name: "Things I Like" },
      })
    ).data.createDeck.deck.id;
    return { client, deckID };
  };

  test("should fail when not logged in", async () => {
    expect.assertions(1);
    const { deckID } = await getTestEnv();
    const client = graphQLClient();
    await expect(
      client.mutate({
        mutation: createConcept,
        variables: { name: "Cookies", deckID },
      })
    ).rejects.toThrow();
  });

  test("works", async () => {
    const { deckID, client } = await getTestEnv();
    const concept = (
      await client.mutate({
        mutation: createConcept,
        variables: { name: "Cookies", deckID },
      })
    ).data.createConcept.concept;
    expect(concept.name).toBe("Cookies");
    expect(concept.deck.id).toBe(deckID);
    expect(concept.deck.name).toBe("Things I Like");
  });

  test("appears in my account", async () => {
    const { deckID, client } = await getTestEnv();
    await expect(
      client.mutate({
        mutation: createConcept,
        variables: { name: "Cookies", deckID },
      })
    ).resolves.not.toThrow();
    const concepts = (await client.query({ query: currentUserConcepts })).data
      .currentUser.decks[0].deck.concepts;
    expect(concepts.length).toBe(1);
    expect(concepts[0].name).toBe("Cookies");
    expect(concepts[0].deck.id).toBe(deckID);
  });

  test("random user can't create concept in my deck", async () => {
    expect.assertions(1);
    const { deckID } = await getTestEnv();
    const client = await clientForUsername("The Count");
    await expect(
      client.mutate({
        mutation: createConcept,
        variables: { name: "Cookies", deckID },
      })
    ).rejects.toThrow();
  });

  test("fails on non-existant deck", async () => {
    expect.assertions(1);
    const { deckID, client } = await getTestEnv();
    await expect(
      client.mutate({
        mutation: createConcept,
        variables: { name: "Cookies", deckID: deckID + 1 },
      })
    ).rejects.toThrow();
  });

  test("fails on nonsense deck", async () => {
    expect.assertions(1);
    const { deckID, client } = await getTestEnv();
    await expect(
      client.mutate({
        mutation: createConcept,
        variables: { name: "Cookies", deckID: "Not a real deck" },
      })
    ).rejects.toThrow();
  });

  test("fails on nonsense deck 2", async () => {
    expect.assertions(1);
    const { deckID, client } = await getTestEnv();
    await expect(
      client.mutate({
        mutation: createConcept,
        variables: { name: "Cookies", deckID: `${deckID}foobar` },
      })
    ).rejects.toThrow();
  });
});
