const gql = require("graphql-tag").default;

module.exports = {
  createDeck: gql`
    mutation Mutation($name: String!) {
      createDeck(input: { name: $name }) {
        deck {
          id
          name
          owner {
            username
          }
        }
      }
    }
  `,
  createConcept: gql`
    mutation Mutation($name: String!, $deckID: ID!) {
      createConcept(input: { name: $name, deck: $deckID }) {
        concept {
          id
          name
          deck {
            id
            name
          }
        }
      }
    }
  `,
  deleteConcept: gql`
    mutation Mutation($conceptID: ID!) {
      deleteConcept(input: { concept: $conceptID }) {
        deletedConcept
      }
    }
  `,
  createAspect: gql`
    mutation Mutation($name: String!, $conceptID: ID!) {
      createAspect(input: { name: $name, concept: $conceptID }) {
        aspect {
          id
          name
          concept {
            id
            name
            deck {
              id
              name
            }
          }
        }
      }
    }
  `,
  deleteAspect: gql`
    mutation Mutation($aspectID: ID!) {
      deleteAspect(input: { aspect: $aspectID }) {
        deletedAspect
      }
    }
  `,
  createSelfGradedPictionaryTest: gql`
    mutation Mutation(
      $name: String!
      $aspectID: ID!
      $front: String!
      $back: String!
    ) {
      createSelfGradedPictionaryTest(
        input: {
          name: $name
          aspect: $aspectID
          testData: { front: $front, back: $back }
        }
      ) {
        test {
          id
          name
          testData {
            __typename
            ... on SelfGradedPictionaryTestData {
              front
              back
            }
          }
          aspect {
            id
            name
            concept {
              id
              name
              deck {
                id
                name
              }
            }
          }
        }
      }
    }
  `,
  scheduleLearning: gql`
    mutation Mutation($dueDate: Date!, $aspectID: ID!, $step: Int!) {
      scheduleAspectLearning(
        input: { aspect: $aspectID, dueDate: $dueDate, step: $step }
      ) {
        scheduledAspect {
          aspect {
            id
          }
          dueDate
          schedule {
            __typename
            ... on LearningSchedule {
              step
            }
          }
        }
      }
    }
  `,
  scheduleRelearning: gql`
    mutation Mutation(
      $dueDate: Date!
      $aspectID: ID!
      $step: Int!
      $factor: Float!
    ) {
      scheduleAspectRelearning(
        input: {
          aspect: $aspectID
          dueDate: $dueDate
          step: $step
          factor: $factor
        }
      ) {
        scheduledAspect {
          aspect {
            id
          }
          dueDate
          schedule {
            __typename
            ... on RelearningSchedule {
              step
              factor
            }
          }
        }
      }
    }
  `,
  scheduleReview: gql`
    mutation Mutation(
      $dueDate: Date!
      $aspectID: ID!
      $studiedDate: Date!
      $factor: Float!
    ) {
      scheduleAspectReview(
        input: {
          aspect: $aspectID
          dueDate: $dueDate
          studiedDate: $studiedDate
          factor: $factor
        }
      ) {
        scheduledAspect {
          aspect {
            id
          }
          dueDate
          schedule {
            __typename
            ... on ReviewSchedule {
              factor
              studiedDate
            }
          }
        }
      }
    }
  `,
};
