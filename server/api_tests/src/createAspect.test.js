const gql = require("graphql-tag").default;
const { clearDatabase, graphQLClient, clientForUsername } = require("./util");
const { createDeck, createConcept, createAspect } = require("./queries");

beforeEach(() => {
  return clearDatabase();
});

describe("`createAspect` mutation", () => {
  const currentUserAspects = gql`
    query Query {
      currentUser {
        decks {
          deck {
            id
            concepts {
              id
              name
              aspects {
                name
                concept {
                  id
                  name
                }
              }
            }
          }
        }
      }
    }
  `;

  const getTestEnv = async () => {
    const client = await clientForUsername("Cookie Monster");
    const deckID = (
      await client.mutate({
        mutation: createDeck,
        variables: { name: "Things I Like" },
      })
    ).data.createDeck.deck.id;
    const conceptID = (
      await client.mutate({
        mutation: createConcept,
        variables: { name: "Parts of Cookie", deckID },
      })
    ).data.createConcept.concept.id;
    return { client, conceptID, deckID };
  };

  test("should fail when not logged in", async () => {
    expect.assertions(1);
    const { conceptID } = await getTestEnv();
    const client = graphQLClient();
    await expect(
      client.mutate({
        mutation: createAspect,
        variables: { name: "Chocolate Chips", conceptID },
      })
    ).rejects.toThrow();
  });

  test("works", async () => {
    const { conceptID, deckID, client } = await getTestEnv();
    const aspect = (
      await client.mutate({
        mutation: createAspect,
        variables: { name: "Sprinkles", conceptID },
      })
    ).data.createAspect.aspect;
    expect(aspect.name).toBe("Sprinkles");
    expect(aspect.concept.id).toBe(conceptID);
    expect(aspect.concept.name).toBe("Parts of Cookie");
    expect(aspect.concept.deck.id).toBe(deckID);
    expect(aspect.concept.deck.name).toBe("Things I Like");
  });

  test("appears in my account", async () => {
    const { conceptID, client } = await getTestEnv();
    const aspect = (
      await client.mutate({
        mutation: createAspect,
        variables: { name: "Sprinkles", conceptID },
      })
    ).data.createAspect.aspect;
    const aspects = (await client.query({ query: currentUserAspects })).data
      .currentUser.decks[0].deck.concepts[0].aspects;
    expect(aspects.length).toBe(1);
    expect(aspects[0].name).toBe("Sprinkles");
    expect(aspects[0].concept.id).toBe(conceptID);
  });

  test("random user can't create an aspect in my deck", async () => {
    expect.assertions(1);
    const { conceptID } = await getTestEnv();
    const client = await clientForUsername("The Count");
    await expect(
      client.mutate({
        mutation: createAspect,
        variables: { name: "Cookies, eh", conceptID },
      })
    ).rejects.toThrow();
  });

  test("fails on non-existant deck", async () => {
    expect.assertions(1);
    const { conceptID, client } = await getTestEnv();
    await expect(
      client.mutate({
        mutation: createAspect,
        variables: { name: "Cookies", conceptID: conceptID + 1 },
      })
    ).rejects.toThrow();
  });

  test("fails on nonsense deck", async () => {
    expect.assertions(1);
    const { conceptID, client } = await getTestEnv();
    await expect(
      client.mutate({
        mutation: createAspect,
        variables: { name: "Cookies", conceptID: "Not a real concept" },
      })
    ).rejects.toThrow();
  });

  test("fails on nonsense deck 2", async () => {
    expect.assertions(1);
    const { conceptID, client } = await getTestEnv();
    await expect(
      client.mutate({
        mutation: createAspect,
        variables: { name: "Cookies", deckID: `${conceptID}foobar` },
      })
    ).rejects.toThrow();
  });
});
