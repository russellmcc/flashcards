const gql = require("graphql-tag").default;
const { clearDatabase, graphQLClient, clientForUsername } = require("./util");
const {
  createDeck,
  createConcept,
  createAspect,
  createSelfGradedPictionaryTest,
  scheduleLearning,
  deleteAspect,
} = require("./queries");

beforeEach(() => {
  return clearDatabase();
});

describe("`deleteAspect` mutation", () => {
  const currentUserAspects = gql`
    query Query {
      currentUser {
        decks {
          deck {
            id
            concepts {
              id
              name
              aspects {
                id
                name
                tests {
                  id
                  name
                }
              }
            }
          }
          scheduledAspects {
            edges {
              node {
                aspect {
                  id
                }
              }
            }
          }
          readyAspects {
            aspect {
              id
            }
          }
        }
      }
    }
  `;

  const getTestEnv = async () => {
    const client = await clientForUsername("Cookie Monster");
    const deckID = (
      await client.mutate({
        mutation: createDeck,
        variables: { name: "Things I Like" },
      })
    ).data.createDeck.deck.id;
    const conceptID = (
      await client.mutate({
        mutation: createConcept,
        variables: { name: "Parts of Cookie", deckID },
      })
    ).data.createConcept.concept.id;
    const aspectID = (
      await client.mutate({
        mutation: createAspect,
        variables: { name: "Chocolate Chips", conceptID },
      })
    ).data.createAspect.aspect.id;
    const testID = (
      await client.mutate({
        mutation: createSelfGradedPictionaryTest,
        variables: {
          name: "Color Test",
          aspectID,
          front: "Color?",
          back: "Blackish brown",
        },
      })
    ).data.createSelfGradedPictionaryTest.test.id;
    await client.mutate({
      mutation: scheduleLearning,
      variables: {
        aspectID,
        dueDate: "2020-06-21T00:00:00Z",
        step: 3,
      },
    });

    return { client, conceptID, deckID, aspectID, testID };
  };

  test("works", async () => {
    const { client, aspectID } = await getTestEnv();
    const deleted = (
      await client.mutate({
        mutation: deleteAspect,
        variables: { aspectID },
      })
    ).data.deleteAspect.deletedAspect;
    expect(deleted).toBe(aspectID);

    const data = (await client.query({ query: currentUserAspects })).data
      .currentUser;
    expect(
      data.decks.some((deck) =>
        deck.deck.concepts.some((concept) =>
          concept.aspects.some((aspect) => (aspect.id = aspectID))
        )
      )
    ).toBe(false);
  });

  test("can't delete already deleted aspect", async () => {
    const { client, aspectID } = await getTestEnv();
    await client.mutate({
      mutation: deleteAspect,
      variables: { aspectID },
    });
    await expect(
      client.mutate({
        mutation: deleteAspect,
        variables: { aspectID },
      })
    ).rejects.toThrow();
  });

  test("automatically readies", async () => {
    const { client, aspectID } = await getTestEnv();
    await client.mutate({
      mutation: deleteAspect,
      variables: { aspectID },
    });
    const data = (await client.query({ query: currentUserAspects })).data
      .currentUser;
    expect(
      data.decks.some(
        (deck) =>
          deck.scheduledAspects.edges.some(
            (sa) => sa.node.aspect.id == aspectID
          ) || deck.readyAspects.some((ua) => ua.aspect.id == aspectID)
      )
    ).toBe(false);
  });

  test("can't delete someone else's aspect", async () => {
    const { aspectID } = await getTestEnv();
    const client = await clientForUsername("The Count");
    await expect(
      client.mutate({
        mutation: deleteAspect,
        variables: { aspectID },
      })
    ).rejects.toThrow();
  });
});
