const gql = require("graphql-tag").default;
const { clearDatabase, graphQLClient } = require("./util");

beforeEach(() => {
  return clearDatabase();
});

describe("`currentUser` query", () => {
  const register = gql`
    mutation Mutation($username: String!) {
      register(input: { username: $username, password: "Please" }) {
        session {
          token
        }
      }
    }
  `;
  const login = gql`
    mutation Mutation($username: String!) {
      login(input: { username: $username, password: "Please" }) {
        session {
          token
        }
      }
    }
  `;

  const currentUser = gql`
    query Query {
      currentUser {
        username
      }
    }
  `;

  test("should fail when not logged in", async () => {
    expect.assertions(1);
    const client = graphQLClient();
    await expect(client.query({ query: currentUser })).rejects.toThrow();
  });

  test("gets expected name after registering", async () => {
    const client = graphQLClient();
    const res = await client.mutate({
      mutation: register,
      variables: { username: "Gomez" },
    });
    const loggedIn = graphQLClient(res.data.register.session.token);
    const resp = await loggedIn.query({ query: currentUser });
    expect(resp.data.currentUser.username).toBe("Gomez");
  });

  test("gets expected name after login", async () => {
    const client = graphQLClient();
    await client.mutate({
      mutation: register,
      variables: { username: "Gomez" },
    });
    const session = await client.mutate({
      mutation: login,
      variables: { username: "Gomez" },
    });
    const loggedIn = graphQLClient(session.data.login.session.token);
    const resp = await loggedIn.query({ query: currentUser });
    expect(resp.data.currentUser.username).toBe("Gomez");
  });

  test("can have multiple users", async () => {
    const client = graphQLClient();
    const clients = {};
    for (const name of ["Gomez", "Morticia", "Cousin It", "Fester"]) {
      clients[name] = graphQLClient(
        (
          await client.mutate({
            mutation: register,
            variables: { username: name },
          })
        ).data.register.session.token
      );
    }
    for (const name of Object.keys(clients)) {
      expect(
        (await clients[name].query({ query: currentUser })).data.currentUser
          .username
      ).toBe(name);
    }
  });
});
