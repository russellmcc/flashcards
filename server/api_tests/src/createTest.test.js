const gql = require("graphql-tag").default;
const { clearDatabase, graphQLClient, clientForUsername } = require("./util");
const {
  createDeck,
  createConcept,
  createAspect,
  createSelfGradedPictionaryTest,
} = require("./queries");

beforeEach(() => {
  return clearDatabase();
});

describe("`createTest` mutation", () => {
  const currentUserTests = gql`
    query Query {
      currentUser {
        decks {
          deck {
            id
            concepts {
              id
              name
              aspects {
                name
                tests {
                  id
                  name
                  aspect {
                    id
                  }
                  testData {
                    __typename
                    ... on SelfGradedPictionaryTestData {
                      front
                      back
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `;

  const getTestEnv = async () => {
    const client = await clientForUsername("Cookie Monster");
    const deckID = (
      await client.mutate({
        mutation: createDeck,
        variables: { name: "Things I Like" },
      })
    ).data.createDeck.deck.id;
    const conceptID = (
      await client.mutate({
        mutation: createConcept,
        variables: { name: "Parts of Cookie", deckID },
      })
    ).data.createConcept.concept.id;
    const aspectID = (
      await client.mutate({
        mutation: createAspect,
        variables: { name: "Chocolate Chips", conceptID },
      })
    ).data.createAspect.aspect.id;
    return { client, conceptID, deckID, aspectID };
  };

  test("should fail when not logged in", async () => {
    expect.assertions(1);
    const { aspectID } = await getTestEnv();
    const client = graphQLClient();
    await expect(
      client.mutate({
        mutation: createSelfGradedPictionaryTest,
        variables: {
          name: "Color Test",
          aspectID,
          front: "Color?",
          back: "Blackish brown",
        },
      })
    ).rejects.toThrow();
  });

  test("works", async () => {
    const { aspectID, conceptID, deckID, client } = await getTestEnv();
    const test = (
      await client.mutate({
        mutation: createSelfGradedPictionaryTest,
        variables: {
          name: "Color Test",
          aspectID,
          front: "Color?",
          back: "Blackish brown",
        },
      })
    ).data.createSelfGradedPictionaryTest.test;
    expect(test.name).toBe("Color Test");
    expect(test.testData.front).toBe("Color?");
    expect(test.testData.back).toBe("Blackish brown");
    expect(test.aspect.id).toBe(aspectID);
    expect(test.aspect.concept.id).toBe(conceptID);
    expect(test.aspect.concept.name).toBe("Parts of Cookie");
    expect(test.aspect.concept.deck.id).toBe(deckID);
    expect(test.aspect.concept.deck.name).toBe("Things I Like");
  });

  test("appears in my account", async () => {
    const { aspectID, client } = await getTestEnv();
    const test = (
      await client.mutate({
        mutation: createSelfGradedPictionaryTest,
        variables: {
          name: "Color Test",
          aspectID,
          front: "Color?",
          back: "Blackish brown",
        },
      })
    ).data.createSelfGradedPictionaryTest.test;
    const tests = (await client.query({ query: currentUserTests })).data
      .currentUser.decks[0].deck.concepts[0].aspects[0].tests;
    expect(tests.length).toBe(1);
    expect(tests[0].name).toBe("Color Test");
    expect(tests[0].aspect.id).toBe(aspectID);
    expect(tests[0].testData.__typename).toBe("SelfGradedPictionaryTestData");
    expect(tests[0].testData.front).toBe("Color?");
    expect(tests[0].testData.back).toBe("Blackish brown");
  });

  test("random user can't create an aspect in my deck", async () => {
    expect.assertions(1);
    const { aspectID } = await getTestEnv();
    const client = await clientForUsername("The Count");
    await expect(
      client.mutate({
        mutation: createSelfGradedPictionaryTest,
        variables: {
          name: "Color Test",
          aspectID,
          front: "Color?",
          back: "Blackish brown",
        },
      })
    ).rejects.toThrow();
  });

  test("fails on non-existant deck", async () => {
    expect.assertions(1);
    const { aspectID, client } = await getTestEnv();
    await expect(
      client.mutate({
        mutation: createSelfGradedPictionaryTest,
        variables: {
          name: "Color Test",
          aspectID: aspectID + 1,
          front: "Color?",
          back: "Blackish brown",
        },
      })
    ).rejects.toThrow();
  });

  test("fails on nonsense aspect", async () => {
    expect.assertions(1);
    const { aspectID, client } = await getTestEnv();
    await expect(
      client.mutate({
        mutation: createSelfGradedPictionaryTest,
        variables: {
          name: "Color Test",
          aspectID: "Not a real aspect",
          front: "Color?",
          back: "Blackish brown",
        },
      })
    ).rejects.toThrow();
  });

  test("fails on nonsense aspect 2", async () => {
    expect.assertions(1);
    const { aspectID, client } = await getTestEnv();
    await expect(
      client.mutate({
        mutation: createSelfGradedPictionaryTest,
        variables: {
          name: "Color Test",
          aspectID: `${aspectID}foobar`,
          front: "Color?",
          back: "Blackish brown",
        },
      })
    ).rejects.toThrow();
  });
});
