const gql = require("graphql-tag").default;
const { clearDatabase, graphQLClient } = require("./util");

beforeEach(() => {
  return clearDatabase();
});

describe("`register` mutation", () => {
  const register = gql`
    mutation Mutation($username: String!) {
      register(input: { username: $username, password: "Please" }) {
        session {
          token
        }
      }
    }
  `;

  test("should return a session", async () => {
    const client = graphQLClient();
    const res = await client.mutate({
      mutation: register,
      variables: { username: "Wednesday" },
    });
    expect(res.data.register.session.token.length > 0);
  });

  test("can't register same user twice", async () => {
    expect.assertions(1);
    const client = graphQLClient();
    await client.mutate({
      mutation: register,
      variables: { username: "Wednesday" },
    });
    await expect(
      client.mutate({
        mutation: register,
        variables: { username: "Wednesday" },
      })
    ).rejects.toThrow();
  });

  test("okay to register multiple users", async () => {
    const client = graphQLClient();
    await client.mutate({
      mutation: register,
      variables: { username: "Wednesday" },
    });
    await client.mutate({
      mutation: register,
      variables: { username: "Thing" },
    });
  });
});
