const gql = require("graphql-tag").default;
const { clearDatabase, graphQLClient } = require("./util");

beforeEach(() => {
  return clearDatabase();
});

describe("`login` mutation", () => {
  const register = gql`
    mutation Mutation($username: String!) {
      register(input: { username: $username, password: "Please" }) {
        session {
          token
        }
      }
    }
  `;
  const login = gql`
    mutation Mutation($username: String!, $password: String!) {
      login(input: { username: $username, password: $password }) {
        session {
          token
        }
      }
    }
  `;

  test("Logging in with garbage user fails", async () => {
    expect.assertions(1);
    const client = graphQLClient();
    await expect(
      client.mutate({
        mutation: login,
        variables: { username: "No one", password: "nothing" },
      })
    ).rejects.toThrow();
  });

  test("Logging in with wrong password fails", async () => {
    expect.assertions(1);
    const client = graphQLClient();
    await client.mutate({
      mutation: register,
      variables: { username: "Pythagoras" },
    });
    await expect(
      client.mutate({
        mutation: login,
        variables: { username: "Pythagoras", password: "I like triangles" },
      })
    ).rejects.toThrow();
  });

  test("Basic log-in", async () => {
    const client = graphQLClient();
    await client.mutate({
      mutation: register,
      variables: { username: "Pythagoras" },
    });
    expect(
      (
        await client.mutate({
          mutation: login,
          variables: { username: "Pythagoras", password: "Please" },
        })
      ).data.login.session.token.length > 0
    );
  });
});
