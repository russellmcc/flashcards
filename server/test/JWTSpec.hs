module JWTSpec where

import Authentication.JWT
import Data.Time.Calendar (Day (..))
import Data.Time.Clock (DiffTime, UTCTime (..), secondsToDiffTime)
import Test.Hspec
import Test.QuickCheck

spec :: Spec
spec = do
  describe "JWT" $ do
    it "can authenticate" $
      verifyJWT day "secret" jwt `shouldBe` Just 43
  where
    day = UTCTime (ModifiedJulianDay 59130) $ secondsToDiffTime 0
    jwt = makeJWT day "secret" 43
