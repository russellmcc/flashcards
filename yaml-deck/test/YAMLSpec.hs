module YAMLSpec where

import qualified Data.ByteString.Lazy as B
import Data.YAML.Aeson as Y
import Deck
import Test.Hspec

expectedDeck :: Deck
expectedDeck =
  Deck
    "foo2"
    [ Concept "moo" [Aspect "Q" [Test "my Test" (TestDataEntry "my front" "my back" (Just ["wrong1", "wrong2", "wrong3"]) True)], Aspect "T" []],
      Concept "loo" [Aspect "R" [], Aspect "J" []]
    ]

spec :: Spec
spec = do
  describe "Yaml" $ do
    it "can parse a file" $ do
      (Y.decode1 <$> B.readFile "test/assets/test.yml") >>= (`shouldBe` Right expectedDeck)
