module MakeDeck where

import Control.Monad (void)
import Data.Morpheus.Types
import Deck as D
import Graphql
import Graphql.Queries as Q
import Login (withLogin)

makeTest :: (Graphql m) => ID -> D.Test -> m ()
makeTest
  aspectID
  Test
    { name,
      testData =
        TestDataSelfGradedPictionary
          { front,
            back
          }
    } =
    do
      (_ :: DoCreateSelfGradedPictionaryTest) <-
        fetch'
          ( Q.DoCreateSelfGradedPictionaryTestArgs
              Q.CreateSelfGradedPictionaryTestInput
                { name = name,
                  aspect = aspectID,
                  testData =
                    Q.SelfGradedPictionaryTestDataInput
                      { front = front,
                        back = back
                      }
                }
          )
      pure ()
makeTest aspectID Test {name, testData = TestDataEntry {front, back, wrongs, autoGrade}} = do
  (_ :: DoCreateEntryTest) <-
    fetch'
      ( Q.DoCreateEntryTestArgs
          Q.CreateEntryTestInput
            { name = name,
              aspect = aspectID,
              testData =
                Q.EntryTestDataInput
                  { front = front,
                    back = back,
                    wrongs = wrongs,
                    autoGrade = autoGrade
                  }
            }
      )
  pure ()

makeAspect :: (Graphql m) => ID -> D.Aspect -> m ()
makeAspect conceptID Aspect {name, tests} = do
  (payload :: DoCreateAspect) <-
    fetch'
      ( Q.DoCreateAspectArgs
          Q.CreateAspectInput
            { name = name,
              concept = conceptID
            }
      )
  mapM_ (makeTest (aspectID . caAspect . createAspect $ payload)) tests

deleteTest :: forall m. (Graphql m) => ID -> m ()
deleteTest testID =
  void
    ( ( fetch' $
          Q.DoDeleteTestArgs Q.DeleteTestInput {test = testID}
      ) ::
        m DoDeleteTest
    )

deleteAspect :: forall m. (Graphql m) => ID -> m ()
deleteAspect aspectID =
  void
    ( ( fetch' $
          Q.DoDeleteAspectArgs Q.DeleteAspectInput {aspect = aspectID}
      ) ::
        m DoDeleteAspect
    )

deleteConcept :: forall m. (Graphql m) => ID -> m ()
deleteConcept conceptID =
  void
    ( ( fetch' $
          Q.DoDeleteConceptArgs Q.DeleteConceptInput {concept = conceptID}
      ) ::
        m DoDeleteConcept
    )

makeConcept :: (Graphql m) => ID -> D.Concept -> m ()
makeConcept deckID Concept {name, aspects} = do
  (payload :: DoCreateConcept) <-
    fetch'
      ( Q.DoCreateConceptArgs
          Q.CreateConceptInput
            { name = name,
              deck = deckID
            }
      )
  mapM_ (makeAspect (conceptID . ccConcept . createConcept $ payload)) aspects

makeDeck' :: (Graphql m) => D.Deck -> m ()
makeDeck' Deck {name, concepts} = do
  (payload :: DoCreateDeck) <-
    fetch'
      ( Q.DoCreateDeckArgs
          Q.CreateDeckInput
            { name = name
            }
      )
  mapM_ (makeConcept (deckID . cdDeck . createDeck $ payload)) concepts

makeDeck :: (Graphql m) => D.Deck -> m ()
makeDeck deck = withLogin (makeDeck' deck)
