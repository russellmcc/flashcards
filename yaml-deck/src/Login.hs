module Login (withLogin) where

import Control.Monad.Reader
import Data.Morpheus.Types
import Graphql
import Graphql.Queries as Q

withLogin :: (Graphql m) => m a -> m a
withLogin f = do
  Env {config = Config {graphqlUsername, graphqlPassword}} <- ask
  (payload :: DoLogin) <-
    fetch'
      ( Q.DoLoginArgs
          Q.LoginInput
            { username = graphqlUsername,
              password = graphqlPassword
            }
      )
  runAuth (Token . unpackID . Q.token . session . login $ payload) f
