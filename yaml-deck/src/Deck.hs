module Deck where

import ConvertTag (convertTag)
import Data.Aeson (FromJSON (..), Options (..), SumEncoding (..), ToJSON (..), defaultOptions, genericParseJSON, genericToJSON)
import Data.Text
import GHC.Generics

data TestData
  = TestDataSelfGradedPictionary {front :: Text, back :: Text}
  | TestDataEntry
      { front :: Text,
        back :: Text,
        wrongs :: Maybe [Text],
        autoGrade :: Bool
      }
  deriving (Show, Generic, Eq)

instance FromJSON TestData where
  parseJSON = genericParseJSON defaultOptions {constructorTagModifier = convertTag, sumEncoding = TaggedObject "type" "data"}

instance ToJSON TestData where
  toJSON = genericToJSON defaultOptions {constructorTagModifier = convertTag, sumEncoding = TaggedObject "type" "data"}

data Test = Test
  { name :: Text,
    testData :: TestData
  }
  deriving (Show, Generic, Eq, FromJSON, ToJSON)

data Aspect = Aspect
  { name :: Text,
    tests :: [Test]
  }
  deriving (Show, Generic, Eq, FromJSON, ToJSON)

data Concept = Concept
  { name :: Text,
    aspects :: [Aspect]
  }
  deriving (Show, Generic, Eq, FromJSON, ToJSON)

data Deck = Deck
  { name :: Text,
    concepts :: [Concept]
  }
  deriving (Show, Generic, Eq, FromJSON, ToJSON)
