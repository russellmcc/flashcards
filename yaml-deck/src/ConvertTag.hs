module ConvertTag where

convertTag :: String -> String
convertTag "TestDataSelfGradedPictionary" = "self-graded-pictionary"
convertTag "TestDataEntry" = "entry"
convertTag _ = error "Invalid tag"
