module Application where

import Control.Monad.Reader
import qualified Data.Aeson as A
import qualified Data.ByteString.Lazy as B
import Data.Text
import qualified Data.YAML.Aeson as Y
import GHC.Generics
import GetDeck
import Graphql
import LoadEnv
import MakeDeck
import Options.Applicative
import SyncDeck
import System.Envy

data EnvArgs = EnvArgs
  { graphqlUrl :: Maybe Text,
    graphqlUsername :: Maybe Text,
    graphqlPassword :: Maybe Text
  }
  deriving (Show, Generic)

instance FromEnv EnvArgs where
  fromEnv _ =
    EnvArgs <$> (envMaybe "GRAPHQL_URL" .!= Nothing)
      <*> (envMaybe "GRAPHQL_USERNAME" .!= Nothing)
      <*> (envMaybe "GRAPHQL_PASSWORD" .!= Nothing)

loadConfig :: IO EnvArgs
loadConfig = loadEnv >> decodeEnv >>= either fail pure

data CommonArgs = CommonArgs
  { username :: Text,
    password :: Text,
    url :: Text
  }

data UploadArgs = UploadArgs
  { common :: CommonArgs,
    deckFile :: String
  }

data DownloadArgs = DownloadArgs
  { common :: CommonArgs,
    deckName :: Text,
    deckFile :: String
  }

data SyncArgs = SyncArgs
  { common :: CommonArgs,
    deckFile :: String
  }

data Command = Upload UploadArgs | Download DownloadArgs | Sync SyncArgs

commonParser :: EnvArgs -> Options.Applicative.Parser CommonArgs
commonParser EnvArgs {graphqlUsername, graphqlPassword, graphqlUrl} =
  CommonArgs
    <$> strOption (long "username" <> short 'u' <> metavar "user" <> v graphqlUsername)
    <*> strOption (long "password" <> short 'p' <> metavar "pass" <> v graphqlPassword)
    <*> strOption (long "url" <> short 'a' <> help "URL of GraphQL API" <> metavar "url" <> v graphqlUrl)
  where
    v = maybe mempty value

uploadParser :: EnvArgs -> Options.Applicative.Parser UploadArgs
uploadParser c = UploadArgs <$> commonParser c <*> strArgument (metavar "deck-yaml-file")

downloadParser :: EnvArgs -> Options.Applicative.Parser DownloadArgs
downloadParser c =
  DownloadArgs
    <$> commonParser c
    <*> strOption (long "deck-name" <> short 'd' <> metavar "deck")
    <*> strArgument (metavar "deck-yaml-file")

syncParser :: EnvArgs -> Options.Applicative.Parser SyncArgs
syncParser c =
  SyncArgs
    <$> commonParser c
    <*> strArgument (metavar "deck-yaml-file")

uploadOpts :: EnvArgs -> ParserInfo Command
uploadOpts c =
  info
    (Upload <$> uploadParser c)
    ( progDesc "Uploads a deck in yaml format to misremember"
    )

downloadOpts :: EnvArgs -> ParserInfo Command
downloadOpts c =
  info
    (Download <$> downloadParser c)
    ( progDesc "Downloads a deck in yaml format to misremember"
    )

syncOpts :: EnvArgs -> ParserInfo Command
syncOpts c =
  info
    (Sync <$> syncParser c)
    (progDesc "Syncs a deck in yaml format to misremember.  Note currently this will only add missing concepts/aspects/tests, and cannot delete parts of the deck.")

opts :: EnvArgs -> ParserInfo Command
opts c =
  info
    ( hsubparser
        ( command "download" (downloadOpts c)
            <> command "upload" (uploadOpts c)
            <> command "sync" (syncOpts c)
        )
    )
    (fullDesc <> header "misremember-yaml-deck-exe")

toConfig :: CommonArgs -> Config
toConfig CommonArgs {username, url, password} = Config url username password

runGQL :: CommonArgs -> ReaderT Env m a -> m a
runGQL common f = runReaderT f (Env {config = toConfig common, token = Nothing})

decodeFileThrow :: (A.FromJSON a) => String -> IO a
decodeFileThrow f =
  B.readFile f >>= \y ->
    either (fail . snd) pure (Y.decode1 y)

encodeFile :: (A.ToJSON a) => String -> a -> IO ()
encodeFile f y = B.writeFile f (Y.encode1 y)

upload :: UploadArgs -> IO ()
upload UploadArgs {common, deckFile} =
  decodeFileThrow deckFile >>= \parsed ->
    runGQL common (makeDeck parsed)

download :: DownloadArgs -> IO ()
download DownloadArgs {common, deckName, deckFile} =
  runGQL common (getDeck deckName)
    >>= either fail (encodeFile deckFile)

sync :: SyncArgs -> IO ()
sync SyncArgs {common, deckFile} =
  decodeFileThrow deckFile >>= \parsed ->
    runGQL common (syncDeck parsed) >>= either fail pure

runCommand :: Command -> IO ()
runCommand (Upload u) = upload u
runCommand (Download d) = download d
runCommand (Sync d) = sync d

app :: IO ()
app = loadConfig >>= (execParser . opts) >>= runCommand
