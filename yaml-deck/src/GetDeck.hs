module GetDeck (getDeck, findDeck) where

import Control.Arrow ((>>>))
import Data.Functor ((<&>))
import qualified Data.List as L
import Data.Maybe (maybeToList)
import Data.Text
import Deck as D
import Graphql
import Graphql.GetAllDecksQuery as Q
import Login (withLogin)

toTestData :: GetAllDecksCurrentUserDecksDeckConceptsAspectsTestsTestData -> Maybe D.TestData
toTestData
  ( GetAllDecksCurrentUserDecksDeckConceptsAspectsTestsTestDataVariantSelfGradedPictionaryTestData
      ( GetAllDecksCurrentUserDecksDeckConceptsAspectsTestsTestDataSelfGradedPictionaryTestData
          { front,
            back
          }
        )
    ) =
    Just $
      TestDataSelfGradedPictionary {front, back}
toTestData
  ( GetAllDecksCurrentUserDecksDeckConceptsAspectsTestsTestDataVariantEntryTestData
      ( GetAllDecksCurrentUserDecksDeckConceptsAspectsTestsTestDataEntryTestData
          { front,
            back,
            wrongs,
            autoGrade
          }
        )
    ) =
    Just $
      TestDataEntry {front, back, wrongs, autoGrade}
toTestData _ = Nothing

toTest :: GetAllDecksCurrentUserDecksDeckConceptsAspectsTests -> Maybe D.Test
toTest GetAllDecksCurrentUserDecksDeckConceptsAspectsTests {name, testData = testData'} =
  toTestData testData' <&> \testData -> D.Test {name, testData}

toAspect :: GetAllDecksCurrentUserDecksDeckConceptsAspects -> D.Aspect
toAspect GetAllDecksCurrentUserDecksDeckConceptsAspects {name, tests} = D.Aspect {name, tests = tests >>= (toTest >>> maybeToList)}

toConcept :: GetAllDecksCurrentUserDecksDeckConcepts -> D.Concept
toConcept GetAllDecksCurrentUserDecksDeckConcepts {name, aspects} = D.Concept {name, aspects = toAspect <$> aspects}

toDeck :: GetAllDecksCurrentUserDecksDeck -> D.Deck
toDeck GetAllDecksCurrentUserDecksDeck {name, concepts} = D.Deck {name, concepts = toConcept <$> concepts}

findDeck :: Text -> GetAllDecks -> Maybe GetAllDecksCurrentUserDecksDeck
findDeck deckName payload =
  L.find
    (\GetAllDecksCurrentUserDecksDeck {name} -> deckName == name)
    ((\GetAllDecksCurrentUserDecks {deck} -> deck) <$> (decks . currentUser) payload)

getDeck' :: (Graphql m) => Text -> m (Either String D.Deck)
getDeck' deckName = maybe (Left $ "Could not find deck `" ++ unpack deckName ++ "`") (Right . toDeck) . findDeck deckName <$> fetch' ()

getDeck :: (Graphql m) => Text -> m (Either String D.Deck)
getDeck name = withLogin (getDeck' name)
