module SyncDeck (syncDeck) where

import Control.Arrow ((>>>))
import Control.Monad (when)
import qualified Data.List as L
import Data.Morpheus.Types
import Data.Text
import Deck as D
import GetDeck
import Graphql
import Graphql.GetAllDecksQuery as Q
import Login (withLogin)
import MakeDeck

isDuplicateName ::
  (Foldable t1, Eq a1, Eq a2) =>
  a2 ->
  a1 ->
  (t2 -> a2) ->
  (t2 -> a1) ->
  t1 t2 ->
  Bool
isDuplicateName name id_ nameG idG gs =
  maybe False (\g -> id_ /= idG g) $
    L.find (\g -> name == nameG g) gs

syncTests :: (Graphql m) => ID -> [D.Test] -> [GetAllDecksCurrentUserDecksDeckConceptsAspectsTests] -> m ()
syncTests aspectID tests tests' = mapM_ syncTest tests >> mapM_ deleteOld tests'
  where
    nameG GetAllDecksCurrentUserDecksDeckConceptsAspectsTests {name = name'} = name'
    idG GetAllDecksCurrentUserDecksDeckConceptsAspectsTests {id = ID id'} = id'
    isMissing name = not $ L.any (\D.Test {name = name'} -> name == name') tests
    syncTest t@D.Test {name} =
      maybe
        (makeTest aspectID t)
        (pure $ pure ())
        $ L.find (\GetAllDecksCurrentUserDecksDeckConceptsAspectsTests {name = name'} -> name' == name) tests'
    deleteOld GetAllDecksCurrentUserDecksDeckConceptsAspectsTests {name, id = ID id_} =
      when (isDuplicateName name id_ nameG idG tests' || isMissing name) $ deleteTest (ID id_)

syncAspects :: (Graphql m) => ID -> [D.Aspect] -> [GetAllDecksCurrentUserDecksDeckConceptsAspects] -> m ()
syncAspects conceptID aspects aspects' = mapM_ syncCurrent aspects >> mapM_ deleteOld aspects'
  where
    idG GetAllDecksCurrentUserDecksDeckConceptsAspects {id = ID id'} = id'
    nameG GetAllDecksCurrentUserDecksDeckConceptsAspects {name = name'} = name'
    isMissing name = not $ L.any (\D.Aspect {name = name'} -> name == name') aspects
    syncCurrent a@D.Aspect {name, tests} =
      maybe
        (makeAspect conceptID a)
        (\GetAllDecksCurrentUserDecksDeckConceptsAspects {id = aspectID, tests = tests'} -> syncTests aspectID tests tests')
        $ L.find (\GetAllDecksCurrentUserDecksDeckConceptsAspects {name = name'} -> name' == name) aspects'
    deleteOld GetAllDecksCurrentUserDecksDeckConceptsAspects {name, id = ID id_} =
      when (isDuplicateName name id_ nameG idG aspects' || isMissing name) $ deleteAspect (ID id_)

syncConcepts :: (Graphql m) => ID -> [D.Concept] -> [GetAllDecksCurrentUserDecksDeckConcepts] -> m ()
syncConcepts deckID concepts concepts' = mapM_ syncConcept concepts >> mapM_ deleteOld concepts'
  where
    idG GetAllDecksCurrentUserDecksDeckConcepts {id = ID id'} = id'
    nameG GetAllDecksCurrentUserDecksDeckConcepts {name = name'} = name'
    isMissing name = not $ L.any (\D.Concept {name = name'} -> name == name') concepts
    syncConcept c@D.Concept {name, aspects} =
      maybe
        (makeConcept deckID c)
        (\GetAllDecksCurrentUserDecksDeckConcepts {id = conceptID, aspects = aspects'} -> syncAspects conceptID aspects aspects')
        $ L.find (\GetAllDecksCurrentUserDecksDeckConcepts {name = name'} -> name' == name) concepts'
    deleteOld GetAllDecksCurrentUserDecksDeckConcepts {name, id = ID id_} =
      when (isDuplicateName name id_ nameG idG concepts' || isMissing name) $ deleteConcept (ID id_)

syncDeck' :: (Graphql m) => D.Deck -> m (Either String ())
syncDeck' D.Deck {name = deckName, concepts} =
  fetch' ()
    >>= ( findDeck deckName
            >>> maybe (pure . Left $ "Could not find deck `" ++ unpack deckName ++ "`") ((Right <$>) . sync)
        )
  where
    sync :: (Graphql m) => GetAllDecksCurrentUserDecksDeck -> m ()
    sync GetAllDecksCurrentUserDecksDeck {id = deckID, concepts = concepts'} = syncConcepts deckID concepts concepts'

syncDeck :: (Graphql m) => D.Deck -> m (Either String ())
syncDeck deck = withLogin (syncDeck' deck)
