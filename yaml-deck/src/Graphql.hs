module Graphql where

import Control.Monad.Catch
import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.ByteString.Lazy.Char8 (ByteString)
import Data.Data (Typeable)
import Data.Morpheus.Client (Fetch (..), FetchError)
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8)
import GHC.Generics
import Network.HTTP.Req
import Text.URI
  ( Authority (authPort),
    URI (URI, uriAuthority),
    mkURI,
  )

data FetchException a = FetchException (FetchError a) deriving (Show)

instance (Typeable a, Show a) => Exception (FetchException a)

fetch' :: (Fetch b, Graphql m, Typeable b, Show b) => Args b -> m b
fetch' q = fetch resolver q >>= either (throwM . FetchException) pure

data Config = Config
  { graphqlUrl :: Text,
    graphqlUsername :: Text,
    graphqlPassword :: Text
  }
  deriving (Show, Generic)

newtype Token = Token Text deriving (Show)

data Env = Env
  { config :: Config,
    token :: Maybe Token
  }
  deriving (Show, Generic)

type App = ReaderT Env IO

class GQLResolve m where
  resolver :: ByteString -> m ByteString

instance GQLResolve App where
  resolver b =
    reader (graphqlUrl . config) >>= mkURI >>= \uri ->
      maybe (fail "Bad URL") (either (reqI uri) (reqI uri)) . useURI $ uri
    where
      headers = header "Content-Type" "application/json"
      reqI :: (MonadIO m, MonadReader Env m) => URI -> (Url a, b) -> m ByteString
      reqI uri (s, _) =
        reader token
          >>= \tok ->
            let options = maybe mempty (\(Token t) -> header "Authorization" ("Bearer " <> encodeUtf8 t)) tok
             in liftIO $
                  runReq defaultHttpConfig (responseBody <$> req POST s (ReqBodyLbs b) lbsResponse (options <> headers <> port (port' uri)))
      port' :: URI -> Int
      port' URI {uriAuthority = (Left _)} = 80
      port' URI {uriAuthority = (Right a)} = maybe 80 fromIntegral $ authPort a

runAuth :: (Graphql m) => Token -> m a -> m a
runAuth t = local (\e -> e {token = Just t})

type Graphql m = (MonadFail m, MonadThrow m, GQLResolve m, MonadReader Env m)
