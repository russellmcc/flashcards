module Graphql.Queries where

import Data.Morpheus.Client
  ( declareGlobalTypes,
    declareLocalTypesInline,
    raw,
  )
import Data.Morpheus.Types
import Data.Text (Text)
import Graphql.Date (Date)

declareGlobalTypes "../graphql/schema.gql"
declareLocalTypesInline
  "../graphql/schema.gql"
  [raw|
      mutation DoLogin($input: LoginInput!) {
        login(input: $input) {
          session {
            token
          }
        }
      }
  |]

declareLocalTypesInline
  "../graphql/schema.gql"
  [raw|
      mutation DoCreateDeck($input: CreateDeckInput!) {
        createDeck(input: $input) {
          cdDeck: deck {
            deckID: id
          }
        }
      }
  |]

declareLocalTypesInline
  "../graphql/schema.gql"
  [raw|
      mutation DoCreateConcept($input: CreateConceptInput!) {
        createConcept(input: $input) {
          ccConcept: concept {
            conceptID: id
          }
        }
      }
  |]

declareLocalTypesInline
  "../graphql/schema.gql"
  [raw|
      mutation DoDeleteConcept($input: DeleteConceptInput!) {
        deleteConcept(input: $input) {
          deletedConcept
        }
      }
  |]

declareLocalTypesInline
  "../graphql/schema.gql"
  [raw|
      mutation DoCreateAspect($input: CreateAspectInput!) {
        createAspect(input: $input) {
          caAspect: aspect {
            aspectID: id
          }
        }
      }
  |]

declareLocalTypesInline
  "../graphql/schema.gql"
  [raw|
      mutation DoDeleteAspect($input: DeleteAspectInput!) {
        deleteAspect(input: $input) {
          deletedAspect
        }
      }
  |]

declareLocalTypesInline
  "../graphql/schema.gql"
  [raw|
      mutation DoDeleteTest($input: DeleteTestInput!) {
        deleteTest(input: $input) {
          deletedTest
        }
      }
  |]

declareLocalTypesInline
  "../graphql/schema.gql"
  [raw|
      mutation DoCreateSelfGradedPictionaryTest($input: CreateSelfGradedPictionaryTestInput!) {
        createSelfGradedPictionaryTest(input: $input) {
          test {
            testID: id
          }
        }
      }
  |]

declareLocalTypesInline
  "../graphql/schema.gql"
  [raw|
      mutation DoCreateEntryTest($input: CreateEntryTestInput!) {
        createEntryTest(input: $input) {
          test {
            testID: id
          }
        }
      }
  |]
