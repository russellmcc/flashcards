module Graphql.Date (Date (..)) where

import Data.Aeson (FromJSON (..), ToJSON (..), withText)
import qualified Data.Aeson (Value (String))
import Data.Either.Combinators (mapLeft)
import Data.Morpheus.Kind (SCALAR)
import Data.Morpheus.Types
  ( DecodeScalar (..),
    EncodeScalar (..),
    GQLType (KIND),
    ScalarValue (String),
  )
import Data.Text (pack, unpack)
import Data.Time.Clock (UTCTime)
import Data.Time.Format.ISO8601 (iso8601ParseM, iso8601Show)
import GHC.Generics (Generic)

newtype FailEither a = FailEither {runFailEither :: Either String a}
  deriving newtype
    ( Functor,
      Applicative,
      Monad
    )

instance MonadFail FailEither where
  fail x = FailEither $ Left x

instance DecodeScalar Date where
  decodeScalar (String x) = Date <$> mapLeft pack (runFailEither . iso8601ParseM . unpack $ x)
  decodeScalar _ = Left "Not a valid date."

instance EncodeScalar Date where
  encodeScalar (Date t) = String . pack $ iso8601Show t

instance GQLType Date where
  type KIND Date = SCALAR

newtype Date = Date {runDate :: UTCTime} deriving (Generic, Show, Eq)

instance FromJSON Date where
  parseJSON = withText "Date" $ \v ->
    (either (fail . unpack) pure . decodeScalar . String) v

instance ToJSON Date where
  toJSON = Data.Aeson.String . pack . iso8601Show . runDate
