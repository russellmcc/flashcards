module Graphql.GetAllDecksQuery where

import Data.Morpheus.Client
  ( declareGlobalTypes,
    declareLocalTypesInline,
    raw,
  )
import Data.Morpheus.Types
import Data.Text (Text)
import Graphql.Date (Date)

declareGlobalTypes "../graphql/schema.gql"

declareLocalTypesInline
  "../graphql/schema.gql"
  [raw|
   query GetAllDecks {
    currentUser {
      decks {
        deck {
          name
          id
          concepts {
            id
            name
            aspects {
              id
              name
              tests {
                id
                name
                testData {
                  ... on SelfGradedPictionaryTestData {
                    __typename
                    front
                    back
                  }
                  ... on EntryTestData {
                    __typename
                    front
                    back
                    wrongs
                    autoGrade
                  }
                }
              }
            }
          }
        }
      }
    }
  }
|]
